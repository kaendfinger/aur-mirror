#Maintainer: Alain Kalker <ackalker "at" gmail {dot} com>


pkgname=vmware-tools
pkgver=9.2.2.894247
pkgrel=1
pkgdesc="Tools for guest operating systems"
arch=('i686' 'x86_64')
url="http://kb.vmware.com/kb/340"
license=('custom:vmware')

_PV=$(echo ${pkgver} | cut -d . -f 1-3)
_MINOR=$(echo ${pkgver} | cut -d . -f 3)
_BUILD=$(echo ${pkgver} | cut -d . -f 4)
_BASE_URI="http://softwareupdate.vmware.com/cds/vmw-desktop/player/5.0.1/${_BUILD}/linux/packages/"

depends=()
makedepends=('libxslt')

if [ "$CARCH" = "i686" ]; then
	sha256sums=('fc547490f6b35e2da124ee817eb7d170e4c7af1891c7d0c01fcecfaa26752f1e'
            '245a464663c1db8afe273a6e0d5b075cec31907aabf0807644ff4386427887fd'
            'd4d60532c4838f814a63e0e53ce0a582500f43c47a308147fd9b84d8e1567fb4'
            '2187a1fc673681c4b98d544945f1af5947ae8c75eb7be4bfce8453cd884f4ab5'
            '19b741b6462125536a7b07e01894fdd42911c0e895436d543e0133c0cb337c56'
            'd064fb538610bf6c1ca90174d69cab559248956c5d25a24cda82063321b0f265'
            '6da856f4438c271d1082d1bd247ea587a6cb90d7e45130f97f55d948af69ed94'
            '4ee8796c9b4cdad00e9ed51f5a3e986f69b306f28675b7a0672522f5abcaa8f3')
elif [ "$CARCH" = "x86_64" ]; then
	sha256sums=('c23a7fc385b22b393774783215065169e17d1ff0b4cbe4bf1dad1f256af5e198'
            '26d27a1f3367ce648fae668165c0ecaf7a0337d1f120a7990bc4c64309786822'
            'bfdaffd6c9915844e4902ce74ab862239c614378fcf4aab780dc37802593cfad'
            '762465a7e7e3ef169c2c98192b99ef67998b1a7f8e42375e2d05b591bd517367'
            '852017eeeb1b2a3722a8fff847e66f43d23f4e55c8c2e9bab0ec0340746b4d24'
            '2bc29ec7d74b8e71fb88029267ab5701677cb5b01128d08b876867087d9dbcf6'
            '6da856f4438c271d1082d1bd247ea587a6cb90d7e45130f97f55d948af69ed94'
            '4ee8796c9b4cdad00e9ed51f5a3e986f69b306f28675b7a0672522f5abcaa8f3')
fi

_VMWARE_GUEST="freebsd linux netware solaris windows winPre2k"

_VM_INSTALL_DIR="/opt/vmware"

source=()
for guest in ${_VMWARE_GUEST} ; do
	if [ "$CARCH" = "i686" ]; then
		source+=("${_BASE_URI}vmware-tools-${guest}-${_PV}-${_BUILD}.i386.component.tar")
	elif [ "$CARCH" = "x86_64" ]; then
		source+=("${_BASE_URI}vmware-tools-${guest}-${_PV}-${_BUILD}.${CARCH}.component.tar")
	fi
done ; unset guest

source+=('list-bundle-components.xsl'
	'list-component-files.xsl')

vmware-bundle_extract-component() {
	local component=${1:?} dest=${2:-.}
	local -i offset=${3}
	local -i component_manifestOffset=$(od -An -j$((offset+9)) -N4 -tu4 "${component}")
	local -i component_manifestSize=$(od -An -j$((offset+13)) -N4 -tu4 "${component}")
	local -i component_dataOffset=$(od -An -j$((offset+17)) -N4 -tu4 "${component}")
	local -i component_dataSize=$(od -An -j$((offset+21)) -N8 -tu8 "${component}")
	tail -c+$((offset+component_manifestOffset+1)) "${component}" 2> /dev/null |
		head -c$((component_manifestSize)) | xsltproc "${srcdir}"/list-component-files.xsl - |
		while read -r file_offset file_compressedSize file_uncompressedSize file_path ; do
			if [[ ${file_path} ]] ; then
				echo -n '.'
				file_path="${dest}/${file_path}"
				mkdir -p "$(dirname "${file_path}")" || return 1
				tail -c+$((offset+component_dataOffset+file_offset+1)) "${component}" 2> /dev/null |
					head -c$((file_compressedSize)) | gzip -cd > "${file_path}" || return 1
			fi
		done
	echo
}

build() {
	cd "$srcdir"
	local arch
	if [ "$CARCH" = "i686" ] ; then arch='i386'
	elif [ "$CARCH" = "x86_64" ] ; then arch='x86_64'
	fi
	local guest ; for guest in ${_VMWARE_GUEST} ; do
		local component="vmware-tools-${guest}-${_PV}-${_BUILD}.${arch}.component"
		echo "Extracting '${component}'"
		vmware-bundle_extract-component "${component}"
	done
}

package() {
	cd "$srcdir"
	install -d "$pkgdir/${_VM_INSTALL_DIR}"/lib/vmware/isoimages
	local guest ; for guest in ${_VMWARE_GUEST} ; do
		cp -dr --no-preserve=ownership "${guest}".iso{,.sig} "$pkgdir/${_VM_INSTALL_DIR}"/lib/vmware/isoimages
	done
}
