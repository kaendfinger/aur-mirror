# Maintainer: anergy <anergy.25100321@gmail.com>

#### Specify target architecture.
### All target list is in ${srcdir}/${_gitname}-build/TargetList.txt 

_target_arch=NEHALEM 

_num_cores=4
#### Specify 
pkgname=openblas-git
pkgver=20130209
pkgrel=1
pkgdesc="optimized BLAS library based on GotoBLAS2 1.13 BSD version"
arch=(i686 x86_64)
url="https://github.com/xianyi/OpenBLAS"
license=('BSD')
groups=()
depends=()
makedepends=('git')
provides=('openblas' 'blas=3.4.2' 'cblas')
conflicts=('openblas' 'blas=3.4.2' 'cblas')
replaces=()
backup=()
options=()

_gitroot=git://github.com/xianyi/OpenBLAS.git
_gitname=openblas

case $arch in
    i686)
    _binary=32
    ;;
    x86_64)
    _binary=64
    ;;
esac

build() {
  cd "$srcdir"
  msg "Connecting to GIT server...."

  if [[ -d "$_gitname" ]]; then
    cd "$_gitname" && git pull origin
    msg "The local files are updated."
  else
    git clone "$_gitroot" "$_gitname"
  fi

  msg "GIT checkout done or server timeout"
  msg "Starting build..."
  rm -rf "$srcdir/$_gitname-build"
  git clone "$srcdir/$_gitname" "$srcdir/${_gitname}-build"

  if [ -z ${_target_arch} ] ; then
    msg "Please specify _target_arch, which is passed to TARGET."
    msg "The list is in ${srcdir}/${_gintname}-build/TargetList.txt"
    return 1;
  fi

  cd "$srcdir/$_gitname-build"
  make TARGET=${_target_arch} BINARY=${_arch} USE_OPENMP=1 NO_LAPACK=1 OMP_NUM_THREADS=${_num_cores}
}

package() {
  cd "${srcdir}/$_gitname-build"
  make PREFIX="${pkgdir}/usr" OMP_NUM_THREADS=${_num_cores}install
  install -m644 -D LICENSE ${pkgdir}/usr/share/licenses/${_gitname}/LICENSE

  #### fixing symlinks
  cd ${pkgdir}/usr/lib
  ln -sf libopenblas.so libblas.so.3
  ln -sf libopenblas.a libblas.a
  ln -sf libblas.so.3 libblas.so
  ln -sf libopenblas.so libcblas.so.3
  ln -sf libopenblas.a libcblas.a
  ln -sf libcblas.so.3 libcblas.so
  for symlink in *; do
    if [ -L $symlink ] ; then
      ln -sf $(basename $(readlink $symlink)) $symlink
    fi
  done

}

# vim:set ts=2 sw=2 et:
