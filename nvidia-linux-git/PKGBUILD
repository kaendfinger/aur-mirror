# Maintainer: Boohbah <boohbah at gmail.com>
# Contributor: Thomas Baechler <thomas at archlinux.org>

pkgname=nvidia-linux-git
pkgver=313.26
_extramodules=extramodules-3.9-git
pkgrel=1
pkgdesc="NVIDIA drivers for linux-git."
arch=('i686' 'x86_64')
url="http://www.nvidia.com/"
depends=('linux-git>=20121212' "nvidia-utils=$pkgver")
makedepends=('linux-git-headers>=20121212')
conflicts=('nvidia-96xx' 'nvidia-173xx' 'nvidia-304xx')
license=('custom')
install=nvidia.install
options=(!strip)

if [ "$CARCH" = "i686" ]; then
    _arch='x86'
    _pkg="NVIDIA-Linux-$_arch-$pkgver"
    source=("ftp://download.nvidia.com/XFree86/Linux-$_arch/$pkgver/$_pkg.run")
    md5sums=('3c2f5138d0fec58b27e26c5b37d845b8')
elif [ "$CARCH" = "x86_64" ]; then
    _arch='x86_64'
    _pkg="NVIDIA-Linux-$_arch-$pkgver-no-compat32"
    source=("ftp://download.nvidia.com/XFree86/Linux-$_arch/$pkgver/$_pkg.run")
    md5sums=('2d35124fa5a4b009f170fe893b5d07e3')
fi

build() {
    _kernver="$(cat /usr/lib/modules/$_extramodules/version)"
    cd "$srcdir"
    sh "$_pkg.run" --extract-only
    cd "$_pkg/kernel"
    make SYSSRC=/usr/src/"linux-$_kernver/" module
}

package() {
    install -D -m644 "$srcdir/$_pkg/kernel/nvidia.ko" \
        "$pkgdir/usr/lib/modules/$_extramodules/nvidia.ko"
    install -d -m755 "$pkgdir/usr/lib/modprobe.d"
    echo "blacklist nouveau" >> "$pkgdir/usr/lib/modprobe.d/$pkgname.conf"
    sed -i -e "s/EXTRAMODULES='.*'/EXTRAMODULES='$_extramodules'/" "$startdir/nvidia.install"
    gzip "$pkgdir/usr/lib/modules/$_extramodules/nvidia.ko"
}

# vim:set ts=2 sw=2 et:
