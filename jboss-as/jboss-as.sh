# Home of JBoss AS
export JBOSS_HOME=/usr/share/jboss-as

# Application server mode
export JBOSS_AS_MODE=standalone

# Configuration file
export JBOSS_CONFIG=standalone.xml

# AS user
export JBOSS_USER=jboss-as

# PID file
export JBOSS_PIDFILE=/run/jboss-as/jboss-as.pid

# Console log
export JBOSS_CONSOLE_LOG=/var/log/jboss-as/console.log

# Run in background
export LAUNCH_JBOSS_IN_BACKGROUND=1

