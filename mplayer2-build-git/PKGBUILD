# Maintainer: Gustavo Alvarez <s1lpkn07@gmail.com>

pkgname=mplayer2-build-git
pkgver=20130225
pkgrel=1
pkgdesc="A movie player for linux (uses statically linked libav). (GIT version)"
arch=('i686' 'x86_64')
depends=('a52dec'
         'desktop-file-utils'
         'directfb'
         'enca'
         'faad2'
         'fribidi'
         'jack'
         'lcms2'
         'libbluray'
         'libcaca'
         'libcdio-paranoia'
         'libdca'
         'libgl'
         'libmng'
         'libdv'
         'libdvdnav'
         'libmad'
         'libpulse'
         'libquvi'
         'libtheora'
         'libvdpau'
         'libxinerama'
         'libxss'
         'libxxf86vm'
         'lirc-utils'
         'mpg123'
         'nut-multimedia-git'
         'portaudio'
         'rsound'
         'smbclient'
         'speex'
         'ttf-dejavu'
         'xvidcore')
license=('GPL')
url="http://www.mplayer2.org/"
makedepends=('git'
             'live-media'
             'mesa'
             'python'
             'python-docutils'
             'vstream-client'
             'yasm')
backup=('etc/mplayer/codecs.conf'
        'etc/mplayer/input.conf')
provides=('mplayer' 'mplayer2')
conflicts=('mplayer' 'mplayer2')
replaces=('mplayer' 'mplayer2')
options=('!emptydirs')
source=('http://ftp.mplayer2.org/pub/release/mplayer2-2.0.tar.xz')
noextract=('mplayer2-2.0.tar.xz')
md5sums=('b880ae4be0e5b9693cdecf97c84b74f3')
install="$pkgname.install"

_gitroot="git://git.mplayer2.org/mplayer2-build.git"
_gitname="mplayer2-build"

build() {
  cd "${srcdir}"

  if [ -d "${_gitname}" ]; then
    msg "Clean and Update repository"
    cd "${_gitname}"
    ./clean
    ./update
  else
    msg "Clone Repository"
    git clone "${_gitroot}" && cd "${_gitname}"
    ./init --shallow
  fi

  rm -fr "${srcdir}/${_gitname}-build"
  cp -R "${srcdir}/${_gitname}" "${srcdir}/${_gitname}-build"
  cd "${srcdir}/${_gitname}-build"

  msg "Add language PO files from mplayer2-2.0.tar.xz"
  bsdtar -xf "${srcdir}/mplayer2-2.0.tar.xz" mplayer2-2.0/po
  mv mplayer2-2.0/po mplayer && rm -fr mplayer2-2.0
  msg2 "Done"

  msg "Configure Mplayer2"
  # Make Mplayer2 build flags
  echo "--confdir=/etc/mplayer
--enable-translation
--language=all
--language-doc=all
--language-man=all
--language-msg=all
--prefix=/usr
" > mplayer_options
  msg2 "Done"

  msg "Starting Make Mplayer2"
  make
  msg2 "Done"
}

package() {
  msg "Install Mplayer2"
  cd "${srcdir}/${_gitname}-build"
  make DESTDIR="${pkgdir}" install
  install -Dm644 mplayer/etc/{codecs,input,example}.conf "${pkgdir}/etc/mplayer/"
  install -d "${pkgdir}/usr/share/"{mplayer,applications}
  ln -s /usr/share/fonts/TTF/DejaVuSans.ttf "${pkgdir}/usr/share/mplayer/subfont.ttf"
  install -Dm644 mplayer/etc/mplayer.desktop "${pkgdir}/usr/share/applications/mplayer.desktop"
  install -Dm644 mplayer/etc/mplayer.xpm "${pkgdir}/usr/share/pixmaps/mplayer.xpm"
  sed -e 's|gmplayer|mplayer|' -e 's|GTK;||' -i "${pkgdir}/usr/share/applications/mplayer.desktop"
  echo 'NoDisplay=true' >> "${pkgdir}/usr/share/applications/mplayer.desktop"
  msg2 "Done" 
}
