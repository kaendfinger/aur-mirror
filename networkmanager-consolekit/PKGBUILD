# Maintainer: Lukas Jirkovsky <l.jirkovsky@gmail.com>
# Contributor: Jan Alexander Steffens (heftig) <jan.steffens@gmail.com>
# Contributor: Jan de Groot <jgc@archlinxu.org>
# Contributor: Wael Nasreddine <gandalf@siemens-mobiles.org>
# Contributor: Tor Krill <tor@krill.nu>
# Contributor: Will Rea <sillywilly@gmail.com>
# Contributor: Valentine Sinitsyn <e_val@inbox.ru>

pkgname=networkmanager-consolekit
_pkgname=NetworkManager
pkgver=0.9.6.4
pkgrel=1
pkgdesc="NetworkManager with ConsoleKit support for non-systemd systems"
arch=('i686' 'x86_64')
license=('GPL')
url="http://www.gnome.org/projects/$_pkgname/"
depends=('dbus-glib' 'iproute2' 'libnl' 'nss' 'polkit' 'consolekit-git' 'udev' 'wpa_supplicant' 'ppp' 'dhclient' 'libsoup')
makedepends=('intltool' 'dhcpcd' 'iptables' 'gobject-introspection')
optdepends=('modemmanager: for modem management service'
            'dhcpcd: alternative DHCP client; does not support DHCPv6'
            'iptables: Connection sharing'
            'dnsmasq: Connection sharing'
            'bluez: Bluetooth support'
            'openresolv: resolvconf support')
provides=('networkmanager')
conflicts=('networkmanager')
options=('!libtool')
backup=('etc/NetworkManager/NetworkManager.conf')
install=networkmanager.install
source=(http://ftp.gnome.org/pub/gnome/sources/$_pkgname/${pkgver:0:3}/$_pkgname-$pkgver.tar.xz
        NetworkManager.conf disable_set_hostname.patch dnsmasq-path.patch
        nm-polkit-permissive.patch repair-signals.patch systemd-sleep.patch)
sha256sums=('511b411e055d187bc8f26c519fdb3e55e07fc40d4adecbbec623c0249380a7eb'
            '44b048804c7c0b8b3b0c29b8632b6ad613c397d0a1635ec918e10c0fbcdadf21'
            '25056837ea92e559f09563ed817e3e0cd9333be861b8914e45f62ceaae2e0460'
            '65124505048cc8396daf0242c9f5d532fa669b4bbca305998c248ab2329490cb'
            'e23b651a90ef62d515921953b4c6779538d35a737fb74c266b7fce94d62fdb06'
            'a1c65a09e5e8ecb1004ab7922517aa7fbc9700dc61e3b2fd348d3d7c23808ce6'
            '24c181ab69e769e703e45f64a552add5df5de22fbcd8899d588add053674420a')

build() {
  cd $_pkgname-$pkgver

  patch -Np1 -i ../disable_set_hostname.patch
  patch -Np1 -i ../dnsmasq-path.patch
  patch -Np1 -i ../nm-polkit-permissive.patch
  patch -Np1 -i ../repair-signals.patch # FS#31115
  patch -Np1 -i ../systemd-sleep.patch
  AUTOPOINT="intltoolize -f -c --automake" autoreconf -fi

  ./configure \
    --prefix=/usr \
    --sysconfdir=/etc \
    --localstatedir=/var \
    --libexecdir=/usr/lib/networkmanager \
    --with-crypto=nss \
    --with-distro=arch \
    --with-dhclient=/usr/sbin/dhclient \
    --with-dhcpcd=/usr/sbin/dhcpcd \
    --with-iptables=/usr/sbin/iptables \
    --with-systemdsystemunitdir=/usr/lib/systemd/system \
    --with-udev-dir=/usr/lib/udev \
    --with-resolvconf=/usr/sbin/resolvconf \
    --with-session-tracking=ck \
    --disable-static \
    --enable-more-warnings=no \
    --disable-wimax

  make
}

package() {
  cd $_pkgname-$pkgver
  make DESTDIR="$pkgdir" install

  install -m644 ../NetworkManager.conf "$pkgdir/etc/NetworkManager/"

  rm -r "$pkgdir/var/run"
}
