# Maintainer: Sergey Omelkov <omelkovs at gmail dot com>>

pkgname=gnome-settings-daemon-120dpi
_pkgname=gnome-settings-daemon
pkgver=3.6.4
pkgrel=1
pkgdesc="The GNOME Settings daemon with changed hardcoded DPI value to 120"
arch=('i686' 'x86_64')
license=('GPL')
depends=('dconf' 'gnome-desktop' 'gsettings-desktop-schemas' 'hicolor-icon-theme' 'libcanberra-pulse' 'libnotify'
         'libsystemd' 'libwacom' 'nss' 'pulseaudio' 'pulseaudio-alsa' 'upower')
makedepends=('intltool' 'xf86-input-wacom' 'libxslt' 'docbook-xsl')
options=('!emptydirs' '!libtool')
install=gnome-settings-daemon.install
url="http://www.gnome.org"
groups=('gnome')
provides=('gnome-settings-daemon')
conflicts=('gnome-settings-daemon')
source=(http://ftp.gnome.org/pub/gnome/sources/$_pkgname/${pkgver%.*}/$_pkgname-$pkgver.tar.xz
        https://projects.archlinux.org/svntogit/packages.git/plain/gnome-settings-daemon/trunk/0001-power-and-media-keys-Use-logind-for-suspending-and-r.patch
        0002-set-dpi-to-120.patch)
md5sums=('362803ee1f1a0aa02e3c7df61ef82309'
         'c83bd9c1b03ee4d80a64c9bf9f931f34'
         '90d41fc4961b2726099752573cee4d76')
         
build() {
  cd $_pkgname-$pkgver

  # logind key handling FS#31801, patch from Fedora
  # rebased onto screensaver.patch
  patch -Np1 -i ../0001-power-and-media-keys-Use-logind-for-suspending-and-r.patch
  patch -Np1 -i ../../0002-set-dpi-to-120.patch

  aclocal
  autoconf
  automake --add-missing

  ./configure --prefix=/usr --sysconfdir=/etc --localstatedir=/var \
      --libexecdir=/usr/lib/gnome-settings-daemon --disable-static \
      --enable-systemd --disable-ibus

  #https://bugzilla.gnome.org/show_bug.cgi?id=656231
  sed -i -e 's/ -shared / -Wl,-O1,--as-needed\0/g' libtool

  make
}

package() {
  cd $_pkgname-$pkgver
  make DESTDIR="$pkgdir" install

  # Plugins that aren't installed still have schema references
  # and cause gsettings errors - remove the references we're not using
  sed -i '/org\.gnome\.settings-daemon\.plugins\.updates/d' \
    "$pkgdir/usr/share/glib-2.0/schemas/org.gnome.settings-daemon.plugins.gschema.xml"
}


