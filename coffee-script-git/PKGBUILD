# Contributor: Stephen Diehl <sdiehl at clarku dot edu>
# Maintainer: Stefan Husmann <stefan-husmann@t-online.de>

pkgname=coffee-script-git
pkgver=20121209
pkgrel=1
pkgdesc='CoffeeScript is a little language that compiles into JavaScript.'
arch=any
url=http://coffeescript.org
license=('custom')
depends=('nodejs')
provides=('coffee-script')
conflicts=('coffee-script')

_gitroot=(https://github.com/jashkenas/coffee-script.git)
_gitname='coffee-script'

build() {
  cd "$srcdir"
  msg "Connecting to the coffee-script git repository..."

  if [ -d "$srcdir/$_gitname" ] ; then
    cd $_gitname && git pull origin
    msg "The local files are updated."
  else
    git clone $_gitroot
  fi

  msg "GIT checkout done or server timeout"
  msg "Starting make..."

  cd "$srcdir"

  [ -d $_gitname-build ] && rm -rf $_gitname-build
  git clone $_gitname $_gitname-build
}

package() {
  cd $_gitname-build
  install -d $pkgdir/usr/lib/node_modules
  bin/cake --prefix "$pkgdir/usr/" install
  cp -r $pkgdir/usr/lib/coffee-script $pkgdir/usr/lib/node_modules/
  rm -rf $pkgdir/usr/lib/coffee-script
  install -Dm644 LICENSE "$pkgdir/usr/share/licenses/${pkgname}/LICENSE"
  install -Dm644 README "$pkgdir/usr/share/doc/${pkgname}/README"
  rm -rf $pkgdir/usr/bin/cake $pkgdir/usr/bin/coffee &&
  ln -s /usr/lib/node_modules/coffee-script/bin/coffee $pkgdir/usr/bin/coffee &&
  ln -s /usr/lib/node_modules/coffee-script/bin/cake $pkgdir/usr/bin/cake
}
