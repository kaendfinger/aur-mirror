# Contributor: Tom < reztho at archlinux dot us >

pkgname=hostapd-git
pkgver=20121212
pkgrel=1
pkgdesc='Daemon for wireless software access points. Git version.'
arch=('i686' 'x86_64')
url='http://hostap.epitest.fi/hostapd/'
license=('custom')
depends=('openssl' 'libnl')
provides=('hostapd')
conflicts=('hostapd')
install=hostapd.install
backup=('etc/hostapd/hostapd.accept'
        'etc/hostapd/hostapd.conf'
        'etc/hostapd/hostapd.deny'
        'etc/hostapd/hostapd.eap_user'
        'etc/hostapd/hostapd.radius_clients'
        'etc/hostapd/hostapd.sim_db'
        'etc/hostapd/hostapd.vlan'
        'etc/hostapd/hostapd.wpa_psk'
        'etc/hostapd/wired.conf'
        'etc/hostapd/hlr_auc_gw.milenage_db')

_gitroot='git://w1.fi/srv/git/hostap.git'
_gitname='hostap'

source=('config'
	'hostapd'
	'hostapd.conf.d'
	'hostapd.service')
md5sums=('5d7ee10b04e33f22c37be56a4c33dddb'
         'd570327c385f34a4af24d3a0d61cea19'
         'f169534b0f59b341f6df1a21e0344511'
         'a0a16879eed5e4e41ae6b225a4809955')

build() {
  cd ${srcdir}
  msg "Connecting to GIT server...."
  if [ -d "${srcdir}/${_gitname}" ] ; then
    cd ${srcdir}/${_gitname} && git pull origin
    msg "The local files are updated."
  else
    git clone ${_gitroot}
  fi

  msg "GIT checkout done or server timeout"
  msg "Starting make..."

  rm -rf "${srcdir}/${_gitname}-build"
  git clone ${srcdir}/${_gitname} ${srcdir}/${_gitname}-build

  cd ${srcdir}/${_gitname}-build/hostapd

  cp ${srcdir}/config ./.config
  sed -i 's#/etc/hostapd#/etc/hostapd/hostapd#' hostapd.conf
  export CFLAGS="$CFLAGS $(pkg-config --cflags libnl-3.0)"
  make
}

package() {
  cd ${srcdir}/${_gitname}-build/hostapd

  # RC script
  install -Dm755 $srcdir/hostapd "$pkgdir/etc/rc.d/hostapd"
  install -Dm644 $srcdir/hostapd.conf.d "$pkgdir/etc/conf.d/hostapd"

  # Systemd unit
  install -Dm644 $srcdir/hostapd.service "$pkgdir/usr/lib/systemd/system/hostapd.service"

  # License
  install -Dm644 ../COPYING "$pkgdir/usr/share/licenses/$pkgname/COPYING"

  # Binaries
  install -d "$pkgdir/usr/bin"
  install -t "$pkgdir/usr/bin" hostapd hostapd_cli

  # Configuration
  install -d "$pkgdir/etc/hostapd"
  install -m644 -t "$pkgdir/etc/hostapd" \
    hostapd.{accept,conf,deny,eap_user,radius_clients,sim_db,vlan,wpa_psk} \
    wired.conf hlr_auc_gw.milenage_db

  # Man pages
  install -Dm644 hostapd.8 "$pkgdir/usr/share/man/man8/hostapd.8"
  install -Dm644 hostapd_cli.1 "$pkgdir/usr/share/man/man1/hostapd_cli.1"
}
