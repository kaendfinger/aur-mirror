# Contributor: Olivier Mehani <olivier.mehani@nicta.com.au>
pkgname=oml2
_redmineid=792
pkgver=2.9.0
pkgrel=1
pkgdesc="OML is a measurement library that allows to define measurement points inside applications"
arch=(i686 x86_64)
url="http://oml.mytestbed.net/"
license=('custom:MIT-Nicta')
depends=('libxml2' 'sqlite3' 'popt' 'ruby>=1.8.7' 'postgresql-libs')
makedepends=('git' 'asciidoc')
optdepends=("postgresql: to use as a backend instead of sqlite3")
checkdepends=('check')
provides=(liboml liboml-git oml2)
conflicts=(liboml liboml-git oml2)
replaces=(liboml-git)
source=(
	http://oml.mytestbed.net/attachments/download/${_redmineid}/oml2-${pkgver}.tar.gz
	oml2-server.rc
	oml2-server.conf
)
backup=(etc/conf.d/oml2-server.conf)
install="oml2.install"

_builddir=${pkgname}-${pkgver}

build() {
	cd "${srcdir}"
	cd "${srcdir}/${_builddir}"
	./configure --prefix=/usr --localstatedir=/var --enable-doc --disable-doxygen-doc --with-pgsql
	make || return 1
}

package() {
	cd "${srcdir}/${_builddir}"
	make DESTDIR="${pkgdir}/" install
	install -D -m 0644 ${srcdir}/${_builddir}/COPYING \
		${pkgdir}/usr/share/licenses/${pkgname}/COPYING
	install -D -m 0755 ${startdir}/oml2-server.rc ${pkgdir}/etc/rc.d/oml2-server
	install -D -m 0644 ${startdir}/oml2-server.service ${pkgdir}/usr/lib/systemd/system/oml2-server.service
	install -D -m 0644 ${startdir}/oml2-server.conf ${pkgdir}/etc/conf.d/oml2-server.conf
	install -D -m 0644 ${startdir}/oml2-server.logrotate ${pkgdir}/etc/logrotate.d/oml2-server
}

check() {
	cd "${srcdir}/${_builddir}"
	make check
}
md5sums=('15a7cc145c109a56412903c5ae920071'
         '3d783cf3f03d339d5a1607325ee71b0f'
         '0fcaf350b4c5ac5bc8db2f17de11f9f9')
