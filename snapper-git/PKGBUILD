# Maintainer: Tom Kuther <gimpel@sonnenkinder.org>

pkgname=snapper-git
pkgver=20130206
pkgrel=1
pkgdesc="A tool for managing btrfs snapshots. It can compare snapshots and revert differences between snapshots."
provides=('snapper')
conflicts=('snapper')
arch=('i686' 'x86_64')
url="http://en.opensuse.org/Portal:Snapper"
license=('GPL2')
depends=('libxml2' 'openssl' 'pcre' 'python' 'dbus')
makedepends=('boost' 'swig' 'automake-1.11')
backup=('etc/conf.d/snapper')
source=()
md5sums=()
install=snapper.install

_gitroot='https://github.com/openSUSE/snapper.git'
_gitname='snapper'

build() {
  cd "$srcdir"
  msg "Connecting to GIT server...."

  if [[ -d "$_gitname" ]]; then
    cd "$_gitname" && git pull origin
    msg "The local files are updated."
  else
    git clone "$_gitroot" "$_gitname"
  fi

  msg "GIT checkout done or server timeout"
  msg "Starting build..."

  rm -rf "$srcdir/$_gitname-build"
  git clone "$srcdir/$_gitname" "$srcdir/$_gitname-build"
  cd "$srcdir/$_gitname-build"

  # use /etc/conf.d instead of /etc/sysconfig	
  sed -i -e 's@/etc/sysconfig@/etc/conf.d@g' \
      snapper/SnapperDefines.h \
      scripts/* \
      doc/*
  # don't install zypper plugin
  sed -i '/usr\/lib\/zypp/d' scripts/Makefile.am
  # rename cron scripts
  sed -i -e 's@suse.de-@@g' scripts/Makefile.am
  
  # boost fixlets
  # don't link against non-existant libbost_thread-mt
  sed -i -e 's@lboost_thread-mt@lboost_thread@g' snapper/Makefile.am
  # add -lboost_system
  sed -E -i 's@snapperd_LDADD(.*)@snapperd_LDADD\1 \-lboost_system@g' server/Makefile.am
  sed -E -i 's@snapper_LDADD(.*)@snapper_LDADD\1 \-lboost_system@g' client/Makefile.am

  aclocal-1.11
  libtoolize --force --automake --copy
  autoheader
  automake-1.11 --add-missing --copy
  autoconf
  export CPPFLAGS="`pkg-config --cflags python3`"
  ./configure --prefix=/usr
  make
}

package() {
  cd ${_gitname}-build
  msg2 "Running make install"
  make DESTDIR=${pkgdir} install
  mkdir ${pkgdir}/etc/conf.d
  install -D -m 644 data/sysconfig.snapper ${pkgdir}/etc/conf.d/snapper
}

# vim:set ts=2 sw=2 et:
