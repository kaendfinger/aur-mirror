# Maintainer:  Vojtech Aschenbrenner     <v@asch.cz>
# Contributor: Jason Gardner             <buhrietoe@gmail.com>
# Contributor: Ross melin                <rdmelin@gmail.com>
# Maintainer (Parabola): Márcio Silva    <coadde@lavabit.com>
# Contributor (Parabola): André Silva    <emulatorman@lavabit.com>

# based of debian squeeze package

pkgbase=zoneminder
pkgname=zoneminder
pkgver=1.25.0
pkgrel=24
pkgdesc='Capture, analyse, record and monitor video security cameras'
arch=(
  i686
  x86_64
  mips64el
  arm
)
backup=(
  etc/zm.conf
)
url="http://www.$pkgbase.com"
license=(
  GPL
)
depends=(
  apache
  cambozola
  libav-static
  gnutls
  mariadb
  perl-archive-zip
  perl-date-manip
  perl-dbd-mysql
  perl-dbi
  perl-expect
  perl-libwww
  perl-mime-lite
  perl-mime-tools
  perl-php-serialization
  perl-net-sftp-foreign
  perl-sys-mmap
  perl-time-modules
  perl-x10
  php
  php-apache
  php-gd
  php-mcrypt
)
makedepends=(
  netpbm
)
optdepends=(
  netpbm
)
install=$pkgbase.install
source=(
  http://www.$pkgbase.com/downloads/ZoneMinder-$pkgver.tar.gz
  httpd-$pkgbase.conf
  $pkgbase
  $pkgbase.service
)
sha512sums=(
  3e18993b0539729491052c97d8c94227ccc089eb40277c2f07682f30049033303c7cfe9734fdac6d33ae67df29c76eb72bf7fbb5dae8227e8831fa603b61c375
  4ce0d8eba9d006d258f5b8a83920fc17f1f602b96518d37b7a47cd9b6eb84ef2587641a6ba839a469c3f0e33b46475866187279ae3f8be0d4054b074ee5d6b08
  ab4e1d5ddaf4d9cd53d6ca59d7965902afd6a2dc830fbbafa270736c52c2b3563075fee860bb0276466f96e9dbfb71b259ac45a4ae2e4ead8eaec154a0159eb0
  cfb0eb87a989236c72741a496ddc6a73aa2696e5beaaca4836d3c231ddb24c7ef5e9f65e7afa49674f2115cbfa4a07c75486e1947ce294c816ddbb875f3b99cf
)

build() {
  cd $srcdir/ZoneMinder-$pkgver

  # ZM_RUNDIR need change to run dir
  export CPPFLAGS=-D__STDC_CONSTANT_MACROS\
    OPT_FFMPEG=yes\
    PATH_FFMPEG=/usr/bin/avconv-static\
    ZM_LOGDIR=/var/log/$pkgbase\
    ZM_RUNDIR=/tmp/$pkgbase\
    ZM_SSL_LIB=gnutls\
    ZM_TMPDIR=/tmp/$pkgbase

  # Patch for GCC 4.7.x
  sed -i -e 's/^#include <errno.h>/#include <errno.h>\n#include <unistd.h>/'\
     src/zm_logger.cpp || read
  sed -i -e 's/^#include <pthread.h>/#include <pthread.h>\n#include <unistd.h>/'\
     src/zm_thread.h || read

  # Patch for automake 1.12
  sed -i -e '/am__api_version=/ s/1.11/1.12/'\
    configure || read

  # Patch for disable ZM_CHECK_FOR_UPDATES
  sed -i -e '/ZM_CHECK_FOR_UPDATES/,+1 s/yes/no/'\
    scripts/ZoneMinder/lib/ZoneMinder/ConfigData.pm.in || read

  # Patch for support html5 video and flv
  sed -i -e '/ZM_MPEG_LIVE_FORMAT/,+1 s/swf/webm/;/ZM_MPEG_REPLAY_FORMAT/,+1 s/swf/webm/;
    /ZM_FFMPEG_FORMATS/,+1 s/mpg mpeg wmv asf avi\* mov swf 3gp\*\*/mpg mpeg wmv asf avi\* mov flv swf 3gp\*\* webm ogg h264/'\
    scripts/ZoneMinder/lib/ZoneMinder/ConfigData.pm.in || read

  # Patch for enable video export
  #sed -i -e '/ZM_OPT_FFMPEG/,+1 s/@OPT_FFMPEG@/yes/;
  #  /ZM_PATH_FFMPEG/,+1 s/@PATH_FFMPEG@/\/usr\/bin\/avconv-static/'\
  #  scripts/ZoneMinder/lib/ZoneMinder/ConfigData.pm.in || read

  # Patch for change path run dir
  sed -i -e '/ZM_PATH_SOCKS/,+1 s/TMP/RUN/'\
    scripts/ZoneMinder/lib/ZoneMinder/ConfigData.pm.in || read

  # Patch for change ZM name to ZoneMinder
  sed -i -e '/ZM_WEB_TITLE_PREFIX/,+1 s/"ZM"/"ZoneMinder"/'\
    scripts/ZoneMinder/lib/ZoneMinder/ConfigData.pm.in || read

  # Patch for v4l2 convert support
  sed -i -e "s/$ENV{SHELL} = \'\/bin\/sh\' if exists $ENV{SHELL};/$ENV{SHELL} = \'\/bin\/sh\' if exists $ENV{SHELL};\n$ENV{LD_PRELOAD} = \'\/usr\/lib\/libv4l\/v4l2convert.so\' ;/"\
    scripts/zmdc.pl.in || read

  # Patch for add more socket tries
  sed -i -e '/$max_socket_tries/ s/3/15/'\
    web/ajax/stream.php || read

  # Patch for wrong "suppported"
  sed -i -e 's/suppported/supported/'\
     src/zm_local_camera.cpp || read

  # Patch for type cast in linux-libre kernel 3.5
  sed -i -e 's/enum v4l2_buf_type type = v4l2_data.fmt.type;/enum v4l2_buf_type type = (v4l2_buf_type)v4l2_data.fmt.type;/'\
    src/zm_local_camera.cpp || read

  # Patch for drop custom perl install paths
  sed -i -e '/# Slight hack for non-standard perl install paths/,+10 d;
    s/^AC_SUBST(ZM_CONFIG,"$SYSCONFDIR\/zm.conf")/AC_SUBST(ZM_CONFIG,"$SYSCONFDIR\/zm.conf")\n\nEXTRA_PERL_LIB="# Include from system perl paths only"\nPERL_MM_PARMS="INSTALLDIRS=vendor"/'\
    configure.ac

  # Patch for support ffmpeg with <libavutil/mathematics.h> and C library
  sed -i -e 's/^extern "C" {/extern "C" {\n#ifdef _STDINT_H\n#undef _STDINT_H\n#endif\n#include <stdint.h>/
    s/^#include <libavutil\/avutil.h>/#include <libavutil\/avutil.h>\n#include <libavutil\/mathematics.h>/'\
    src/zm_ffmpeg.h
  sed -i -e 's/^AM_CONFIG_HEADER(config.h)/AM_CONFIG_HEADER(config.h)\n\nAC_SUBST([AM_CXXFLAGS], [-D__STDC_CONSTANT_MACROS])/;
    s/^AC_CHECK_HEADERS(mysql\/mysql.h,,AC_MSG_ERROR(zm requires MySQL headers - check that MySQL development packages are installed),)/AC_CHECK_HEADERS(mysql\/mysql.h,,AC_MSG_ERROR(zm requires MySQL headers - check that MySQL development packages are installed),)\nAC_LANG_PUSH([C])/;
    s/^AC_CHECK_HEADERS(libswscale\/swscale.h,,,)/AC_CHECK_HEADERS(libswscale\/swscale.h,,,)\nAC_LANG_POP([C])/'\
    configure.ac

  # Patch for wrong install run, tmp and log dir
  sed -i -e '/ install-data-hook/d;/install-data-hook:/d;/# Yes, you are correct. This is a HACK!/d;
    /	( cd $(DESTDIR)$(sysconfdir); chown $(webuser):$(webgroup) $(sysconf_DATA); chmod 600 $(sysconf_DATA) )/d;
    /	( if ! test -e $(ZM_RUNDIR); then mkdir -p $(ZM_RUNDIR); fi; if test "$(ZM_RUNDIR)" != "\/var\/run"; then chown $(webuser):$(webgroup) $(ZM_RUNDIR); chmod u+w $(ZM_RUNDIR); fi )/d;
    /	( if ! test -e $(ZM_TMPDIR); then mkdir -m 700 -p $(ZM_TMPDIR); fi; if test "$(ZM_TMPDIR)" != "\/tmp"; then chown $(webuser):$(webgroup) $(ZM_TMPDIR); chmod u+w $(ZM_TMPDIR); fi )/d;
    /	( if ! test -e $(ZM_LOGDIR); then mkdir -p $(ZM_LOGDIR); fi; if test "$(ZM_LOGDIR)" != "\/var\/log"; then chown $(webuser):$(webgroup) $(ZM_LOGDIR); chmod u+w $(ZM_LOGDIR); fi )/,+1 d'\
    Makefile.{am,in}

  ./configure --prefix=/usr\
    --enable-crashtrace=no\
    --enable-debug=no\
    --enable-mmap=yes\
    --sysconfdir=/etc\
    --with-cgidir=/srv/http/cgi-bin\
    --with-extralibs='-L/usr/lib -L/usr/lib/mysql'\
    --with-ffmpeg=/opt/libav-static\
    --with-libarch=lib\
    --with-mysql=/usr\
    --with-webdir=/srv/http/$pkgbase\
    --with-webgroup=http\
    --with-webhost=localhost\
    --with-webuser=http

  make V=0
}

package() {
  cd $srcdir/ZoneMinder-$pkgver

  make DESTDIR=$pkgdir install

  mkdir -p $pkgdir/{etc/{httpd/conf/extra,rc.d},srv/http/{cgi-bin,$pkgbase},usr/{lib/systemd/system,share/{license/$pkgbase,$pkgbase/db}},var/{cache/$pkgbase,log/$pkgbase}}

  chown -R http.http $pkgdir/{etc/zm.conf,var/{cache/$pkgbase,log/$pkgbase}}
  chmod 0700 $pkgdir/etc/zm.conf

  for i in events images temp; do
    mv    $pkgdir/srv/http/$pkgbase/$i $pkgdir/var/cache/$pkgbase/$i
    ln -s /var/cache/$pkgbase/$i       $pkgdir/srv/http/$pkgbase/$i
    chown -h http.http                 $pkgdir/srv/http/$pkgbase/$i
  done

  ln -s /srv/http/cgi-bin                  $pkgdir/srv/http/$pkgbase
  chown -h http.http                       $pkgdir/srv/http/{cgi-bin,$pkgbase,$pkgbase/cgi-bin}

  ln -s /usr/share/cambozola/cambozola.jar $pkgdir/srv/http/$pkgbase

  install -D -m 644 $srcdir/httpd-$pkgbase.conf $pkgdir/etc/httpd/conf/extra
  install -D -m 644 $srcdir/$pkgbase            $pkgdir/etc/rc.d
  install -D -m 644 $srcdir/$pkgbase.service    $pkgdir/usr/lib/systemd/system
  install -D -m 644 COPYING                     $pkgdir/usr/share/license/$pkgbase
  install -D -m 644 db/zm*.sql                  $pkgdir/usr/share/$pkgbase/db
}
