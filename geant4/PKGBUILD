# Maintainer: Luis Sarmiento < lgsarmientop-ala-unal.edu.co >
pkgname='geant4'
pkgdesc="A simulation toolkit for particle physics interactions."
optdepends=('java-environment: for histogram visualizations and analysis'
	    'tcsh: for C Shell support'
	    'python: for G4Python support'
            'geant4-ledata: Data files for low energy electromagnetic processes'
            'geant4-levelgammadata: Data files for photon evaporation'
            'geant4-neutronhpdata: Neutron data files with thermal cross sections'
            'geant4-neutronxsdata: Data files for evaluated neutron cross sections on natural composition of elements'
            'geant4-piidata: Data files for shell ionisation cross sections'
	    'geant4-radioactivedata: Data files for radioactive decay hadronic processes'
	    'geant4-realsurfacedata: Data files for measured optical surface reflectance'
	    'geant4-saiddata: Data files from evaluated cross-sections in SAID data-base'
)
makedepends=('cmake>=2.6')
depends=('cmake' 'xerces-c' 'qt4' 'lesstif' 'mesa')
replaces=('geant4-deb')
pkgver=9.6.1
_pkgver=9.6.p01
pkgrel=2

url="http://geant4.cern.ch/"
arch=('x86_64' 'i686')
license=('custom: http://geant4.cern.ch/license/')
options=('!emptydirs')

source=("http://geant4.cern.ch/support/source/${pkgname}.${_pkgver}.tar.gz")
md5sums=('9e6e08da2a04c122e46835193ed8a96e')

build() {
	[ -d ${srcdir}/build ] && rm -rf ${srcdir}/build
	mkdir ${srcdir}/build
	cd ${srcdir}/build
	cmake -DGEANT4_USE_GDML=ON \
              -DGEANT4_USE_G3TOG4=ON \
              -DGEANT4_USE_QT=ON \
	      -DQT_QMAKE_EXECUTABLE=`which qmake-qt4` \
              -DCMAKE_INSTALL_PREFIX=/usr \
              -DGEANT4_INSTALL_EXAMPLES=ON \
              -DGEANT4_USE_RAYTRACERX=ON \
              -DGEANT4_INSTALL_DATA=OFF \
              -DGEANT4_USE_OPENGL_X11=ON \
              -DGEANT4_RAYTRACER_X11=ON \
              -DGEANT4_SYSTEM_CLHEP=OFF \
              -DGEANT4_XM=ON \
              -DCMAKE_INSTALL_LIBDIR=/usr/lib \
              ../${pkgname}.${_pkgver}

	make
}

package() {

#Since the basic package does not include the data files, their
#configuration should be removed from the configuration file. Data
#files are also available on the AUR and the environment variables are
#set automatically for you from the packages.
  variables=("G4NEUTRONHPDATA" \
    "G4LEDATA" \
    "G4LEVELGAMMADATA" \
    "G4RADIOACTIVEDATA" \
    "G4NEUTRONXSDATA" \
    "G4PIIDATA" \
    "G4REALSURFACEDATA" \
    "G4SAIDXSDATA")
  for _varname in ${variables[*]}
  do
    echo "${_varname} being removed(!) from the configuration script."
    echo "Make sure you added yourself or simply reinstall the dataset"
    echo "you want in case you are upgrading just the package"
    sed -i "/${_varname}/d" ${srcdir}/build/InstallTreeFiles/geant4.sh
    sed -i "/${_varname}/d" ${srcdir}/build/InstallTreeFiles/geant4.csh
  done

  cd ${srcdir}/build
  make DESTDIR="${pkgdir}" install

  echo 'pushd /usr/bin &> /dev/null && source geant4.sh && popd &>/dev/null' > ${srcdir}/geant4.profile.sh
  echo 'pushd /usr/bin &> /dev/null && source geant4.csh && popd &>/dev/null' > ${srcdir}/geant4.profile.csh
  install -d ${pkgdir}/etc/profile.d
  install -m755 ${srcdir}/geant4.profile.sh ${pkgdir}/etc/profile.d/geant4.sh
  install -m755 ${srcdir}/geant4.profile.csh ${pkgdir}/etc/profile.d/geant4.csh
}
