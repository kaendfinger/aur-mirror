# Contributor: Richard Kakaš <richard.kakas@gmail.com>
# Maintainer: Eric Anderson <ejona86@gmail.com>

pkgname=printrun-git
pkgver=20130121
pkgrel=1
pkgdesc='GUI interface for 3D printing on RepRap and other printers'
arch=('any')
url='https://github.com/kliment/Printrun'
license=('GPL')
depends=('python2-pyserial' 'wxpython' 'pyglet')
makedepends=('git')
source=('pronterface.desktop' 'pronterface.png')
md5sums=('3dfd9fca6b36d914d9b60a69dc9d0d14'
         '9010a039260f0ea34f02671025138ab2')

_gitroot='git://github.com/kliment/Printrun.git'
_gitname='Printrun'

build() {
  cd "${srcdir}"
  msg "Connecting to the GIT server...."
  if [ -d ${_gitname} ] ; then
    cd ${_gitname} && git pull origin
    msg "The local files are updated..."
  else
    git clone ${_gitroot} ${_gitname}
  fi
  msg "GIT checkout done or server timeout"

  cd "${srcdir}/${_gitname}"

  python2 setup.py build

  sed 's/^\(#\!.*\)python$/\1python2/' -i \
    build/lib/printrun/calibrateextruder.py \
    build/lib/printrun/gcview.py \
    build/lib/printrun/graph.py \
    build/lib/printrun/SkeinforgeQuickEditDialog.py \
    build/lib/printrun/stlview.py \
    build/lib/printrun/webinterface.py
}

package() {
  cd "${srcdir}/${_gitname}"
  python2 setup.py install --root="${pkgdir}" --prefix=/usr

  install -D -m644 "${srcdir}/pronterface.desktop" \
    "$pkgdir/usr/share/applications/pronterface.desktop"
  install -D -m644 "${srcdir}/pronterface.png" \
    "$pkgdir/usr/share/pixmaps/pronterface.png"
}
