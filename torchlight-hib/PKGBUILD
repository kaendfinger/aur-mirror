# Maintainer: Sam S. <smls75@gmail.com>

pkgname=torchlight-hib
pkgver=1.15_20120926
_hibver=2012-09-26
pkgrel=0
pkgdesc='A hack and slash action role-playing game (Humble Bundle version)'
url='http://www.torchlightgame.com/'
arch=('i686' 'x86_64')
license=('custom:commercial')
depends=('libgl' 'mesa' 'freeglut' 'libxrandr' 'expat' 'libxaw' 'libxft'
         'libxinerama' 'freeimage' 'util-linux' 'zziplib' 'pcre' 'libtxc_dxtn')
source=('torchlight-hib.desktop')
md5sums=('36903fc0f19910870440b68ac4cd22a4')
options=('!strip' '!upx')
PKGEXT='.pkg.tar'

_installer="Torchlight-${_hibver}.sh"
_installer_md5='eff02cb27a818b4d29e399bc8a293d7f'


package() {
  cd $srcdir

  # Get installer
  _get_local_source "${_installer}" --md5 "${_installer_md5}" || {
    error "Unable to find the game archive. Please download it from your Humble
           Bundle page, and place it into one of the above locations."
    exit 1; }

  # Execute installer (it validates its own checksum)
  msg "Starting setup..."
  [[ -d "./temp" ]] && rm -r "./temp"
  sh "./${_installer}" --unattended --no-register --packager pacman \
                       --keep --overwrite --accept-license \
                       --target  "${srcdir}/temp" \
                       --bindir  "${srcdir}/bin" \
                       --datadir "${pkgdir}/opt"

  # Remove bundled libraries & helper binaries (use system packages instead)
  rm -rf "${pkgdir}/opt/Torchlight/xdg-"*

  # Fix permissions
  find "${pkgdir}" -type f -exec chmod 644 "{}" +

  # Install desktop entry
  install -Dm644 "${pkgname}.desktop" \
                 "${pkgdir}/usr/share/applications/${pkgname}.desktop"

  # Install icon
  install -Dm644 "${pkgdir}/opt/Torchlight/Torchlight.png" \
                 "${pkgdir}/usr/share/pixmaps/torchlight.png"

  # Install launcher symlink
  case $CARCH in i686) _arch=x86;; x86_64) _arch=x86_64;; esac
  chmod 755 "${pkgdir}/opt/Torchlight/Torchlight.bin.${_arch}"
  install -d "${pkgdir}/usr/bin"
  ln -s "/opt/Torchlight/Torchlight.bin.${_arch}" "${pkgdir}/usr/bin/torchlight"

  # Install license file
  install -Dm644 "temp/config/license" "${pkgdir}/usr/share/licenses/${pkgname}/LICENSE"
}


###### HELPER FUNCTIONS ######

# Locates a file or folder provided by the user, and symlinks it into $srcdir
_get_local_source() {
  msg "Looking for '$1'..."; rm -f "$srcdir/$1"
  declare -A _search=(['build dir']="$startdir"
                      ['$LOCAL_PACKAGE_SOURCES']="$LOCAL_PACKAGE_SOURCES")
  for _key in "${!_search[@]}"; do local _dir="${_search["$_key"]}"
    echo -n "    - in $_key [${_dir:-<undefined>}] ... ";
    if [[ -z "$_dir" || ! -e "$_dir/$1" ]]; then
      echo "NOT FOUND"
    elif [ "$(${2#--}sum "$_dir/$1"|awk '{print $1}')" != "$3" ]; then
      echo "CHECKSUM FAILED";
    else
      echo "FOUND"; ln -sfT "$(readlink -f "$_dir/$1")" "$srcdir/$1"; break; fi
  done
  if [ ! -e "$srcdir/$1" ]; then return 1; fi
}
