# Maintainer: Boohbah <boohbah at gmail.com>
# Contributor: Eric Belanger <eric at archlinux.org>
# Contributor: Daniel J Griffiths <ghost1227 at archlinux.us>

_pkgname=htop
pkgname=$_pkgname-solarized
pkgver=1.0.2
pkgrel=1
pkgdesc="Interactive process viewer with solarized patch"
arch=('i686' 'x86_64')
url="http://htop.sourceforge.net/"
license=('GPL')
depends=('ncurses')
makedepends=('python2')
optdepends=('lsof: show files opened by a process'
            'strace: attach to a running process')
provides=('htop')
conflicts=('htop')
options=('!emptydirs')
source=("http://downloads.sourceforge.net/$_pkgname/$_pkgname-$pkgver.tar.gz"
        'htop-solarized-patch.diff'
	'tree-crash.patch')
md5sums=('0d01cca8df3349c74569cefebbd9919e'
         'b52c0281e599dde2f111f42ef9683316'
         '48eba3c0303bfd19d761b859bc69d713')

build() {
  cd "$_pkgname-$pkgver"

  sed -i 's|ncursesw/curses.h|curses.h|' RichString.[ch] configure
  sed -i 's|python|python2|' scripts/MakeHeader.py

  # Boost field buffer size - crashes when trying to draw very deep UTF-8 trees
  # Test by nesting 30 shells
  patch -N -i ../tree-crash.patch
  # Solarized patch: https://gist.github.com/alexeiz/4657334
  patch -N -i ../htop-solarized-patch.diff

  ./configure \
      --prefix=/usr \
      --enable-unicode \
      --enable-openvz \
      --enable-vserver \
      --enable-cgroup

  make
}

package() {
  make -C "$_pkgname-$pkgver" DESTDIR="$pkgdir" install
}

# vim:set ts=2 sw=2 et:
