# Contributor: Roman Kyrylych <Roman.Kyrylych@gmail.com>
# Contributor: Benjamin Wild <benwild@gmx.de>
# Contributor: Daniel J Griffiths <ghost1227@archlinux.us>
# Maintainer: Joshua Stiefer <facedelajunk@gmail.com>

pkgname=exaile
pkgver=3.3.1
pkgrel=1
pkgdesc="A full-featured media player for GTK+"
arch=('any')
url="http://www.exaile.org"
license=('GPL')
depends=('python2'
	 'gstreamer0.10-python'
	 'gstreamer0.10-good-plugins'
	 'mutagen'
	 'dbus-python'
	 'pygtk>=2.10'
	 'librsvg')
makedepends=('make' 'help2man')
optdepends=('pycddb: CD metadata retrieval'
            'python2-bsddb: music collection support'
	    'gstreamer0.10-bad-plugins: support for more formats'
	    'gstreamer0.10-ugly-plugins: support for more formats'
	    'gstreamer0.10-ffmpeg: support for more formats' )
source=(https://www.launchpad.net/exaile/3.3.x/${pkgver}/+download/exaile-${pkgver}.tar.gz)
install=$pkgname.install
md5sums=('cd7ea267a9d53cdd5231912f19ae03f8')

build() {
  cd "${srcdir}/${pkgname}-${pkgver}"

	make PREFIX=/usr
}

package() {
	cd "${srcdir}/${pkgname}-${pkgver}"
	make PREFIX=/usr DESTDIR="${pkgdir}" install

	# fix for clicking files with spaces in names from nautilus
	sed -i "s#%u#%f#" "${pkgdir}/usr/share/applications/exaile.desktop"
  sed -i "s|Exec=$pkgdir/*|Exec=/|" "${pkgdir}/usr/share/dbus-1/services/org.exaile.Exaile.service"
}
