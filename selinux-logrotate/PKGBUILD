# $Id: PKGBUILD 171604 2012-11-20 02:56:24Z allan $
# Maintainer: Pierre Schmitz <pierre@archlinux.de>
# Maintainer: Nicky726 <Nicky726@gmail.com>
# Contributor: Judd Vinet <jvinet@zeroflux.org>

pkgname=selinux-logrotate
_origname=logrotate
pkgver=3.8.2
pkgrel=2
pkgdesc="Tool to rotate system logs automatically with SELinux support"
arch=('i686' 'x86_64')
url="https://fedorahosted.org/logrotate/"
license=('GPL')
groups=('selinux' 'selinux-system-utilities')
depends=('popt' 'selinux-cronie' 'gzip' 'selinux-usr-libselinux')
conflicts=("${_origname}")
provides=("${_origname}=${pkgver}-${pkgrel}")
backup=('etc/logrotate.conf')
source=("https://fedorahosted.org/releases/l/o/logrotate/logrotate-${pkgver}.tar.gz"
        'noasprintf.patch'
        'paths.patch'
        'logrotate.conf'
        'logrotate.cron.daily')
md5sums=('ddd4dcf28c38b3ac6bc6ff4e0148308a'
         'cd76976b5ce37d328b452c806b55a015'
         'e76526bcd6fc33c9d921e1cb1eff1ffb'
         '86209d257c8b8bc0ae34d6f6ef057c0f'
         'aa8ac8283908b6114483a293adcb650f')

build() {
  cd "$srcdir/${_origname}-${pkgver}"

  patch -p0 -i "$srcdir/noasprintf.patch"
  patch -p0 -i "$srcdir/paths.patch"

  make RPM_OPT_FLAGS="$CFLAGS" EXTRA_LDFLAGS="$LDFLAGS" WITH_SELINUX=yes
}

check() {
  cd "$srcdir/${_origname}-${pkgver}"
  make test
}

package() {
  cd "$srcdir/${_origname}-${pkgver}"
  make PREFIX="$pkgdir" MANDIR="/usr/share/man" install

  install -Dm644 "$srcdir/logrotate.conf" "$pkgdir/etc/logrotate.conf"
  install -Dm744 "$srcdir/logrotate.cron.daily" "$pkgdir/etc/cron.daily/logrotate"
}
