# Maintainer: Alain Kalker <a.c.kalker@gmail.com>
# Contributor: jedahu <jedahu@gmail.com>
# Contributor: Leonidas <marek@xivilization.net>
pkgname=factor-git
pkgver=20130302
pkgrel=2
pkgdesc="A general purpose, dynamically typed, stack-based programming language"
arch=(i686 x86_64)
url="http://factorcode.org"
license=(BSD)
provides=(factor)
conflicts=(factor)
depends=(pango cairo glib2 freetype2 mesa libgl gtkglext)
optdepends=(udis86)
makedepends=(git)
options=(!strip)
source=(factor.desktop
        factor.svg)
md5sums=('59242ddb19a9be927915e489e2bfca27'
         'a6c664f3837713a8e558e4740d78db90')

_gitroot="git://factorcode.org./git/factor.git"
_gitname="factor"

build() {
  cd "$srcdir"
  msg "Connecting to GIT server...."

  if [[ -d "$_gitname" ]]; then
    cd "$srcdir/$_gitname"
    # update done by build-support/factor.sh
    msg "Running factor update script...."
    build-support/factor.sh quick-update
    #build-support/factor.sh update
  else
    git clone "$_gitroot" "$_gitname"
    msg "GIT checkout done or server timeout"
    msg "Starting build..."

    cd "$srcdir/$_gitname"
    # build done by build-support/factor.sh
    msg "Running factor bootstrap script...."
    build-support/factor.sh net-bootstrap
  fi
}

package() {
    mkdir -p $pkgdir/usr/bin
    mkdir -p $pkgdir/usr/lib/factor
    mkdir -p $pkgdir/usr/share/licenses/$pkgname/

    # copy over the stdlib
    cd $srcdir/$_gitname
    cp -a misc extra core basis factor.image $pkgdir/usr/lib/factor/
    # use factor-vm in fuel
    sed -i -e 's/(t "factor")/(t "factor-vm")/'  \
        $pkgdir/usr/lib/factor/misc/fuel/fuel-listener.el
    # make folders r+x and files r
    chmod -R 0755 $pkgdir/usr/lib/factor
    find $pkgdir/usr/lib/factor -type f -exec chmod -x {} \;

    # copy over the actual binary and create a symlink called factor-vm
    # (otherwise it conflicts with factor from the GNU coreutils)
    cp -a factor $pkgdir/usr/lib/factor/factor
    cd $pkgdir/usr/bin
    ln -s ../lib/factor/factor factor-vm
    cd -

    # copy over the license (as defined in Arch Packaging Standards)
    chmod -x license.txt
    cp license.txt $pkgdir/usr/share/licenses/$pkgname/COPYING

    # add the desktop entry
    install -D "$srcdir/factor.desktop" "$pkgdir/usr/share/applications/factor.desktop"
    install -D "$srcdir/factor.svg" "$pkgdir/usr/share/pixmaps/factor.svg"
}
