#
# Maintainer: Mrk3004 <suportevg@uol.com.br>
#
# Contributors: Abbradar
#               Zephyr
#               Christian Autermann
#               Biginoz
#               Martin Lee
#               Ricardo Funke
#               PirateJonno
#               lh
#               Cilyan Olowen
#               Shaffer
#               Brcha
#               Lyle Putnam
#               Det

pkgname=plymouth-git
pkgver=20121102
pkgrel=2
pkgdesc="A graphical boot splash screen with kernel mode-setting support (Git version)"
url="http://cgit.freedesktop.org/cgit/plymouth/"

arch=('i686' 'x86_64')
license=('GPL')

depends=('libdrm' 'pango' 'systemd')
makedepends=('git')
optdepends=('ttf-dejavu')

options=('!libtool' '!emptydirs')

install=$pkgname.install

provides=('plymouth')
conflicts=('plymouth')
backup=('etc/plymouth/plymouthd.conf')

source=('arch-logo.png'
        'encrypt_hook'
        'encrypt_install'
        'gdm-plymouth.service'
        'kdm-plymouth.service'
        'kdm-unpatched-plymouth.service'
        'lxdm-plymouth.service'
        'plymouth.functions'
        "$pkgname.install"
        'plymouth.initcpio_hook'
        'plymouth.initcpio_install'
        'plymouth-pid.patch'
        'plymouth-quit.service'
        'plymouth-set-default-theme.in.patch'
        'plymouth-update-initrd.patch'
        'plymouthd.conf'
        'system-release'
        'systemd-unit-dir.patch')
        
md5sums=('65fa2763d5c9bb9f80973ea5e6e3db3e'   # arch-logo.png       
         '79613b70a0a0be4c33978ef37c30f00a'   # encrypt_hook       
         '65eb269910f1b81e3f0970af456394ca'   # encrypt_install       
         '34d0ed7bbcb3d0ea498de24bc17b2ee9'   # gdm-plymouth.service       
         '75ec0ac73eb9a1e3f6104df64520d431'   # kdm-plymouth.service       
         'ceec65ce58accdb270f3eaa23d794b4a'   # kdm-unpatched-plymouth.service       
         '62c3b4e894330a2ca4c2b8fbcaba083c'   # lxdm-plymouth.service       
         'f0110fba1e77cd22c32d1727399d0b8a'   # plymouth.functions   
         '95e987cfbc2d30a2cea215394eb9ac20'   # plymouth-git.install    
         '4f3be343eea9956bbc94d615d5f41def'   # plymouth.initcpio_hook       
         'e606ce14ceace727c2f13758e371a4ed'   # plymouth.initcpio_install       
         '5d46fe43ca842ea8ed4fa6b2dbc55336'   # plymouth-pid.patch       
         '3bb0b8008edfad9e23703674341766a0'   # plymouth-quit.service       
         '42fddd683720dbacbe6e30b4b7892f3c'   # plymouth-set-default-theme.in.patch    
         'c2a0cf5a499d6c14723461166a24ede7'   # plymouth-update-initrd.patch       
         '37479b134827dc307b3c55bfb2711e99'   # plymouthd.conf       
         'ab8a557d10f74bec5c94ed6cae34729c'   # system-release       
         '30acbfc7722fb62ee464b8fec32b5cfd')  # systemd-unit-dir.patch 

_gitroot='git://anongit.freedesktop.org/plymouth'
_gitname='plymouth'

build() {
  msg "Connecting to git.freedesktop.org GIT server...."
  
  if [ -d $_gitname ]; then
    cd $_gitname && time git pull --depth 1 origin
    msg "The local files are updated."
  else
    git clone --depth 1 $_gitroot
  fi
  
  msg "GIT checkout done. Preparing sources..."
  
  cd "$srcdir"
  rm -rf $_gitname-build
  cp -r $_gitname $_gitname-build

  msg "Applying Patches..."

  cd $_gitname-build
  msg2 "Setting a human-readable error for '# plymouth-set-default-theme -R'"
  patch -Np0 -i "$srcdir/plymouth-set-default-theme.in.patch"

  msg2 "Fixing updating initrd"
  patch -Np0 -i "$srcdir/plymouth-update-initrd.patch"

  msg2 "Changing PID file to /run/plymouth.pid"
  patch -Np0 -i "$srcdir/plymouth-pid.patch"

  msg2 "Changing SYSTEMD_UNIT_DIR to /usr/lib/systemd/system/"
  patch -Np0 -i "$srcdir/systemd-unit-dir.patch"

  sed -e 's:png_set_gray_1_2_4_to_8:png_set_expand_gray_1_2_4_to_8:g' \
       -i src/libply-splash-graphics/ply-image.c

  msg "Starting make..."

  ./autogen.sh --prefix=/usr --exec-prefix=/usr --sysconfdir=/etc --localstatedir=/var --libdir=/usr/lib --libexecdir=/usr/lib \
    --enable-systemd-integration \
    --enable-tracing \
    --with-gdm-autostart-file=yes \
    --with-logo=/usr/share/plymouth/arch-logo.png \
    --with-background-start-color-stop=0x000000 \
    --with-background-end-color-stop=0x4D4D4D \
    --without-rhgb-compat-link \
    --without-system-root-install

  make
}

package() {
  cd $_gitname-build

  make DESTDIR="$pkgdir" install

  install -Dm644 "$srcdir/arch-logo.png" "$pkgdir/usr/share/plymouth/arch-logo.png"
  install -Dm644 "$srcdir/encrypt_hook" "$pkgdir/usr/lib/initcpio/hooks/plymouth-encrypt"
  install -Dm644 "$srcdir/encrypt_install" "$pkgdir/usr/lib/initcpio/install/plymouth-encrypt"
  install -Dm644 "$srcdir/plymouth.functions" "$pkgdir/etc/rc.d/functions.d/plymouth.functions"
  install -Dm644 "$srcdir/plymouth.initcpio_hook" "$pkgdir/usr/lib/initcpio/hooks/plymouth"
  install -Dm644 "$srcdir/plymouth.initcpio_install" "$pkgdir/usr/lib/initcpio/install/plymouth"
  install -Dm644 "$srcdir/plymouthd.conf" "$pkgdir/etc/plymouth/plymouthd.conf"
  install -Dm644 "$srcdir/system-release" "$pkgdir/etc/system-release"

  # Plymouth<->systemd integration, including units for DMs
  for i in {{gdm,kdm{,-unpatched},lxdm}-plymouth,plymouth-quit}.service; do
    install -Dm644 "$srcdir/$i" "$pkgdir/usr/lib/systemd/system/$i"
  done
  
  # Fix for rescue.service and emergency.service
  # https://bugs.archlinux.org/task/32216
  mkdir "$pkgdir/bin" && ln -s /usr/bin/plymouth "$pkgdir/bin/plymouth"
}
