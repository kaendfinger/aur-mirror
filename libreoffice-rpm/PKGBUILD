# Contributor: Red_Squirrel <evangelion87d@gmail.com>
# Contributor: Xavier114fch <xavier114fch@gmail.com>
# Contributor: Mark E. Lee <mark@markelee.com>
# Maintainer : Mark E. Lee <mark@markelee.com>

## Changelog :
## changed build function to package function (since nothing is built)
## removed rpmextract dependency since bsdtar can already extract rpms (per Red_Squirrel's advice)
## removed double posting query
## fixed up pkgver and pkgrel variables
## updated to libreoffice 4.0.1
## added libnp12 as a dependency due to <https://bugassistant.libreoffice.org/show_bug.cgi?id=61571> as reported by xavier114fch
## fixed up some file permissions for :
##    FILE/DIR                        PERMISSIONS
##    --------                        -----------
##    /usr/bin/                          755
##    /opt/libreoffice4.0/LICENSE        644
##    /opt/libreoffice4.0/CREDITS.odt    644
##    /opt/libreoffice4.0/LICENSE.odt    644
## Using package defaults for rest

pkgname=libreoffice-rpm
pkgver=4.0.1
pkgrel=2
pkgdesc="LGPL Office Suite installed from rpms"
arch=('i686' 'x86_64')
url='http://www.libreoffice.org'
license=('LGPL')
depends=('glibc>=2.5' 'gtk2>=2.10.4' 'linux>=2.6.18' 'xorg-server' 'libpng12')
makedepends=('wget' 'awk')
optdepends=('jre7-openjdk' 'gtk3' 'gnome')

if [ "$(uname -m)" == "i686" ]; then  ## convert bit architecture to libreoffice format
  arch_mod='x86';
  md5sums+=('177a58ff05223d7a56253eb64f056d2b');
  source+=("http://download.documentfoundation.org/libreoffice/stable/${pkgver}/rpm/${arch_mod}/LibreOffice_${pkgver}_Linux_${arch_mod/_/-}_rpm.tar.gz")
 else
  arch_mod='x86_64';
  md5sums+=('3524a59107d1120c2ce364a46bc9d795')
  source+=("http://download.documentfoundation.org/libreoffice/stable/${pkgver}/rpm/${arch_mod}/LibreOffice_${pkgver}_Linux_${arch_mod/_/-}_rpm.tar.gz")
fi;

package() {   ## package function
cd ${srcdir};  ## enter the package source directory

## install optional language & help packs (queries user)
wget -q "http://download.documentfoundation.org/libreoffice/stable/${pkgver}/rpm/${arch_mod}/";  ## get index.html
declare -ar opt_pak=('langpack' 'helppack');  ## declare optional extensions
for a in ${opt_pak[@]}; do  ## loop for all optional extensions
  read -p "Do you want to install additional ${a} (Y/y/N/n)?" opt_ans;
  case $opt_ans in  ## evaluate the answer
    Y|y)
       echo "Which ${a} do you want to install?";
       ## generate a menu for all available packages
       select opt_ext in $(cat index.html | awk -F'_' "/${a}/ && !/.asc/ {print \$7}" | awk -F'.' '{print $1}'); do
         ## download the rpm package
         msg "Retrieving LibreOffice_${pkgver}_Linux_${arch_mod/_/-}_rpm_${a}_${opt_ext}.tar.gz...";
         wget "http://download.documentfoundation.org/libreoffice/stable/${pkgver}/rpm/${arch_mod}/LibreOffice_${pkgver}_Linux_${arch_mod/_/-}_rpm_${a}_${opt_ext}.tar.gz";
         ## download the rpm md5
         wget -q "http://download.documentfoundation.org/libreoffice/stable/${pkgver}/rpm/${arch_mod}/LibreOffice_${pkgver}_Linux_${arch_mod/_/-}_rpm_${a}_${opt_ext}.tar.gz.md5"
         ## check the md5sum for the package
         msg "Done";
         msg "Validating LibreOffice_${pkgver}_Linux_${arch_mod/_/-}_rpm_${a}_${opt_ext}.tar.gz with md5sum..."
         md5sum -c "LibreOffice_${pkgver}_Linux_${arch_mod/_/-}_rpm_${a}_${opt_ext}.tar.gz.md5"
         if [ ! $? -eq 0 ]; then  ## check the md5sum error
            echo "Failed md5sum check for http://download.documentfoundation.org/libreoffice/stable/${pkgver}/rpm/${arch_mod}/LibreOffice_${pkgver}_Linux_${arch_mod/_/-}_rpm_${a}_${opt_ext}.tar.gz";
            exit;
          else
            msg "Done";
            ## extract the help/lang pack archive
            msg "Extracting LibreOffice_${pkgver}_Linux_${arch_mod/_/-}_rpm_${a}_${opt_ext}.tar.gz..."
            tar -xf "LibreOffice_${pkgver}_Linux_${arch_mod/_/-}_rpm_${a}_${opt_ext}.tar.gz";
            msg "Done";
         fi;
         break;  ## break the select loop
       done;
       ;;
    N|n)
       echo "Not installing additional $a";
       ;;
    *)
       echo "Not a valid answer";
       ;;
  esac;
done;

## extract rpms and install them
for a in $(ls -d */); do  ## loop for all directories found
  cd "${srcdir}/${a}RPMS";  ## enter the RPMS directory
  for b in *.rpm; do  ## loop for all rpm files found
    bsdtar -xf $b;  ## extract the rpm files
  done;
  cp -rf opt ${pkgdir}/;  ## copy/merge the opt directory to the package directory
  
  ## change the permissions for files that shouldn't be executable
  declare -a wrongexec=("opt/libreoffice${pkgver%.1}/CREDITS.odt" "opt/libreoffice${pkgver%.1}/LICENSE.odt" "opt/libreoffice${pkgver%.1}/NOTICE");  ## set the array to change permissions
  for a in ${wrongexec[@]}; do
    chmod 644 ${pkgdir}/$a; ## change permissions to read/write for root, read only for users
  done;
done;

## Get desktop icons for libreoffice
cd ${srcdir}/LibreOffice_${pkgver}.${pkgrel}_Linux_${arch_mod/_/-}_rpm/RPMS/desktop-integration;  ## enter the desktop integration directory
bsdtar -xf libreoffice${pkgver%.1}-freedesktop-menus-${pkgver}-${pkgrel}.noarch.rpm;  ## extract the desktop files (note: ${pkgver%.1} = 4.0)
cp -r usr ${pkgdir}/;  ## copy the usr directory to the package directory
chmod 755 ${pkgdir}/usr/bin/*;  ## change directory permissions to read/write/execute for root, read/execute only for users
# chmod -R 644 ${pkgdir}/usr/share;  ## change directory permissions to read/write for root, read only for users
}
