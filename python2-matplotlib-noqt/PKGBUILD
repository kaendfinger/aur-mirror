# $Id$
# Maintainer: Pedro Martinez-Julia <pedromj@um.es>
# Contributor: Stéphane Gaudreault <stephane@archlinux.org>
# Contributor: Stefan Husmann <stefan-husmann@t-online.de>
# Contributor: Angel 'angvp' Velasquez <angvp[at]archlinux.com.ve>
# Contributor: Douglas Soares de Andrade <dsa@aur.archlinux.org>

pkgname=python2-matplotlib-noqt
pkgver=1.2.0
pkgrel=1
pkgdesc="A python plotting library, making publication quality plots"
arch=('i686' 'x86_64')
url='http://matplotlib.org'
license=('custom')
makedepends=('python2-pytz' 'python2-numpy' 'tk' 'python2-cairo' 'python2-dateutil'
             'python2-gobject' 'python2-pyparsing' 'pygtk' 'python-six' 'ghostscript'
			 'texlive-bin')
optdepends=('tk: used by the TkAgg backend'
            'ghostscript: usetex dependencies'
            'texlive-bin: usetex dependencies')
source=("https://github.com/downloads/matplotlib/matplotlib/matplotlib-${pkgver}.tar.gz"
         python-matplotlib-tk.patch)
sha1sums=('1d0c319b2bc545f1a7002f56768e5730fe573518'
          'ddab33a37913855fe3078d74d3ce8762adb9fb2a')

build() {
   cd "${srcdir}"/matplotlib-${pkgver}

   patch -Np1 -i ../python-matplotlib-tk.patch

   # use system python-six
   rm lib/six.py

   # remove internal copies of pyparsing
   rm -r lib/matplotlib/pyparsing_py{2,3}.py
   sed -i -e 's/matplotlib.pyparsing_py[23]/pyparsing/g' lib/matplotlib/{mathtext,fontconfig_pattern}.py

   # For numpy 1.7
   sed -i '/include/s/numpy\/arrayobject.h/numpy\/oldnumeric.h/g' \
      src/*.{c,cpp,h} lib/matplotlib/delaunay/*.{cpp,h} lib/matplotlib/tri/*.h

   cd ..
   cp -a matplotlib-${pkgver} matplotlib-${pkgver}-py3

   # Build python2
   cd matplotlib-${pkgver}
   for file in $(find . -name '*.py' -print); do
      sed -i -e "s|^#!.*/usr/bin/python|#!/usr/bin/python2|" \
             -e "s|^#!.*/usr/bin/env *python|#!/usr/bin/env python2|" ${file}
   done

   python2 setup.py build
}

package() {
   cd "${srcdir}"/matplotlib-${pkgver}
   python2 setup.py install -O1 --skip-build --root "${pkgdir}" --prefix=/usr

   install -dm755 "${pkgdir}"/usr/share/licenses/python2-matplotlib-noqt
   install -m 644 doc/users/license.rst "${pkgdir}"/usr/share/licenses/python2-matplotlib-noqt
}
