# Maintainer: Det
# Based on jre: https://aur.archlinux.org/packages.php?ID=51908

pkgname=jdk
_major=7
_minor=17
_build=b02
_pkg=${_major}u$_minor
pkgver=$_major.$_minor
pkgrel=1
pkgdesc="Java $_major Development Kit"
arch=('i686' 'x86_64')
url=http://www.oracle.com/technetwork/java/javase/downloads/index.html
license=('custom')
depends=('desktop-file-utils' 'hicolor-icon-theme' 'libx11' 'libxrender' 'libxslt' 'libxtst' 'shared-mime-info' 'xdg-utils')
optdepends=('alsa-lib: sound'
            'ttf-dejavu: fonts')
provides=("java-environment=$_major" "java-runtime=$_major" "java-runtime-headless=$_major" "java-web-start=$_major")
conflicts=("${provides[@]}")
backup=('etc/conf.d/derby-network-server'
        'etc/profile.d/jdk.csh'
        'etc/profile.d/jdk.sh')
install=jdk.install
_arch=i586  # Workaround for the AUR Web interface Source parser
_arch2=i386; [ "$CARCH" = 'x86_64' ] && _arch=x64 && _arch2=amd64
source=("http://download.oracle.com/otn-pub/java/jdk/$_pkg-$_build/jdk-$_pkg-linux-$_arch.tar.gz"
        'derby-network-server'
        'derby-network-server.conf'
        'java-monitoring-and-management-console.desktop'
        'java-policy-settings.desktop'
        'java-visualvm.desktop'
        'javaws-launcher'
        'jdk.csh'
        'jdk.sh')
md5sums=('694f9592d894b86a8a3cb56bf71768e6'  # jdk-$_pkg-linux-i586.tar.gz
         'a279e195e249000646895d93e199860d'  # derby-network-server
         '4bdff6982c66d24a879c424aaac3d04d'  # derby-network-server.conf
         'da10de5e6507c392fc9871076ef53ec6'  # java-monitoring-and-management-console.desktop
         'f4e25ef1ccef8d36ff2433c3987a64fe'  # java-policy-settings.desktop
         '35fd89c5c170021d2183593335703703'  # java-visualvm.desktop
         '45c15a6b4767288f2f745598455ea2bf'  # javaws-launcher
         'e81bb7853b071df6adca0b90f0c2ac2d'  # jdk.csh
         'a06d46b277e2926d73d2ee11c9950973') # jdk.sh
[ "$CARCH" = 'x86_64' ] && md5sums[0]='d9b5870a94c47efa0282d6c1863d0667'  # jdk-$_pkg-linux-x64.tar.gz
# # Alternative mirrors, if your local one is throttled:
# source[0]="http://uni-smr.ac.ru/archive/dev/java/SDKs/sun/j2se/$_major/jdk-$_pkg-linux-$_arch.tar.gz"
# source[0]="http://ftp.wsisiz.edu.pl/pub/pc/pozyteczne%20oprogramowanie/java/jdk-$_pkg-linux-$_arch.tar.gz"

DLAGENTS=('http::/usr/bin/curl -fLC - --retry 3 --retry-delay 3 -O --header "Cookie: gpw_e24=h"')
PKGEXT=".pkg.tar"

package() {
  msg2 "Creating required dirs"
  cd jdk1.$_major.0_$_minor
  mkdir -p "$pkgdir"/{opt/java/,usr/{lib/mozilla/plugins,share/licenses/jdk},etc/{.java/.systemPrefs,{profile,rc}.d}}

  msg2 "Removing the redundancies"
  rm -r db/bin/*.bat jre/{plugin/,COPYRIGHT,LICENSE,*.txt} man/ja # lib/{desktop,visualvm/platform/docs}

  msg2 "Moving stuff in place"
  mv jre/lib/desktop/* man "$pkgdir"/usr/share/
  mv COPYRIGHT LICENSE *.txt "$pkgdir"/usr/share/licenses/jdk/
  mv * "$pkgdir"/opt/java/

  msg2 "Symlinking the plugin"
  ln -s /opt/java/jre/lib/$_arch2/libnpjp2.so "$pkgdir"/usr/lib/mozilla/plugins/

  msg2 "Installing the scripts, confs and .desktops of our own"
  cd "$srcdir"
  install -m755 jdk.{,c}sh "$pkgdir"/etc/profile.d/
  install -Dm644 derby-network-server.conf "$pkgdir"/etc/conf.d/derby-network-server
  install -m755 derby-network-server "$pkgdir"/etc/rc.d/
  install -m755 javaws-launcher "$pkgdir"/opt/java/jre/bin/
  install -m644 *.desktop "$pkgdir"/usr/share/applications/

  msg2 "Tweaking the javaws .desktop file"
  sed -e 's/Exec=javaws/&-launcher %f/' -e '/NoDisplay=true/d' -i "$pkgdir"/usr/share/applications/sun-javaws.desktop
}