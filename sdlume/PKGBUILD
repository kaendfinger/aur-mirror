# Contributor: Anton Shestakov <engored*ya.ru>
# Maintainer: Eric Anderson <ejona86@gmail.com>

pkgname=sdlume
pkgver=0.148
pkgrel=1
_basever=0148
pkgdesc='Universal Machine Emulator combines the features of MAME and MESS into a single multi-purpose emulator'
url='http://mamedev.emulab.it/haze/'
license=('custom:MAME License')
arch=('i686' 'x86_64')
depends=('sdl>=1.2.11' 'sdl_ttf' 'libxinerama' 'gconf' 'zlib' 'expat' 'gtk2' 'alsa-lib')
makedepends=('mesa')
[ "$CARCH" = 'i686' ] && makedepends+=('nasm')
optdepends=('ttf-liberation: recommended UI font')
source=('sdlume.sh'
        "mame${_basever}s.zip::http://mamedev.org/downloader.php?file=releases/mame${_basever}s.zip")
md5sums=('e1d320d42f62f3a18532640b015ee29a'
         '38f7727c2961cd31e2ab6aa1814a23ba')
install='sdlume.install'
noextract=("mame${_basever}s.zip")
PKGEXT='.pkg.tar.gz'

build() {
  mkdir "${srcdir}/${pkgname}-${pkgver}"
  cd "${srcdir}/${pkgname}-${pkgver}"

  bsdtar -xOf "${srcdir}/mame0148s.zip" | bsdtar -xvf -

  make TARGET=ume SUFFIX64='' NOWERROR=1 BUILD_ZLIB=0 BUILD_EXPAT=0 \
      ARCHOPTS="$CFLAGS" PYTHON=python2
}

package() {
  cd "${srcdir}/${pkgname}-${pkgver}"

  # Installing the wrapper script
  install -Dm755 "$srcdir/sdlume.sh" "$pkgdir/usr/bin/sdlume"

  # Installing binaries
  install -Dm755 ume "$pkgdir/usr/share/$pkgname/sdlume"

  # Installing extra bits
  install -d "$pkgdir/usr/share/$pkgname/"{artwork,hash,shader,keymaps}

  install -m644 artwork/* "$pkgdir/usr/share/$pkgname/artwork/"
  install -m644 hash/* "$pkgdir/usr/share/$pkgname/hash/"
  install -m644 src/osd/sdl/shader/glsl*.*h "$pkgdir/usr/share/$pkgname/shader/"
  install -m644 src/osd/sdl/keymaps/* "$pkgdir/usr/share/$pkgname/keymaps/"

  # The license
  install -Dm644 docs/license.txt "$pkgdir/usr/share/licenses/custom/$pkgname/license.txt"
}
