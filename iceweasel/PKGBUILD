# Maintainer: superlex

# Based on Parabola GNU/Linux iceweasel-libre PKGBUILD :
# Contributor (ConnochaetOS): Henry Jensen <hjensen@connochaetos.org>
# Contributor (Parabola): Luke Shumaker <lukeshu@sbcglobal.net>
# Contributor: Figue <ffigue at gmail>
# Contributor (Parabola): fauno <fauno@kiwwwi.com.ar>
# Contributor (Parabola): vando <facundo@esdebian.org>
# Contributor (Parabola): André Silva <emulatorman@lavabit.com>
# Contributor (Parabola): Márcio Silva <coadde@lavabit.com>
# Contributor (Arch): Jakub Schmidtke <sjakub@gmail.com>
# Thank you very much to the older contributors:
# Contributor: evr <evanroman at gmail>
# Contributor: Muhammad 'MJ' Jassim <UnbreakableMJ@gmail.com> 

_pgo=false

# We're getting this from Debian Experimental
_debname=iceweasel
_debver=19.0
_debrel=1
_debrepo=http://ftp.debian.org/debian/pool/main/i/

pkgname=iceweasel
pkgver=$_debver.deb$_debrel
pkgrel=2
pkgdesc="Debian Browser based on Mozilla Firefox"
arch=('i686' 'x86_64')
license=('GPL' 'MPL' 'LGPL')
depends=('alsa-lib' 'dbus-glib' 'desktop-file-utils' 'gtk2' 'hicolor-icon-theme' 'hunspell' 'libevent' 'libnotify' 'libvpx' 'libxt' 'mime-types' 'mozilla-common' 'nss>=3.14.1' 'sqlite3' 'startup-notification')
makedepends=('autoconf2.13' 'diffutils' 'imagemagick' 'libidl2' 'librsvg' 'libxslt' 'mesa' 'pkg-config' 'python2' 'quilt' 'unzip' 'zip')

if $_pgo; then
  makedepends+=('xorg-server-xvfb')
  options=(!ccache)
fi

optdepends=('networkmanager: Location detection via available WiFi networks'
         'iceweasel-sync: Speed up Iceweasel using tmpfs'
         'mozplugger: A Mozilla & Firefox multimedia plugin'
         'iceweasel-extension-globalmenu: Globalmenu Extension for Iceweasel'
         'iceweasel-extension-archsearch: Iceweasel Arch search engines'
         'iceweasel-i18n-it: Italian language pack for Iceweasel'
         'iceweasel-i18n-es: Spanish language pack for Iceweasel')
url="http://www.geticeweasel.org/"
install=iceweasel.install
source=("${_debrepo}/${_debname}/${_debname}_${_debver}.orig.tar.bz2"
        "${_debrepo}/${_debname}/${_debname}_${_debver}-${_debrel}.debian.tar.gz"
        'mozconfig'
        'mozconfig.pgo'
        'iceweasel.desktop'
        'iceweasel-install-dir.patch'
        'vendor.js'
        'shared-libs.patch'
        'Bug-756390-Make-the-Reset-Firefox-feature-more-gener.patch')
sha256sums=('566d1c745c62ebf1d2f702ce05e4f366bb3ea63cfafc65dd7191cffc822c1347'
            '640d4968d5088861239cbcb77569072735b552e28128dcb31a339e3ac9a87eeb'
            'f73a01b6426cdb982455dd7ace8373c9502b9efaa15f55f588ec118ffb84d4f6'
            '5d707dcf733b1d545035c68f14f59677a3fc4db795074a7e1cb539d7ef62d55b'
            '1a193cc28ed8a4c03e44da44dba72016803d571031882c1cfee9770ef67bdfe9'
            'd12a301e288b9583015823a10412d1996eeecc3aebb6da103c0a62d280b70b4b'
            '4b50e9aec03432e21b44d18c4c97b2630bace606b033f7d556c9d3e3eb0f4fa4'
            'e2b4a00d14f4ba69c62b3f9ef9908263fbab179ba8004197cbc67edbd916fdf1'
            'c4d91c573208314995ca88ec13a9440574b4083313378af87843454ff6846858')
build() {
  # WebRTC build tries to execute "python" and expects Python 2
  # Workaround taken from chromium PKGBUILD on Archlinux
  mkdir "$srcdir/python2-path"
  ln -s /usr/bin/python2 "$srcdir/python2-path/python"
  export PATH="$srcdir/python2-path:$PATH"

  export QUILT_PATCHES=debian/patches
  export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
  export QUILT_DIFF_ARGS='--no-timestamps'
  export LDFLAGS="$LDFLAGS -Wl,-rpath,/usr/lib/$pkgname"
  export PYTHON="/usr/bin/python2"
  export DEBIAN_BUILD="mozilla-release"
  mv debian "$srcdir/$DEBIAN_BUILD"
  cd "$srcdir/$DEBIAN_BUILD"
  
  # We wont save user profile in .mozilla/iceweasel
  sed -i 's/MOZ_APP_PROFILE=mozilla\/firefox/MOZ_APP_PROFILE=mozilla\/iceweasel/g' "debian/branding/configure.sh"
  
  # Doesn't apply and seems unimportant
  rm -v debian/patches/l10n/Place-google-and-gmail-before-yandex.patch || true

  # This patch doesn't works in some parts due that has patches for others locales languages, source code hasn't it
  rm -v debian/patches/debian-hacks/Bug-756390-Make-the-Reset-Firefox-feature-more-gener.patch || true

  quilt push -a

  # Adding fixed Bug-756390-Make-the-Reset-Firefox-feature-more-gener.patch
  patch -Np1 -i "$srcdir/Bug-756390-Make-the-Reset-Firefox-feature-more-gener.patch" 
  
  # Install to /usr/lib/$pkgname
  patch -Np1 -i "$srcdir/iceweasel-install-dir.patch" # install to /usr/lib/$pkgname
  patch -Np1 -i "$srcdir/shared-libs.patch"

  # Load our build config
  cp "$srcdir/mozconfig" .mozconfig

  if $_pgo; then
    cat "$srcdir/mozconfig.pgo" >> .mozconfig
  fi

  # Fix PRE_RELEASE_SUFFIX
  sed -i '/^PRE_RELEASE_SUFFIX := ""/s/ ""//' \
    browser/base/Makefile.in

 
  if $_pgo; then
    LD_PRELOAD="" /usr/bin/Xvfb -nolisten tcp -extension GLX -screen 0 1280x1024x24 :99 &
    LD_PRELOAD="" DISPLAY=:99 make -j1 -f client.mk profiledbuild MOZ_MAKE_FLAGS="$MAKEFLAGS"
    kill $! || true
  else
    LD_PRELOAD="" make -j1 -f client.mk build MOZ_MAKE_FLAGS="$MAKEFLAGS"
  fi
}

package() {
  cd "$srcdir/$DEBIAN_BUILD"
  make -j1 -f client.mk DESTDIR="$pkgdir" install

  install -Dm644 ../vendor.js "$pkgdir/usr/lib/$pkgname/defaults/preferences/vendor.js"

  # I don't even know why we're hitting the objdir, and ConnOS didn't
  _brandingdir=debian/branding
  brandingdir=moz-objdir/$_brandingdir
  icondir="$pkgdir/usr/share/icons/hicolor"
  for i in 16x16 32x32 48x48 64x64; do
    install -Dm644 "$brandingdir/default${i/x*/}.png" "$icondir/$i/apps/$pkgname.png"
  done
  install -Dm644 "$brandingdir/mozicon128.png"      "$icondir/128x128/apps/$pkgname.png"
  install -Dm644 "$_brandingdir/iceweasel_icon.svg" "$icondir/scalable/apps/$pkgname.svg"

  install -d                                        "$pkgdir/usr/share/applications"
  install -m644  "$srcdir/iceweasel.desktop"        "$pkgdir/usr/share/applications"
  
  
  # Use system-provided dictionaries
  rm -rf "$pkgdir/usr/lib/$pkgname/"{dictionaries,hyphenation}
  ln -sf /usr/share/hunspell            "$pkgdir/usr/lib/$pkgname/dictionaries"
  ln -sf /usr/share/hyphen              "$pkgdir/usr/lib/$pkgname/hyphenation"
    
  
  # We don't want the development stuff
  rm -rf "$pkgdir"/usr/{include,lib/$pkgname-devel,share/idl}

  # Workaround for now: https://bugzilla.mozilla.org/show_bug.cgi?id=658850
  ln -sf $pkgname "$pkgdir/usr/lib/$pkgname/$pkgname-bin"

  
  # Remove $srcdir refers
  sed -i '1d' "$pkgdir/usr/lib/$pkgname/defaults/pref/channel-prefs.js"
  
  
  
  # Searchplugins section
  
  # According to debian choices, we prefer to use /etc/icewasel/searchplugins
  install -d "$pkgdir/etc/${pkgname}/searchplugins/common"
  install -d "$pkgdir/etc/${pkgname}/searchplugins/locale"
  
  # Add common web searchplugins
  install -Dm644 "$srcdir/$DEBIAN_BUILD/debian/duckduckgo.xml" "$pkgdir/etc/${pkgname}/searchplugins/common/duckduckgo.xml"
  install -Dm644 "$srcdir/$DEBIAN_BUILD/debian/debsearch.xml" "$pkgdir/etc/${pkgname}/searchplugins/common/debsearch.xml"
  
  # Add web searchplugins for default locale (en-US)
  cp -R "$pkgdir/usr/lib/$pkgname/searchplugins" "$pkgdir/etc/${pkgname}/searchplugins/locale/en-US"
  rm -rv "$pkgdir/usr/lib/$pkgname/searchplugins"    
}
