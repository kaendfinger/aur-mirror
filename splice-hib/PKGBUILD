# Contributor: Ben R. <thebenj88 *AT* gmail *DOT* com>

pkgname=splice-hib
pkgver=1.0
pkgrel=4
pkgdesc='A unique puzzle game involving DNA-strand manipulation (Humble Bundle version)'
url='http://www.cipherprime.com'
arch=('i686' 'x86_64')
license=('custom:commercial')
depends=('libgl' 'mesa' 'mono')
optdepends=('catalyst: AMD Proprietary 3D driver'
			'nvidia: NVIDIA Proprietary 3D driver')
install="${pkgname}.install"
PKGEXT='.pkg.tar'
_gamepkg="splice-linux-1353389454.tar.gz"
if [[ $CARCH == x86_64 ]]; then
	_arch="x86_64"
	_badarch="x86"
else
	_arch="x86"
	_badarch="x86_64"
fi

package() {
	# Get installer
	_get_humblebundle_source "${_gamepkg}" || {
		error "Unable to find the game archive. Please download it from your Humble
			Bundle page, and place it into one of the above locations."
		exit 1;
	}
    msg "Extract files"
	cd ${srcdir}
	bsdtar -xf ${srcdir}/${_gamepkg}

	msg "Create directory for binary"
	mkdir -p ${pkgdir}/usr/bin
	mkdir ${pkgdir}/opt

	msg "Install tar.gz contents into package directory"
	mv -v ${srcdir}/Linux/ ${pkgdir}/opt/${pkgname}

	msg "Set owner and permissions"
	chown root:games ${pkgdir}/opt/${pkgname} -R
	find ${pkgdir}/opt/${pkgname} -type f -print0 | xargs -0 chmod 644
	find ${pkgdir}/opt/${pkgname} -type d -print0 | xargs -0 chmod 755
	chmod 755 ${pkgdir}/opt/${pkgname}/Splice.$_arch

	msg "Link binary to /usr/bin"
	ln -s /opt/${pkgname}/Splice.$_arch ${pkgdir}/usr/bin/${pkgname}
	
	msg "Removing unneeded binary"
	rm ${pkgdir}/opt/${pkgname}/Splice.$_badarch
	rm -rf ${pkgdir}/opt/${pkgname}/Splice_Data/Mono/$_badarch
}
  
# Thanks again to smls for the following two functions, these saved me
# a lot of time.

# Locate a game archive from one of the Humble Bundles, and symlink it into $srcdir
_get_humblebundle_source() {
  _get_local_source "$1" || return 1;
}

# Locate a file or folder provided by the user, and symlink it into $srcdir
_get_local_source() {
  msg "Looking for '$1'..."
  declare -A _search=(['build dir']="$startdir"
                      ['$LOCAL_PACKAGE_SOURCES']="$LOCAL_PACKAGE_SOURCES")
  for _key in "${!_search[@]}"; do local _dir="${_search["$_key"]}"
    if [ -z "$_dir" ]; then _dir="<undefined>"; fi
    echo -n "    - in $_key ['$_dir'] ... ";
    if [ -e "$_dir/$1" ]; then
      echo "FOUND"; ln -sfT "$(readlink -f "$_dir/$1")" "$srcdir/$1"; break; fi
    echo "NOT FOUND"
  done
  if [ ! -e "$srcdir/$1" ]; then return 1; fi
}
