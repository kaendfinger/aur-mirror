pkgname=monogame-git
pkgver=20130302
pkgrel=1
pkgdesc="XNA Implementation for Mono based platforms (git)"
arch=(any)
license=("Microsoft Public License")
depends=(mono "opentk>=2012.03.15")
makedepends=(mono nant git)
options=(!strip)
conflicts=(monogame)
provides=(monogame)
url="http://monogame.codeplex.com"
_gitroot="https://github.com/mono/MonoGame.git"
_gitname="MonoGame"

build() {
  cd "$srcdir"
  msg "Connecting to GIT server...."

  if [[ -d "$_gitname" ]]
  then
    cd "$_gitname" && git pull origin
    msg "The local files are updated."
  else
    git clone "$_gitroot" "$_gitname"
  fi

  msg "GIT checkout done or server timeout"
  msg "Starting build..."
  rm -rf "$srcdir/$_gitname-build"
  cp -r "$srcdir/$_gitname" "$srcdir/$_gitname-build"
  cd "$srcdir/$_gitname-build"

  nant buildlinux
  sn -k 1024 "$srcdir/MonoGame.snk"
  cd "MonoGame.Framework/bin/Linux/Release"
  monodis Lidgren.Network.dll --output=Lidgren.Network.il
  monodis MonoGame.Framework.dll --output=MonoGame.Framework.il
  find . -name '*.il' | xargs -rtl1 -I {} ilasm /dll /key:$srcdir/MonoGame.snk {}
}

package() {
  cd "$srcdir/$_gitname-build/MonoGame.Framework/bin/Linux/Release"
  find . -name '*.dll' -o -name '*.mdb' -o -name '*.mgfxo' -o -name '*.config' |
    xargs -rtl1 -I {} install -Dm644 {} "$pkgdir/usr/lib/monogame/"{}
  find "$pkgdir/usr/lib/monogame" -name '*.dll' | xargs -rtl1 -I {} gacutil -i {} -root "$pkgdir/usr/lib"
  install -Dm644 "$srcdir/$_gitname-build/LICENSE.txt" "$pkgdir/usr/share/licenses/$pkgname/LICENSE.txt"
}
