# Maintainer: limser <echo bGltc2VyQHFxLmNvbQo=|base64 -d>

pkgname=pidgin-hg
pkgver=33547
pkgrel=2
pkgdesc="Multi-protocol instant messaging client"
arch=('i686' 'x86_64')
url="http://pidgin.im/"
license=('GPL')
depends=('startup-notification' 'gtkspell' 'libxss' 'libsm' 'hicolor-icon-theme'
         'gstreamer0.10' 'libsasl' 'libidn' 'dbus-glib' 'nss' 'farstream' 'webkitgtk3')
makedepends=('mercurial' 'python2' 'avahi' 'tk' 'ca-certificates' 'intltool' 'networkmanager' 'meanwhile')
optdepends=('aspell: for spelling correction'
            'avahi: Bonjour protocol support'
            'ca-certificates: SSL CA certificates'
            'meanwhile: Lotus Sametime protocol support'
            'python2-dbus: for purple-remote and purple-url-handler'
            'tk: Tcl/Tk scripting support')
provides=('pidgin' 'libpurple')
conflicts=('pidgin' 'libpurple')
replaces=('pidgin' 'libpurple')
options=('!libtool')
source=()
sha256sums=()
install=pidgin.install

_hgroot="http://hg.pidgin.im/pidgin"
_hgrepo="main"

build() {
  export PYTHON=`which python2`
  msg "Connecting to $_hgroot"
  if [ -d $_hgrepo ]; then
    cd $_hgrepo
    hg pull -u || return 1
    msg2 "Finished updating the local repository!"
  else
    hg clone ${_hgroot}/${_hgrepo} "${srcdir}/${_hgrepo}" || return 1
    msg2 "Initial pull complete!"
  fi
  if [ -d "${srcdir}/${_hgrepo}-build" ]; then
    rm -rf "${srcdir}/${_hgrepo}-build"
  fi
  hg clone "${srcdir}/${_hgrepo}" "${srcdir}/${_hgrepo}-build" || return 1
  cd "${srcdir}/${_hgrepo}-build"

  msg "Starting build"
  # Use Python 2
  sed -i 's/env python$/&2/' */plugins/*.py libpurple/purple-{remote,notifications-example,url-handler}
  
  ./autogen.sh || return 1
  ./configure \
    --prefix=/usr \
    --sysconfdir=/etc \
    --disable-schemas-install \
    --disable-gnutls \
    --enable-cyrus-sasl \
    --disable-doxygen \
    --enable-nm \
    --enable-vv \
    --with-python=/usr/bin/python2 \
    --with-system-ssl-certs=/etc/ssl/certs
    make
}

package(){
  cd "${srcdir}/${_hgrepo}-build"

  for _dir in libpurple pidgin doc share/sounds share/ca-certs m4macros po; do
    make -C "$_dir" DESTDIR="$pkgdir" install
  done
  install -Dm644 pidgin.desktop "$pkgdir"/usr/share/applications/pidgin.desktop

  rm "$pkgdir/usr/share/man/man1/finch.1"
}

# vim:set ts=2 sw=2 et:
