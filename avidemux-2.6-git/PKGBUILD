#Maintainer: Gustavo Alvarez <sl1pkn07@gmail.com>

## OPTIONS: 0:Off 1:On ##
_use_qt="0"
_use_gtk="0"
_use_cli="0"

[ "${_build}" = "" ] && [ "${_use_qt}" = "0" ] && [ "${_use_gtk}" = "0" ] && [ "${_use_cli}" = "0" ] && echo "Need choice, at least, one GUI/Frontend, Please edit the PKGBUILD. Exiting" && exit 0

pkgbase=avidemux-2.6-git
pkgname=avidemux-2.6-git

[ "$_use_qt" = "1" ] && _use+="avidemux-2.6-qt-git "
[ "$_use_gtk" = "1" ] && _use+="avidemux-2.6-gtk-git "
[ "$_use_cli" = "1" ] && _use+="avidemux-2.6-cli-git "

true && pkgname=('avidemux-2.6-git' 'avidemux-2.6-core-git' $_use)

pkgver=20130301
pkgrel=1
license=('GPL2')
arch=('i686' 'x86_64')
url="http://www.avidemux.org/"
options=('!makeflags')
replaces=('avidemux-2.6-git')
install=avidemux.install

_gitroot="git://gitorious.org/avidemux2-6/avidemux2-6.git"
_gitname="avidemux"

_fakeroot="${srcdir}/avidemux-build/fakeroot"

build() {

  cd "${srcdir}"

  msg "Connecting to GIT server..."

  if [ -d "${_gitname}" ]; then
    cd "${_gitname}" && git pull
  else
    git clone --depth=1 "${_gitroot}" "${_gitname}"
  fi

  msg "GIT checkout done or server timeout"

  rm -fr "${srcdir}/"{avidemux-build,fakeroot}
  cp -R "${srcdir}/${_gitname}" "${srcdir}/avidemux-build"
  cd "${srcdir}/avidemux-build"

  msg "Build Core Libs"
  mkdir build_core && cd build_core
  cmake ../avidemux_core -DFAKEROOT="${_fakeroot}" -DCMAKE_INSTALL_PREFIX=/usr -G "Unix Makefiles"
  make VERBOSE=""
  make VERBOSE="" DESTDIR="${_fakeroot}" install
  cd ..

  msg "Build Core Plugins"
  mkdir build_core_plugins && cd build_core_plugins
  cmake ../avidemux_plugins -DFAKEROOT="${_fakeroot}" -DCMAKE_INSTALL_PREFIX=/usr -G "Unix Makefiles" \
        -DAVIDEMUX_SOURCE_DIR="${srcdir}/avidemux-build" -DPLUGIN_UI=COMMON
  make VERBOSE=""
  make VERBOSE="" DESTDIR="${_fakeroot}" install
  cd ..

  if [ "$_use_gtk" = "0" ] && [ "$_use_cli" = "0" ]; then
    msg "Build x264 common GTK/CLI encoder plugin"
    echo "Wait please"
    mkdir get_264_encode_plugin_conf && cd get_264_encode_plugin_conf
    cmake ../avidemux/cli -DFAKEROOT="${_fakeroot}" -DCMAKE_INSTALL_PREFIX=/usr -G "Unix Makefiles" &> /dev/null
    make &> /dev/null
    make DESTDIR="${_fakeroot}" install &> /dev/null
    cd ..

    mkdir build_x264_encode_plugin && cd build_x264_encode_plugin
    cmake ../avidemux_plugins -DFAKEROOT="${_fakeroot}" -DCMAKE_INSTALL_PREFIX=/usr -G "Unix Makefiles" \
          -DAVIDEMUX_SOURCE_DIR="${srcdir}/avidemux-build" -DPLUGIN_UI=CLI
    make VERBOSE=""
    make VERBOSE="" DESTDIR="${_fakeroot}" install
    cd ..
  fi

  if [ "$_use_qt" = "1" ]; then
    msg "Build Qt GUI"
    mkdir build_qt && cd build_qt
    cmake ../avidemux/qt4 -DFAKEROOT="${_fakeroot}" -DCMAKE_INSTALL_PREFIX=/usr -G "Unix Makefiles"
    make VERBOSE=""
    make VERBOSE="" DESTDIR="${_fakeroot}" install
    cd ..

    msg "Build Qt GUI Plugins"
    mkdir build_qt_plugins && cd build_qt_plugins
    cmake ../avidemux_plugins -DFAKEROOT="${_fakeroot}" -DCMAKE_INSTALL_PREFIX=/usr -G "Unix Makefiles" \
          -DAVIDEMUX_SOURCE_DIR="${srcdir}/avidemux-build" -DPLUGIN_UI=QT4
    make VERBOSE=""
    make VERBOSE="" DESTDIR="${_fakeroot}" install
    cd ..
  fi

  if [ "$_use_gtk" = "1" ]; then
    msg "Build Gtk GUI"
    mkdir build_gtk && cd build_gtk
    cmake ../avidemux/gtk -DFAKEROOT="${_fakeroot}" -DCMAKE_INSTALL_PREFIX=/usr -G "Unix Makefiles"
    make VERBOSE=""
    make VERBOSE="" DESTDIR="${_fakeroot}" install
    cd ..

    msg "Build Gtk GUI plugins"
    mkdir build_gtk_plugins && cd build_gtk_plugins
    cmake ../avidemux_plugins -DFAKEROOT="${_fakeroot}" -DCMAKE_INSTALL_PREFIX=/usr -G "Unix Makefiles" \
          -DAVIDEMUX_SOURCE_DIR="${srcdir}/avidemux-build" -DPLUGIN_UI=GTK
    make VERBOSE=""
    make VERBOSE="" DESTDIR="${_fakeroot}" install
    cd ..
  fi

  if [ "$_use_cli" = "1" ]; then
    msg "Build CLI frontend"
    mkdir build_cli && cd build_cli
    cmake ../avidemux/cli -DFAKEROOT="${_fakeroot}" -DCMAKE_INSTALL_PREFIX=/usr -G "Unix Makefiles"
    make VERBOSE=""
    make VERBOSE="" DESTDIR="${_fakeroot}" install
    cd ..

    msg "Build CLI plugins"
    mkdir build_cli_plugins && cd build_cli_plugins
    cmake ../avidemux_plugins -DFAKEROOT="${_fakeroot}" -DCMAKE_INSTALL_PREFIX=/usr -G "Unix Makefiles" \
          -DAVIDEMUX_SOURCE_DIR="${srcdir}/avidemux-build" -DPLUGIN_UI=CLI
    make VERBOSE=""
    make VERBOSE="" DESTDIR="${_fakeroot}" install
    cd ..
  fi
}

package_avidemux-2.6-git() {
  pkgdesc="A graphical tool to edit video (filter/re-encode/split). (GIT version)"
  arch=('any')
}


package_avidemux-2.6-core-git() {
  depends=('twolame' 'opencore-amr' 'jack' 'aften' 'x264' 'flac' 'xvidcore' 'sqlite' 'libpulse' 'libva' 'libvdpau' 'faac' 'faad2' 'lame' 'libdca' 'fribidi' 'libvpx' 'fontconfig')
  pkgdesc="a Core libs for Avidemux 2.6. (GIT version)"
  provides=("avidemux-2.6-core-git=${pkgver}")
  optdepends=('avidemux-2.6-qt-git: a Qt GUI for Avidemux 2.6. (GIT Version)'
              'avidemux-2.6-gtk-git: a GTK GUI for Avidemux 2.6. (GIT Version)'
              'avidemux-2.6-cli-git: a CLI frontend for Avidemux 2.6. (GIT Version)')

  cd "${srcdir}/avidemux-build/build_core"
  make VERBOSE="" DESTDIR="${pkgdir}" install
  cd "${srcdir}/avidemux-build/build_core_plugins"
  make VERBOSE="" DESTDIR="${pkgdir}" install

  install -Dm644 "${_fakeroot}/usr/lib/ADM_plugins6/pluginSettings/x264/1/PSP.json" "${pkgdir}/usr/lib/ADM_plugins6/pluginSettings/x264/1/PSP.json"
  install -Dm644 "${_fakeroot}/usr/lib/ADM_plugins6/pluginSettings/x264/1/iPhone.json" "${pkgdir}/usr/lib/ADM_plugins6/pluginSettings/x264/1/iPhone.json"
  install -m755 "${_fakeroot}/usr/lib/ADM_plugins6/videoEncoders/libADM_ve_x264_other.so" "${pkgdir}/usr/lib/ADM_plugins6/videoEncoders/libADM_ve_x264_other.so"

  rm -fr "${pkgdir}/usr/lib/ADM_plugins6/scriptEngines/libADM_script_qt.so" && rm -fr "${pkgdir}/usr/share/"
}

package_avidemux-2.6-qt-git() {
  depends=("avidemux-2.6-core-git=${pkgver}" 'qt4' 'glu' 'x264')
  pkgdesc="a Qt GUI for Avidemux 2.6. (GIT version)"
  install=avidemux.install

  cd "${srcdir}/avidemux-build/build_qt"
  make VERBOSE="" DESTDIR="${pkgdir}" install
  cd "${srcdir}/avidemux-build/build_qt_plugins"
  make VERBOSE="" DESTDIR="${pkgdir}" install

  rm -fr "${pkgdir}/usr/lib/ADM_plugins6/pluginSettings"

  install -Dm755 "${_fakeroot}/usr/lib/ADM_plugins6/scriptEngines/libADM_script_qt.so" "${pkgdir}/usr/lib/ADM_plugins6/scriptEngines/libADM_script_qt.so"
  install -d "${pkgdir}/usr/share/avidemux6"
  cp -R "${_fakeroot}/usr/share/avidemux6/help" "${pkgdir}/usr/share/avidemux6"

  install -Dm644 "${srcdir}/avidemux-build/avidemux2.desktop" "${pkgdir}/usr/share/applications/avidemux2.6-qt4.desktop"
  sed -e 's|Icon=avidemux|Icon=avidemux2.6-qt4|g' -e 's|Exec=avidemux2_gtk|Exec=avidemux3_qt4|g' -e 's|Name=avidemux2|Name=Avidemux 2.6 Qt|' \
      -i "${pkgdir}/usr/share/applications/avidemux2.6-qt4.desktop"
  install -Dm644 "${srcdir}/avidemux-build/avidemux_icon.png" "${pkgdir}/usr/share/pixmaps/avidemux2.6-qt4.png"
}

package_avidemux-2.6-gtk-git() {
  depends=("avidemux-2.6-core-git=${pkgver}" 'gtk3' 'libxv' 'sdl' 'desktop-file-utils')
  pkgdesc="a GTK GUI for Avidemux 2.6. (GIT version)"
  install=avidemux.install

  cd "${srcdir}/avidemux-build/build_gtk"
  make VERBOSE="" DESTDIR="${pkgdir}" install
  cd "${srcdir}/avidemux-build/build_gtk_plugins"
  make VERBOSE="" DESTDIR="${pkgdir}" install

  rm -fr "${pkgdir}/usr/lib/ADM_plugins6/"{videoEncoders,pluginSettings}

  install -Dm644 "${srcdir}/avidemux-build/avidemux2.desktop" "${pkgdir}/usr/share/applications/avidemux2.6-gtk.desktop"
  sed -e 's|Icon=avidemux|Icon=avidemux2.6-gtk|g' -e 's|Exec=avidemux2_gtk|Exec=avidemux3_gtk|g' -e 's|Name=avidemux2|Name=Avidemux 2.6 Gtk|' \
      -i "${pkgdir}/usr/share/applications/avidemux2.6-gtk.desktop"
  install -Dm644 "${srcdir}/avidemux-build/avidemux_icon.png" "${pkgdir}/usr/share/pixmaps/avidemux2.6-gtk.png"
}

package_avidemux-2.6-cli-git() {
  depends=("avidemux-2.6-core-git=${pkgver}")
  pkgdesc="a CLI frontend for Avidemux 2.6. (GIT version)"

  cd "${srcdir}/avidemux-build/build_cli"
  make VERBOSE="" DESTDIR="${pkgdir}" install
  cd "${srcdir}/avidemux-build/build_cli_plugins"
  make VERBOSE="" DESTDIR="${pkgdir}" install

  rm -fr "${pkgdir}/usr/lib/ADM_plugins6/"{videoEncoders,pluginSettings}
}

makedepends=('cmake' 'git' 'subversion' 'yasm' 'sdl' 'libxv' 'libvdpau' 'sqlite' 'faad2' 'libdca' 'opencore-amr' 'jack' 'libsamplerate'
             'libvpx' 'aften' 'twolame' 'lame' 'xvidcore' 'x264' 'freetype2' 'fribidi' 'fontconfig' 'libxext' 'glu' 'libxml2')
[ "$_use_qt" = "1" ] && makedepends+=('qt4')
[ "$_use_gtk" = "1" ] && makedepends+=('gtk3')
pkgdesc="A graphical tool to edit video (filter/re-encode/split). (GIT version)"