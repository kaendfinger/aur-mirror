# Maintainer: Lone_Wolf lonewolf@xs4all.nl
# Contributor: Sven-Hendrik Haase <sh@lutzhaase.com>
# Contributor: TDY <tdy@gmx.com>
# Contributor: Nelson Menon <nelsonmenon@gmail.com insanatorium.wordpress.com>

pkgname=freecol-git
pkgver=20130203
pkgrel=1
pkgdesc="A turn-based strategy game based on Colonization, git version build against java7 "
arch=('any')
url="http://www.freecol.org/"
license=('GPL')
depends=('java-environment=7' 'bash')
makedepends=('subversion' 'apache-ant' 'junit' 'texlive-core')
source=('freecol-git.sh')
conficts=('freecol-svn')
replaces=('freecol-svn')
md5sums=('2336390d741659899ecde9a4fc9ae7f2')
_gitroot="git://git.code.sf.net/p/freecol/git"
_gitname="freecol-git"
# git clone git://git.code.sf.net/p/freecol/git freecol-git

build() {
  cd "$srcdir"
  msg "Connecting to freecol GIT server...."

  if [[ -d "$_gitname" ]]; then
    cd "$_gitname" && git pull origin
    msg "The local files are updated."
  else
    git clone "$_gitroot" "$_gitname"
  fi

  msg "GIT checkout done or server timeout"
  msg "Starting build..."

  rm -rf "$srcdir/$_gitname-build"
  git clone "$srcdir/$_gitname" "$srcdir/$_gitname-build"
  cd "$srcdir/$_gitname-build"
  ant -Djava.target.version=1.7 package print-manual desktop-entry
}
package() {
  cd "$srcdir/$_gitname-build"
  install -Dm644 FreeCol.jar "$pkgdir/usr/share/java/$pkgname/FreeCol.jar"
  # install manual
  install -Dm644 doc/FreeCol.pdf "$pkgdir/usr/share/doc/$pkgname/FreeCol.pdf"
  # install icon and .desktopfile
  install -Dm644 packaging/common/freecol.xpm "$pkgdir/usr/share/pixmaps/$pkgname.xpm"
  sed -e 's:Name=FreeCol:Name=Freecol-git:' \
      -e 's:Exec=freecol:Exec=/usr/bin/freecol-git:' \
      -e 's:Icon=data/freecol.png:Icon=/usr/share/pixmaps/freecol-git.xpm:' \
      <./.desktop >"$pkgname".desktop
  install -Dm644 "$pkgname".desktop "$pkgdir/usr/share/applications/$pkgname.desktop"
  # copy necessary files and correct some permissions
  cp -r data jars "$pkgdir/usr/share/java/$pkgname/"
  find "$pkgdir/usr/share/java/$pkgname" -type d -exec chmod 755 '{}' \;
  find "$pkgdir/usr/share/java/$pkgname" -type f -exec chmod 644 '{}' \;
  # install shell script to start application
  install -Dm755 ../freecol-git.sh "$pkgdir/usr/bin/$pkgname"
}
