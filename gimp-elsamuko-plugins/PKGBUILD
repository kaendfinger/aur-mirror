# Maintainer: Piotr Rogoża <rogoza dot piotr at gmail dot com>
# Contributor: Piotr Rogoża <rogoza dot piotr at gmail dot com>
# vim:set ts=2 sw=2 et ft=sh tw=100: expandtab

pkgname=gimp-elsamuko-plugins
_pkgname=elsamuko
pkgver=0.1
pkgrel=2
pkgdesc='Varoius Gimp plugins made by elsamuko'
arch=(i686 x86_64)
url='https://sites.google.com/site/elsamuko/gimp'
license=('GPL')
groups=()
depends=(gimp opencv)
makedepends=(gimp cimg unzip)
optdepends=()
provides=()
conflicts=()
replaces=()
backup=()
options=()
optdepends=('octave-image: run Octave script in the Gimp')
install='gimp-elsamuko-plugins.install'
source=(
https://sites.google.com/site/elsamuko/gimp/depthmap/elsamuko-depthmap.c
https://sites.google.com/site/elsamuko/forensics/clone-detection/elsamuko-copy-move.c
https://sites.google.com/site/elsamuko/gimp/gimp-octave/elsamuko-gimp-octave.c
https://sites.google.com/site/elsamuko/gimp/gimp-octave/filter_pack.tar.gz
https://sites.google.com/site/elsamuko/gimp/temperature/elsamuko-temperature.c
https://sites.google.com/site/elsamuko/gimp/eaw-sharpen/elsamuko-eaw-sharpen.tar.gz
eaw-sharpen.patch
https://sites.google.com/site/elsamuko/gimp/facedetect/elsamuko-facedetect-cv.tar.gz
face-detect-cv.patch
https://sites.google.com/site/elsamuko/gimp/get-curves/elsamuko-get-curves.c
https://sites.google.com/site/elsamuko/gimp/get-curves/AlienSkin-examples.zip
https://sites.google.com/site/elsamuko/gimp/get-curves/elsamuko-kodachrome
https://sites.google.com/site/elsamuko/gimp/heatmap/elsamuko-heatmap.c
https://sites.google.com/site/elsamuko/forensics/hsv-analysis/elsamuko-hsv-analysis.c
https://sites.google.com/site/elsamuko/forensics/lab-analysis/elsamuko-lab-analysis.tar.gz
)
#https://sites.google.com/site/elsamuko/gimp/depthmap/elsamuko-depthmap-cv.tar.gz
noextract=(AlienSkin-examples.zip)
build(){
  cd "$srcdir"
  
  unzip -o AlienSkin-examples.zip -d AlienSkin-examples

  # depthmap, octave, get-curves, hsv-analysis
  msg2 "Building depthmap plugin"
  gimptool --build elsamuko-depthmap.c
  msg2 "Building octave plugin"
  gimptool --build elsamuko-gimp-octave.c
  msg2 "Building get-curves plugin"
  gimptool --build elsamuko-get-curves.c
  msg2 "Building hsv-analysis plugin"
  gimptool --build elsamuko-hsv-analysis.c
  # heatmap
  msg2 "Building heatmap plugin"
  CC='gcc -lm' gimptool --build elsamuko-heatmap.c
  # copy-move
  msg2 "Building copy-move plugin"
  CC=g++ CFLAGS=-O3 LIBS=-lpthread gimptool --build elsamuko-copy-move.c
  # temperature
  msg2 "Building temperature plugin"
  CC="g++ -O3" gimptool --build elsamuko-temperature.c 
  # eaw-sharpen
  msg2 "Building eaw-sharpen plugin"
  cd "$srcdir"/elsamuko-eaw-sharpen
  patch -Np1 -i "$srcdir"/eaw-sharpen.patch
  make
  # face-detect-cv
  msg2 "Building face-detect-cv plugin"
  cd "$srcdir"/elsamuko-facedetect-cv
  patch -Np1 -i "$srcdir"/face-detect-cv.patch
  make 
  # lab-analysis
  msg2 "Building lab-analysis plugin"
  cd "$srcdir"/elsamuko-lab-analysis
  CC='g++ -lX11' CFLAGS=-O3 gimptool --build elsamuko-lab-analysis.c
}
package(){
  cd "$srcdir"

  _pluginsdir="$pkgdir"/usr/lib/gimp/2.0/plug-ins
  install -dm755 "${_pluginsdir}"

  for _plugins in *.c; do
    install -Dm755 ${_plugins%.c} "$_pluginsdir"/
  done

  # eaw-sharpen
  cd "$srcdir"/elsamuko-eaw-sharpen
  install -Dm755 elsamuko-eaw-sharpen "$_pluginsdir"/
  # face-detect-cv
  cd "$srcdir"/elsamuko-facedetect-cv
  install -Dm755 elsamuko-facedetect-cv "$_pluginsdir"/
  install -Dm644 haarcascade_frontalface_alt.xml "$_pluginsdir"/
  # lab-analysis
  cd "$srcdir"/elsamuko-lab-analysis
  install -Dm755 elsamuko-lab-analysis "$_pluginsdir"/

  # examples .m files
  cd "$srcdir"
  install -dm755 "$pkgdir"/usr/share/$pkgname/examples
  cp -r filter_pack "$pkgdir"/usr/share/$pkgname/examples

  # doc files
  install -dm755 "$pkgdir"/usr/share/doc/$pkgname
  mv "$pkgdir"/usr/share/$pkgname/examples/filter_pack/README \
    "$pkgdir"/usr/share/doc/$pkgname/octave-plugin.README

  # GIMP curves tool settings
  _curvesdir="$pkgdir"/usr/share/$pkgname/examples/curves
  install -dm755 "$_curvesdir"
  install -Dm644 "$srcdir"/elsamuko-kodachrome "$_curvesdir"/
  cp AlienSkin-examples/* "$_curvesdir"/
  chmod 0644 "$_curvesdir"/*

}
md5sums=('c6232d03ed2938bbe83c5cc719590938'
         '13018333b230f9a31353a4fb0e80a780'
         '2fc97640122e1c6da9f84d697280119b'
         'db194c4c97231f31835f13a313219b36'
         '635b821f4508402a4f4b26daab0eea5e'
         '2708f2cf7e7579fa76211b9bc6b8b401'
         '6323c3cd9fadeb60412c730d5df1f741'
         'dc4a2a8ffa5ae6461b27119ce1545860'
         '4a4b8f66b567bd1e975e65b6713b101d'
         '0f571d1a0b01da1334fa2871f729aebf'
         'ffadbad68664168b9ad637a793851864'
         '3b64bee1c2f563257113669b82995950'
         '0741a2b44e57c1bd9dec7ff54f666b5c'
         '7b1035756f06d8e29fcd8d60046a86de'
         'bdd087a53c32c6480323fcb9d35a31ac')
