# Contributor: Thomas Bächler <thomas@archlinux.org>
# Maintainer: Clément DEMOULINS <clement@archivel.fr>

pkgname=openvpn-dev
_pkgname=openvpn
pkgver=2.3.0
pkgrel=1
pkgdesc="An easy-to-use, robust, and highly configurable VPN (Virtual Private Network)"
arch=('i686' 'x86_64')
url="http://openvpn.net/index.php/open-source.html"
license=('custom')
depends=('openssl' 'lzo2' 'iproute2')
provides=('openvpn')
conflicts=('openvpn')
options=('!libtool')
optdepends=('openresolv: scripts can use this tool to manage resolv.conf')

source=(http://swupdate.openvpn.net/community/releases/${_pkgname}-${pkgver}.tar.xz
        openvpn.rc openvpn.conf openvpn@.service)
md5sums=('33510fc8542bcd5f4da6f10e743ed2fe'
         'c9f7a8c0be2448877275d5115543afb3'
         'b0938f65016c904dab336f019f81e126'
         '44047df812a3fcd57a7e36a61732a9b9')

backup=('etc/conf.d/openvpn')

build() {
  cd $srcdir/${_pkgname}-${pkgver}

  # Build openvpn
  CFLAGS="$CFLAGS -DPLUGIN_LIBDIR=\\\"/usr/lib/openvpn\\\"" ./configure \
    --prefix=/usr \
    --enable-password-save \
    --enable-systemd \
    --enable-iproute2 \
    --mandir=/usr/share/man
  make
}

package() {
  cd $srcdir/${_pkgname}-${pkgver}

  # Install openvpn
  make DESTDIR=$pkgdir install distdir
  install -d -m755 $pkgdir/etc/openvpn
  install -m 755 -D $srcdir/openvpn.rc $pkgdir/etc/rc.d/openvpn
  install -m 644 -D $srcdir/openvpn.conf $pkgdir/etc/conf.d/openvpn
  install -m 644 -D $srcdir/openvpn@.service $pkgdir/usr/lib/systemd/system/openvpn@.service

  # Install samples
  for sample in sample-config-files sample-keys sample-plugins sample-scripts sample-windows; do
    cp -r sample/${sample} ${pkgdir}/usr/share/doc/openvpn/sample/
  done
  cp -r contrib ${pkgdir}/usr/share/doc/openvpn/

  # Install license
  install -D -m644 COPYING $pkgdir/usr/share/licenses/$pkgname/COPYING

  # Install easy-rsa
  #cd $srcdir/${_pkgname}-${pkgver}
  #make -C easy-rsa/2.0 install DESTDIR=$pkgdir PREFIX=usr/share/openvpn/easy-rsa
}
# vim:set ts=2 sw=2 et:
