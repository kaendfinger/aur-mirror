# Maintainer: Michael Lass <bevan@bi-co.net>
# Contributor: Szymon Jakubczak <szym-at-mit-dot-edu>

pkgname=openafs
pkgver=1.6.2
pkgrel=1
pkgdesc="Open source client for the AFS distributed file system"
arch=('i686' 'x86_64')
url="http://www.openafs.org"
license=('custom:"IBM Public License Version 1.0"')
depends=('krb5')
makedepends=('autoconf' 'automake' 'bison' 'flex' 'gzip' 'linux-headers')
conflicts=('openafs-features' 'openafs-features-libafs')
backup=(etc/conf.d/openafs
	etc/openafs/ThisCell
        etc/openafs/cacheinfo
        etc/openafs/CellServDB)
install=openafs.install
source=(http://openafs.org/dl/openafs/${pkgver}/${pkgname}-${pkgver}-src.tar.bz2
        http://openafs.org/dl/openafs/${pkgver}/${pkgname}-${pkgver}-doc.tar.bz2
        adjust-configs.patch
        fix-linux-3_8.patch)
md5sums=('ab1335f74214487aa363b614fbfa3a00'
         '9b0371dc1451c59c47e9519f697a2826'
         'c280442c6c4cec1aef8e5a60f52b111b'
         '1bf73832c2a4f5c5ad53a862fac37417')

build() {
  cd ${srcdir}/${pkgname}-${pkgver}

  # Adjust RedHat config and service files to our needs
  patch -p1 < ${srcdir}/adjust-configs.patch

  # Fix compatibility with linux 3.8
  # Picked from: - http://gerrit.openafs.org/#change,8941
  #              - http://gerrit.openafs.org/#change,8942
  #              - http://gerrit.openafs.org/#change,8948
  patch -p1 < ${srcdir}/fix-linux-3_8.patch

  # Needed when changes to configure were made
  ./regen.sh -q

  ./configure --prefix=/usr \
              --sysconfdir=/etc \
              --libexecdir=/usr/lib \
              --with-linux-kernel-packaging \
              --disable-fuse-client
  make
}


package() {
  _kernelver=$(uname -r)
  _extramodules=$(readlink -fn /usr/lib/modules/${_kernelver}/extramodules)

  cd ${srcdir}/${pkgname}-${pkgver}

  make DESTDIR=${pkgdir} install

  # rename kpasswd which is already provided by krb5
  mv ${pkgdir}/usr/bin/kpasswd ${pkgdir}/usr/bin/kpasswd-openafs
  mv ${pkgdir}/usr/share/man/man1/kpasswd.1 ${pkgdir}/usr/share/man/man1/kpasswd-openafs.1

  # create cache directory
  install -dm700 ${pkgdir}/var/cache/openafs

  # move kernel module
  install -dm755 ${pkgdir}${_extramodules}
  mv ${pkgdir}/lib/modules/${_kernelver}/extra/openafs/openafs.ko ${pkgdir}${_extramodules}/openafs.ko
  gzip -9 ${pkgdir}${_extramodules}/openafs.ko
  rm -rf ${pkgdir}/lib

  # move PAM libs
  install -dm755 ${pkgdir}/usr/lib/security
  mv ${pkgdir}/usr/lib/pam_afs.krb.so.1 ${pkgdir}/usr/lib/pam_afs.so.1 ${pkgdir}/usr/lib/security/

  # install systemd service files
  install -Dm644 ${srcdir}/${pkgname}-${pkgver}/src/packaging/RedHat/openafs-client.service ${pkgdir}/usr/lib/systemd/system/openafs-client.service
  install -Dm644 ${srcdir}/${pkgname}-${pkgver}/src/packaging/RedHat/openafs-server.service ${pkgdir}/usr/lib/systemd/system/openafs-server.service

  # install default configs
  install -Dm644 ${srcdir}/${pkgname}-${pkgver}/src/afsd/CellServDB ${pkgdir}/etc/${pkgname}/CellServDB
  install -Dm644 ${srcdir}/${pkgname}-${pkgver}/src/packaging/RedHat/openafs.sysconfig ${pkgdir}/etc/conf.d/openafs
  install -Dm644 ${srcdir}/${pkgname}-${pkgver}/src/packaging/RedHat/openafs-ThisCell ${pkgdir}/etc/${pkgname}/ThisCell
  install -Dm644 ${srcdir}/${pkgname}-${pkgver}/src/packaging/RedHat/openafs-cacheinfo ${pkgdir}/etc/${pkgname}/cacheinfo

  # install license
  install -Dm644 ${srcdir}/${pkgname}-${pkgver}/src/LICENSE ${pkgdir}/usr/share/licenses/${pkgname}/LICENSE
}
