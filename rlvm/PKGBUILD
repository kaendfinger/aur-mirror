# Maintainer: Jon Bergli Heier <snakebite@jvnv.net>
pkgname=rlvm
pkgver=0.12
pkgrel=4
pkgdesc="RealLive clone for Linux and OSX"
arch=(i686 x86_64)
url="http://www.elliotglaysher.org/rlvm/"
license=('GPL')
depends=('boost-libs>=1.46' glew libmad sdl_mixer sdl_image sdl_ttf)
makedepends=(scons boost)
source=(https://github.com/eglaysher/rlvm/tarball/release-${pkgver}
        build.patch
        boost_filesystem_v3.patch
        boost_filesystem_v3_test.patch
        boost-gcc-fix.patch
        textpage-noncopyable.patch)
md5sums=('9dbafab7f720f6782c727015012e25fe'
         'f702633c91148a239fd0457621a311f5'
         'cc97ff08f27c4c790c86d9dc08914562'
         '35d0432a6616138853215eb3a5cf48d6'
         '45e14973f8833b283e8cb77300594e62'
         'f746754439b33b6af9cccb6b45b28ec5')

build() {
  # release commit, for silly directory names
  commit=bee99d3
  cd "$srcdir/eglaysher-rlvm-$commit"

  patch -Np0 < $srcdir/build.patch || return 1
  patch -Np1 < $srcdir/boost_filesystem_v3.patch || return 1
  patch -Np1 < $srcdir/boost_filesystem_v3_test.patch || return 1
  patch -Np1 < $srcdir/boost-gcc-fix.patch || return 1
  patch -Np1 < $srcdir/textpage-noncopyable.patch || return 1
  scons --release
  install -D "$srcdir/eglaysher-rlvm-$commit/build/release/rlvm" "$pkgdir/usr/bin/rlvm"
  install -D "$srcdir/eglaysher-rlvm-$commit/src/Platforms/gtk/rlvm.desktop" "$pkgdir/usr/share/applications/rlvm.desktop"
  for r in 16 24 32 48 128 256; do
    install -d "$pkgdir/usr/share/icons/hicolor/${r}x$r"
    install -D "$srcdir/eglaysher-rlvm-$commit/resources/$r/rlvm.png" "$pkgdir/usr/share/icons/hicolor/${r}x$r/apps/rlvm.png"
  done
}

# vim:set ts=2 sw=2 et:
