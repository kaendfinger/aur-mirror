# Contributor: Dirk Sohler <spam@0x7be.de>               
# Maintainer: Dirk Sohler <spam@0x7be.de>
# vim: ts=4:sw=4

pkgname=mycron-git
pkgver=20130113
pkgrel=1
pkgdesc='A standards compliant and convenient crontab management system.'

url='https://dev.0x7be.de/mycron'
arch=('any')
license=('GPL')

depends=('python>=3.0')
makedepends=('git')

install=mycron.install

source=('mycron.install'
        'mycron.1'
		'mycron.5')

sha256sums=('a5406ae03412d3a1aaf6aadb902b4dd40bb6eadb21aa2ffb7c21ae95d656f598'
            '2a9782b1ce6f8be1c9657ae0dcb8b86f3714478a0918d08ee361046018a0661d'
			'992af9865a4bf4206eb87b84b2360b0edd7d47edc9728c5918d12977aafda614')

_gitroot='https://github.com/dsohler/mycron'
_gitname='master'

build() {
	cd $srcdir
	msg "Connecting to GIT server...."

	if [[ -d "$_gitname" ]]; then
		cd "$_gitname" && git pull origin
		msg "The local files are updated."
	else
		git clone "$_gitroot" "$_gitname"
	fi

	msg "GIT checkout done or server timeout"
	msg "Starting build..."

	rm -rf "$srcdir/$_gitname-build"
	git clone "$srcdir/$_gitname" "$srcdir/$_gitname-build"
}

package() {
	cd "$srcdir/$_gitname-build"
	DOCPATH="usr/share/doc/mycron"
	MANPATH="usr/share/man"
	install -Dm 755 mycron.py $pkgdir/usr/bin/mycron
	install -Dm 644 example-config.cfg $pkgdir/$DOCPATH/example-config.cfg
	install -Dm 644 example.crontab	$pkgdir/$DOCPATH/example.crontab
	install -Dm 644 crontab.vim $pkgdir/$DOCPATH/crontab.vim
	install -Dm 644 ../mycron.1 $pkgdir/$MANPATH/man1/mycron.1
	install -Dm 644 ../mycron.5 $pkgdir/$MANPATH/man5/mycron.5
	gzip $pkgdir/$MANPATH/man1/mycron.1
	gzip $pkgdir/$MANPATH/man5/mycron.5

}
