pkgname=mingw-w64-ruby
pkgver=2.0.0_p0
pkgrel=1
pkgdesc="An object-oriented language for quick and easy programming (mingw-w64)"
arch=(any)
url="http://www.ruby-lang.org/en"
license=("BSD" "custom")
makedepends=(mingw-w64-gcc mingw-w64-pkg-config ruby)
depends=(mingw-w64-crt mingw-w64-openssl)
options=(!libtool !strip !buildflags)
source=("ftp://ftp.ruby-lang.org/pub/ruby/${pkgver%.*}/ruby-${pkgver//_/-}.tar.bz2")
md5sums=('895c1c581f8d28e8b3bb02472b2ccf6a')

_architectures="i686-w64-mingw32 x86_64-w64-mingw32"

_optimal_make_jobs() {
  if [ -r /proc/cpuinfo ]; then
    local core_count=$(grep -Fc processor /proc/cpuinfo)
  else
    local core_count=0
  fi
  if [ $core_count -gt 1 ]; then
    echo -n $[$core_count-1]
  else
    echo -n 1
  fi
}

build() {
  for _arch in ${_architectures}; do
    unset LDFLAGS
    mkdir -p "${srcdir}/${pkgname}-${pkgver}-build-${_arch}"
    cd "${srcdir}/${pkgname}-${pkgver}-build-${_arch}"
    ${srcdir}/ruby-${pkgver//_/-}/configure \
      --prefix=/usr/${_arch} \
      --build=$CHOST \
      --host=${_arch} \
      --disable-install-doc \
      --disable-install-rdoc \
      --disable-install-capi \
      --with-out-ext=readline,sdbm \
      --disable-ipv6
    make -j$(_optimal_make_jobs)
  done
}

package() {
  for _arch in ${_architectures}; do
    cd "${srcdir}/${pkgname}-${pkgver}-build-${_arch}"
    make DESTDIR="$pkgdir" install
    find "$pkgdir/usr/${_arch}" -name '*.a' -o -name '*.dll' -o -name '*.so' | xargs -rtl1 ${_arch}-strip -g
    find "$pkgdir/usr/${_arch}" -name '*.dll' | xargs -rtl1 ${_arch}-strip -x
    find "$pkgdir/usr/${_arch}" -name '*.exe' -o -name '*.bat' | xargs -rtl1 rm
    rm -r "$pkgdir/usr/${_arch}/share"
  done
}
