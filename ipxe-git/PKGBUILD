# Maintainer: Christian Hesse <mail@eworm.de>

pkgname=ipxe-git
pkgver=20121102
pkgrel=1
pkgdesc="iPXE open source boot firmware - git checkout"
arch=('i686' 'x86_64')
url="http://www.ipxe.org/"
license=('GPL')
makedepends=('git' 'syslinux' 'cdrkit')
provides=('ipxe')
conflicts=('ipxe')
install=ipxe.install
source=('http://www.eworm.de/download/linux/ipxe_gitversion.patch'
	'http://www.eworm.de/download/linux/ipxe_arch.patch'
	'grub'
	'chain-default.ipxe')
backup=('etc/grub.d/20_ipxe')

_gitroot="git://git.ipxe.org/ipxe.git"
_gitname="ipxe"

build() {
	cd "${srcdir}"
	msg "Connecting to GIT server...."

	if [ -d ${_gitname} ] ; then
		cd ${_gitname} && git pull origin
		msg "The local files are updated."
	else
		git clone ${_gitroot} ${_gitname}
	fi

	msg "GIT checkout done or server timeout"
	msg "Starting make..."

	rm -rf "${srcdir}/${_gitname}-build"
	git clone "${srcdir}/${_gitname}" "${srcdir}/${_gitname}-build"

	cd "${srcdir}/${_gitname}-build/src"

	# Show git version and compile timestamp
	patch -Np2 < ${srcdir}/ipxe_gitversion.patch

	# ArchLinux branding
	patch -Np2 < ${srcdir}/ipxe_arch.patch

	# read and set keymap
	[ -s /etc/rc.conf ] && . /etc/rc.conf
	[ -s /etc/vconsole.conf ] && . /etc/vconsole.conf
	if [ -n "${KEYMAP}" ]; then
		sed -i "/^#define\tKEYBOARD_MAP/c #define KEYBOARD_MAP ${KEYMAP}" config/console.h
	fi

	# change menu colours
	sed -i "/COLOR_[A-Z]*_BG/s/COLOR_BLUE/COLOR_BLACK/" config/colour.h

	make

	mv bin/undionly.kpxe bin/undionly-dist.kpxe
	make bin/undionly.kpxe EMBED=${srcdir}/chain-default.ipxe
}

package() {
	cd "${srcdir}/${_gitname}-build/"
	#pkgver="$(git describe --tag --long | sed -e 's/-/_/g' -e 's/^v//')"

	install -D -m755 ${srcdir}/grub ${pkgdir}/etc/grub.d/20_ipxe

	install -D -m644 COPYING ${pkgdir}/usr/share/licenses/${_gitname}/COPYING
	install -D -m644 COPYRIGHTS ${pkgdir}/usr/share/licenses/${_gitname}/COPYRIGRIGHTS

	cd "${srcdir}/${_gitname}-build/src/"

	install -D -m644 bin/ipxe.dsk ${pkgdir}/usr/share/${_gitname}/ipxe.dsk
	install -D -m644 bin/ipxe.usb ${pkgdir}/usr/share/${_gitname}/ipxe.usb
	install -D -m644 bin/ipxe.iso ${pkgdir}/usr/share/${_gitname}/ipxe.iso

	install -D -m644 bin/undionly-dist.kpxe ${pkgdir}/usr/lib/${_gitname}/undionly.kpxe

	install -D -m644 bin/ipxe.lkrn ${pkgdir}/boot/${_gitname}/ipxe.lkrn

	install -D -m644 bin/undionly.kpxe ${pkgdir}/usr/lib/${_gitname}/undionly-default.kpxe
}

sha256sums=('1dcfdd073c9d5eb728d8a50a290e67eea06554caa5bac90ca0b84259787aa01d'
            'cf01f36c648f76c318c1a0f2a6b43136c261ebe74b4969aa248dd8b14520bc40'
            'b3c9ee1bac1f85f9e8d9455abd94e4a3a0627d2b54ad030e29e247168db2dc83'
            'c1948d2aa6f0a7736a1869eda64db70d48ef5d4d5428e1c8a01d30795d7d287f')
