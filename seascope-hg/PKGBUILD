# Maintainer: Alain Kalker <a.c.kalker at gmail dot com>
pkgname=seascope-hg
pkgver=146
pkgrel=2
pkgdesc="A multi-platform multi-language source code browsing tool"
arch=('any')
url="http://code.google.com/p/seascope/"
license=('BSD')
depends=('python2-qscintilla' 'ctags')
makedepends=('mercurial' 'gendesk')
optdepends=('idutils: backend for C, lex, yacc, limited support for C++, Java'
            'cscope: backend for all languages supported by ctags'
            'global: backend for C, C++, Yacc, Java, PHP'
            'graphviz: generate class graphs')
provides=('seascope')
conflicts=('seascope')
options=(!emptydirs)
source=()

_hgroot="https://code.google.com/p"
_hgrepo="seascope"

_name=('Seascope')
_exec=('Seascope')

build() {
  cd "$srcdir"
  msg "Connecting to Mercurial server...."

  if [[ -d "$_hgrepo" ]]; then
    cd "$_hgrepo"
    hg pull -u
    msg "The local files are updated."
  else
    hg clone "$_hgroot/$_hgrepo"
  fi

  msg "Mercurial checkout done or server timeout"
  msg "Starting build..."

  cd "$srcdir/"
  ln -sf "$srcdir/$_hgrepo/src/icons/seascope.svg" "$pkgname.svg"
  gendesk -n

  rm -rf "$srcdir/$_hgrepo-build"
  cp -r "$srcdir/$_hgrepo" "$srcdir/$_hgrepo-build"
  cd "$srcdir/$_hgrepo-build"

  # Use python2
  find -type f -exec sed -i -e '/^#!/s/python$/python2/' {} \;
}

package() {
  cd "$srcdir/$_hgrepo-build"
  python2 setup.py install --root="$pkgdir/" --optimize=1
  chmod +x "$pkgdir/usr/lib/python2.7/site-packages/Seascope/Seascope.py"
  install -m755 -d "$pkgdir/usr/bin"
  ln -s /usr/lib/python2.7/site-packages/Seascope/Seascope.py "$pkgdir/usr/bin/Seascope"
  install -D -m644 "$srcdir/$pkgname.desktop" "$pkgdir/usr/share/applications/$pkgname.desktop"
  install -D -m644 "$srcdir/$pkgname.svg" "$pkgdir/usr/share/pixmaps/$pkgname.svg"
  install -D -m644 LICENSE "$pkgdir/usr/share/licenses/$pkgname/LICENSE"
}

# vim:set ts=2 sw=2 et:
