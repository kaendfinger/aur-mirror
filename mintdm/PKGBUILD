# Maintainer: Pablo Lezaeta <prflr88 (arroba) gmail (dot) com>
# Please Manjaro Not remove Me from Contributors

pkgname=mintdm
_name=mdm
pkgver=1.1.3
pkgrel=4
pkgdesc="Linux Mint display manager, a fork of GDM 2.20.x"
arch=('i686' 'x86_64')
url="http://www.linuxmint.com"
license=('GPL')
groups=()
depends=('pam>=1.0.2' 'libdmx' 'gtk2>=2.6' 'libgnomecanvas>=1.109.0' 'librsvg>=1.1.1' 'libxml2>=2.4.12' 'libart-lgpl>=2-0' 'dbus-glib>=0.80' 'libwebkit')
makedepends=('gnome-common' 'intltool>=0.35.0' 'perl>=5.8.1' 'gnome-doc-utils>=0.3.2' 'xorg-server' 'pango' 'zenity')
optdepends=()
provides=('gdm' 'gdm-old')
conflicts=('gdm')
replaces=()
backup=()
options=('!libtool')
install='mdm.install'
changelog=
source=("$_name-$pkgver.zip::https://github.com/linuxmint/mdm/archive/1.1.3.zip"
	'mdm.pam' 'mdm.service' 'mdm-autologin.pam' 'defaults.conf' 'org.cinnamon.pkexec.mdmsetup.policy'
	'mdm-plymouth.service' 'cambios.patch')
noextract=()

build() {
	cd $srcdir/$_name-$pkgver
	patch -Np1 -i ../cambios.patch
	chmod +x ./autogen.sh
	./autogen.sh --enable-ipv6=yes --with-prefetch
	./configure --prefix=/usr --with-console-kit=no --sysconfdir=/etc \
	--with-systemd --libexecdir=/usr/lib/mdm --localstatedir=/var/lib \
	--disable-static --with-xevie=yes --disable-scrollkeeper \
	--enable-secureremote=yes --enable-ipv6=yes \
	LDFLAGS="-lXau -lm"
	sed -i -e 's|${prefix}|/usr|' config.h
	make DESTDIR=$pkgdir
}

package() {
	cd $srcdir/$_name-$pkgver
	make DESTDIR=$pkgdir install

msg2 'Adding PAM rules'
	#PAM, we use our own, not LinuxMint stuff, problem?...
	install -m755 -d $pkgdir/etc/pam.d
	install -m644 $srcdir/mdm-autologin.pam $pkgdir/etc/pam.d/mdm-autologin
	install -m644 $srcdir/mdm.pam $pkgdir/etc/pam.d/mdm

msg2 'Adding SystemD services'
	#systemd Init script, but ... SysV Inint is soo old
	install -m755 -d $pkgdir/usr/lib/systemd/system
	install -m644 $srcdir/mdm.service $pkgdir/usr/lib/systemd/system/mdm.service

msg2 'Adding config files'
	#configuration
	install -m444 "${srcdir}/defaults.conf" "${pkgdir}/usr/share/mdm/" 
	rm -f ${pkgdir}/usr/share/xsessions/gnome.desktop || true

msg2 'Adding PolicyKit execution scheme'
	#PolicyKit execution scheme (need some tests)
	install -m755 -d $pkgdir/usr/share/polkit-1/actions/
	install -m644 $srcdir/org.cinnamon.pkexec.mdmsetup.policy $pkgdir/usr/share/polkit-1/actions/

	#Why on the hay this directory is created empty in etc, remove it 
	rmdir $pkgdir/etc/dm

msg2 'Adding faces'
	#Fix mdm files conflict with gnome-control-center (usr/share/pixmaps/faces/*)
	install -m755 -d $pkgdir/usr/share/pixmaps/faces/

	make DESTDIR=$pkgdir install -C gui/faces

msg2 'Adding aplications'
	#Fix gdmsetup.desktop
	sed -i -e 's|^Exec=gksu|Exec=pkexec|' $pkgdir/usr/share/mdm/applications/mdmsetup.desktop
	sed -i -e 's|^NoDisplay=true|NoDisplay=false |' $pkgdir/usr/share/mdm/applications/mdmphotosetup.desktop

	#Fix erroneous path for certain aplications
	install -m755 -d $pkgdir/usr/share/applications
	mv -f $pkgdir/usr/share/mdm/applications/*.* $pkgdir/usr/share/applications
	chmod 755 $pkgdir/usr/share/applications/*.*
	rmdir $pkgdir/usr/share/mdm/applications

}
md5sums=('904f14ab69ea5b1fd8b9806b779f3748'
         '22597f67d6af746bd71e0a1ed09af924'
         '30ee650a9ce9efb7b8614ba18bd6902e'
         '3411d72e42d0ce35c5243a25f0193ad0'
         '3ed499250510d082412bfdc8c351fa7c'
         '226bbed682a8d8588659ef6092d601ad'
         '921261984a4d25ba7eea12aae3b88661'
         '0d9a84d7ad6e6bcc24699d3d4fdfaa80')
