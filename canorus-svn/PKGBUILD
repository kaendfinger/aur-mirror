# Contributor: Coenraad van der Westhuizen <chwesthuizen@gmail.com>

_pkgname=canorus
pkgname=$_pkgname-svn
pkgver=1356
pkgrel=1
pkgdesc="A free music score editor"
arch=(i686 x86_64)
url=http://sourceforge.net/projects/$_pkgname/
license=(GPL2 GPL3)
depends=(poppler-qt qtwebkit)
makedepends=(cmake swig ruby python2)
#htlatex qcollectiongenerator lyx ps2pdf - for docs
options=(!buildflags)

_svntrunk=https://$_pkgname.svn.sourceforge.net/svnroot/$_pkgname/trunk
_svnmod=trunk

build() {
    install -d "$srcdir"/$_pkgname/
    cd "$srcdir"/$_pkgname/

    msg "Starting SVN checkout..."
    if [[ -d $_svnmod/.svn ]]; then
        pushd $_svnmod && svn up -r $pkgver
        msg2 "The local files have been updated."
        popd
    else
        svn co $_svntrunk --config-dir ./ -r $pkgver $_svnmod
    fi
    msg2 "SVN checkout done or server timeout"

    rm -rf $_svnmod-build
    cp -r $_svnmod $_svnmod-build
    cd $_svnmod-build

    msg "Compiling..."

    # Scripting disabled.  As of 12/09/12:
    # * will not compile w/ swig enabled
    # * I can't find ruby dev libs (deps)

    sed -e '623,623 s:):z ):' -e 's:.*/doc[/)].*::g' -i src/CMakeLists.txt
    cmake . -DCMAKE_INSTALL_PREFIX=/usr \
        -DSWIG_DIR=/usr/share/swig \
        -DQT_MOC_EXECUTABLE=/usr/bin/moc \
        -DQT_UIC_EXECUTABLE=/usr/bin/uic \
        -Wno-dev \
        -DNO_RUBY=true \
        -DNO_SWIG=true
    make
}

package() {
    cd "$srcdir"/$_pkgname/$_svnmod-build/
    make DESTDIR="$pkgdir" install
}
