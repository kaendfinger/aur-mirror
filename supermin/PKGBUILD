# Maintainer: Xiao-Long Chen <chenxiaolong@cxl.epac.to>

pkgname=supermin
pkgver=4.1.1
pkgrel=1
pkgdesc="Tool for creating supermin appliances"
arch=('i686' 'x86_64')
url="http://people.redhat.com/~rjones/supermin/"
license=('GPL')
makedepends=('ocaml' 'ocaml-findlib' 'prelink')
# Does not provide febootstrap because supermin is not compatible with it
provides=()
conflicts=('febootstrap<=3.21' 'supermin-git')
source=("http://libguestfs.org/download/supermin/supermin-${pkgver}.tar.gz")
sha512sums=('eff201555ae1caa45dc1a2e9d69249d94b872750ec14df01e923a5bccef7a8d45f5ec00a528f3679c3ae670abeb967d928be032d58c53390c86a6c295d5f6bae')

build() {
  cd "${srcdir}/${pkgname}-${pkgver}"
  ./configure --prefix=/usr
  make
}

check() {
  cd "${srcdir}/${pkgname}-${pkgver}"
  #make -k check
  make check
}

package() {
  cd "${srcdir}/${pkgname}-${pkgver}"
  make DESTDIR="${pkgdir}/" install

  # From Fedora's spec file:
  #   supermin-helper is marked as requiring an executable stack.  This
  #   happens because we use objcopy to create one of the component object
  #   files from a data file.  The program does not in fact require an
  #   executable stack.  The easiest way to fix this is to clear the flag
  #   here.
  execstack -c "${pkgdir}/usr/bin/supermin-helper"
}

# vim:set ts=2 sw=2 et:
