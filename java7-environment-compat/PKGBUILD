# Maintainer:  Mattias Andrée

pkgname=java7-environment-compat
pkgver=7.7.15
pkgrel=1
_ver=7u15
_build=b03
_docver=7u15
_docbuild=b02
pkgdesc='Java 7 Development Kit that can be install alongside any other version, includes extracted library source code, Java API in HTML and some /usr/bin/*7 commands'
url=http://www.oracle.com/technetwork/java/javase/downloads/index.html
arch=(i686 x86_64)
license=(custom)
makedepends=(unzip)
DLAGENTS=('http::/usr/bin/curl -fLC - --retry 3 --retry-delay 3 -o %o %u --header "Cookie:oraclelicensejdk-${_ver}-oth-JPR=accept-securebackup-cookie;gpw_e24=http%3A%2F%2Fwww.oracle.com%2Ftechnetwork%2Fjava%2Fjavase%2Fdownloads%2Fjdk${_ver}-downloads-1836413.html"')
source=("http://download.oracle.com/otn-pub/java/jdk/$_ver-$_build/jdk-$_ver-linux-x64.tar.gz"
	"http://download.oracle.com/otn-pub/java/jdk/$_docver-$_docbuild/jdk-$_docver-apidocs.zip")
sha384sums=(c31734ca312c5670cd77b29b2e76681987d97d1660c241f88e1ae074747bedef88ea34594abd74d6151b4647388dbc97
	    142800f96ac8eb3252a5126998c9cab57a793ef02af4f7f91674825b58243a43d140ef0827b37965cbfcf248f6985eaf)

## javase: http://www.oracle.com/technetwork/java/javase/downloads/jdk7-downloads-1880260.html
## apidoc: http://www.oracle.com/technetwork/java/javase/documentation/java-se-7-doc-download-435117.html

[ "${CARCH}" == 'i686' ] &&
  source[0]="http://download.oracle.com/otn-pub/java/jdk/$_ver-$_build/jdk-$_ver-linux-i586.tar.gz" &&
  sha384sums[0]=6a69c328bfa8a0cae3b14db69eec19d9ef49c9b6bbed272244c8cb5dcec6c44abaf4bfd0d1fd5286d0d4c585bab832b5

_binfiles="appletviewer jar jarsigner java javac javadoc javah javap jdb serialver"

build()
{
	msg "Extracting library source code"
	cd $srcdir/jdk1.7.*
	mkdir src
	mv src.zip src
	cd src
	unzip src.zip
	unlink src.zip
}

package()
{
	msg "Correcting file structure"
	mkdir -p $pkgdir/opt/jdk7
	mv $srcdir/* $pkgdir/opt/jdk7
	unlink $pkgdir/opt/jdk7/jdk-*-apidocs.zip
	unlink $pkgdir/opt/jdk7/jdk-*-linux-*.tar.gz
	mv $pkgdir/opt/jdk7/jdk1.7.*/* $pkgdir/opt/jdk7
	rmdir $pkgdir/opt/jdk7/jdk1.7.*
	mkdir -p $pkgdir/usr/bin
	
	msg "Making command symlinks to /usr/bin/"
	for file in $_binfiles; do
	    echo "Making link /usr/bin/${file}7 -> /opt/jdk7/bin/${file}"
	    ln -s /opt/jdk7/bin/${file} $pkgdir/usr/bin/${file}7
	done
}
