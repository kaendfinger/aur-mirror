# Contributor : Jan de Groot <jgc@archlinux.org>
# Contributor: Andreas Radke <andyrtr@archlinux.org>
# Maintainer: Lone_Wolf <lonewolf@xs4all.nl>

pkgname=lib32-mesa-r300-r600-radeonsi-git
pkgver=20130309
pkgrel=1
_realver=9.2
pkgdesc="LIB32 Mesa R300, R600 & Radeon SI - git version."
arch=('x86_64')
depends=('lib32-libxt' 'lib32-libxxf86vm' 'lib32-libxdamage' 'gcc-multilib' 'lib32-libdrm' 'mesa-r300-r600-radeonsi-git' 'lib32-systemd' 'lib32-libxvmc' 'lib32-libvdpau' 'lib32-llvm-amdgpu-svn')
makedepends=('pkgconfig' 'python2' 'talloc' 'libxml2' 'imake' 'git' 'glproto' 'dri2proto' 'xorg-server-devel' 'resourceproto')
optdepends=('lib32-libtxc_dxtn: S3TC support'
            'lib32-mesa-demos: glxinfo and glxgears')
provides=("lib32-mesa=${_realver}" "lib32-libgl=${_realver}" "lib32-ati-dri=${_realver}" "lib32-libglapi=${_realver}" "lib32-osmesa=${_realver}" "lib32-libgbm=${_realver}" "lib32-libgles=${_realver}" "lib32-libegl=${_realver}")
conflicts=('xf86-video-ati<6.9.0-6' lib32-mesa lib32-libgl lib32-ati-dri lib32-libglapi lib32-libegl lib32-libgles lib32-libgbm lib32-osmesa)
url="http://mesa3d.sourceforge.net"
license=(custom)
options=(!libtool)
_gitroot='git://anongit.freedesktop.org/git/mesa/mesa'
_gitname='mesa'

build() {
    cd "$srcdir"
  msg "Connecting to git.freedesktop.org GIT server...."

  if [[ -d "$_gitname" ]]; then
    cd "$_gitname" && git pull origin
    msg "The local files are updated."
  else
    git clone "$_gitroot" "$_gitname"
  fi

  msg "GIT checkout done or server timeout"
  msg "Starting build..."

  rm -rf "$srcdir/$_gitname-build"
  git clone "$srcdir/$_gitname" "$srcdir/$_gitname-build"
  cd "$srcdir/$_gitname-build"
  export PKG_CONFIG_PATH="/usr/lib32/pkgconfig"
  # for our llvm-config for 32 bit
  export LLVM_CONFIG=/usr/bin/llvm-config32
  CFLAGS="-m32" CXXFLAGS="-m32" ./autogen.sh \
    --prefix=/usr \
    --sysconfdir=/etc \
    --with-dri-driverdir=/usr/lib32/xorg/modules/dri \
    --with-gallium-drivers=r300,r600,radeonsi,swrast \
    --with-dri-drivers=swrast \
    --enable-gallium-llvm \
    --enable-egl \
    --enable-gallium-egl \
    --with-egl-platforms=x11,drm \
    --enable-shared-glapi \
    --enable-gbm \
    --enable-glx-tls \
    --enable-dri \
    --enable-glx \
    --enable-osmesa \
    --enable-gles1 \
    --enable-gles2 \
    --enable-texture-float \
    --enable-xa \
    --enable-vdpau \
    --enable-gallium-gbm \
    --enable-gallium-g3dvl \
    --enable-xvmc \
    --enable-xorg \
    --enable-r600-llvm-compiler \
    --with-llvm-shared-libs \
    --enable-32-bit \
    --libdir=/usr/lib32 \


# left out compile flags
# default = auto 
#  --enable-64-bit         build 64-bit libraries [default=auto]
#  --enable-vdpau          enable vdpau library [default=auto]
#  --disable-driglx-direct enable direct rendering in GLX and EGL for DRI [default=auto]
#  --enable-va             enable va library [default=auto]

#
# default = enabled/yes
#  --enable-shared[=PKGS]       build shared libraries [default=yes]
#  --enable-fast-install[=PKGS] optimize for fast installation [default=yes]
#  --disable-asm                disable assembly usage [default=enabled on supported plaforms]
#  --disable-pic                compile PIC objects [default=enabled for shared builds on supported platforms]
#
# default = disabled/no
#  --enable-debug          use debug compiler flags and macros [default=disabled]
#  --enable-mangling       enable mangled symbols and library name [default=disabled]
#  --enable-selinux        Build SELinux-aware Mesa [default=disabled]
#  --disable-opengl        disable support for standard OpenGL API [default=no]
#  --enable-opencl         enable OpenCL library [default=no]
#  --enable-xlib-glx       make GLX library Xlib-based instead of DRI-based [default=disable]
#  --enable-gallium-tests  Enable optional Gallium tests) [default=disable]
#
  make
}

package() {

  cd "${srcdir}/${_gitname}-build" 
  make DESTDIR="${pkgdir}" install

  mv "${pkgdir}"/usr/lib32/libGL.so.1.2.0 "${pkgdir}"/usr/lib32/mesa-libGL.so.1.2.0
  rm "${pkgdir}"/usr/lib32/libGL.so{,.1}
  
  ln -s mesa-libGL.so.1.2.0 ${pkgdir}/usr/lib32/libGL.so
  ln -s mesa-libGL.so.1.2.0 ${pkgdir}/usr/lib32/libGL.so.1
  ln -s mesa-libGL.so.1.2.0 ${pkgdir}/usr/lib32/libGL.so.1.2.0

# removing folders/files that are already provided by mesa-r300-r600-radeonsi-git package
  rm -rf "${pkgdir}"/etc
  rm -rf "${pkgdir}"/usr/include

#license
  install -m755 -d "${pkgdir}/usr/share/licenses/${pkgname}"
  install -m644 "${srcdir}/LICENSE" "${pkgdir}/usr/share/licenses/${pkgname}/"
}
