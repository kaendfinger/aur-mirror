# Maintainer: Kuba Serafinowski <zizzfizzix(at)gmail(dot)com>
# https://github.com/zizzfizzix/pkgbuilds

##############################################################
#### The section below can be adjusted to suit your needs ####
##############################################################

# What type of build do you want?
# See http://techbase.kde.org/Development/CMake/Addons_for_KDE#Buildtypes to check what is supported.
# Default is RelWithDebInfo to help with debugging.

_buildtype="Release"

##############################################################

_name=mirall
pkgname=owncloud-client
pkgver=1.2.1
pkgrel=2
pkgdesc="ownCloud client based on mirall"
arch=('i686' 'x86_64')
url="http://owncloud.org/"
license=('GPL2')
depends=('qt4' 'ocsync')
makedepends=('cmake')
provides=('mirall' 'owncloud-client')
conflicts=('mirall-git')
install=owncloud-client.install
backup=('etc/owncloud-client/sync-exclude.lst')
source=("http://download.owncloud.com/download/${_name}-${pkgver}.tar.bz2"
        "${pkgname}.desktop")
md5sums=('7a3f3d927924392cffc9bb2fbbd85d19'
         'e223d162626c2ff54efebaa90a182d81')

# Clean options array to strip pkg if release buildtype is chosen
if [[ ${_buildtype} == "Release" ]] || [[ ${_buildtype} == "release" ]]; then
  options=()
fi

build() {
  if [[ -e ${srcdir}/${_name}-${pkgver}-build ]]; then rm -rf ${srcdir}/${_name}-${pkgver}-build; fi
  mkdir ${srcdir}/${_name}-${pkgver}-build
  cd ${srcdir}/${_name}-${pkgver}-build

  cmake -DQT_QMAKE_EXECUTABLE=qmake-qt4 \
        -DCMAKE_INSTALL_PREFIX=/usr \
        -DCMAKE_INSTALL_LIBDIR=/usr/lib \
        -DCMAKE_BUILD_TYPE=${_buildtype} \
        -DCSYNC_INCLUDE_PATH=/usr/include/ocsync \
        -DCMAKE_INSTALL_SYSCONFDIR=/etc/${pkgname} \
        ../${_name}-${pkgver}
  make
}

package() {
  cd ${srcdir}/${_name}-${pkgver}-build
  make DESTDIR=${pkgdir} install
  install -Dm644 ${srcdir}/${pkgname}.desktop ${pkgdir}/usr/share/applications/${pkgname}.desktop
}
