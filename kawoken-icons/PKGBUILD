# Maintainer: Limao Luo <luolimao+AUR@gmail.com>
# Contributor: sausageandeggs <sausageandeggs@archlinux.us>

_pkgname=kAwOken
pkgname=kawoken-icons
pkgver=1.2
pkgrel=2
pkgdesc="Simple and quite complete icon set, Token-style. Ported to kde"
arch=(any)
url=http://alecive.deviantart.com/art/$_pkgname-244166779
license=('CCPL:by-nc-sa')
depends=(kdelibs librsvg)
optdepends=('imagemagick: to color the iconset'
    'xdg-utils: to be able to launch programs from the configuration script.'
    'zenity: for customization gui')
options=(!emptydirs !strip)
install=kawoken.install
source=(http://www.deviantart.com/download/244166779/kawoken_by_alecive-d41dcaj.zip)
sha256sums=('88b8fe54365c862b903d7879efe2346b96984698e14922163e380c48bad3891d')
sha512sums=('c6c3215dbc4bafa94512c2132c3d540ce14b05d0f52e5b2871c0865ae9568da4d74af8fa5f489b0c3d05a09004d1be9c1e3c03d8ee9684d43fbca6c5102f576b')

package() {
    cd "$srcdir"/$_pkgname-$pkgver/

    install -d "$pkgdir"/usr/share/icons/
    for archive in ${_pkgname}{,Dark,White}.tar.gz; do
        tar xzf $archive -C "$pkgdir"/usr/share/icons
    done

    cd "$pkgdir"/usr/share/icons/
    find -type d -exec chmod 755 '{}' \;
    find -type f -exec chmod 644 '{}' \;
    find -name '*.sh' -exec chmod 755 '{}' \;
    chown -R root:root *

    install -Dm644 $_pkgname/Installation_and_Instructions.pdf "$pkgdir"/usr/share/doc/$pkgname/Customization.pdf
    rm $_pkgname/Installation_and_Instructions.pdf

    local scripts=($_pkgname/kawoken-icon-theme-customization{,-clear}
        ${_pkgname}Dark/kawoken-icon-theme-customization-dark
        ${_pkgname}White/kawoken-icon-theme-customization-white)

    for i in ${_pkgname}{,Dark,White}/index.theme ${scripts[@]}; do
        chmod 755 $i
    done
    install -d "$pkgdir"/usr/bin/
    for i in ${scripts[@]}; do
        ln -s /usr/share/icons/$i "$pkgdir"/usr/bin/
    done
}
