# Maintainer: Tianjiao Yin <ytj000@gmail.com>

pkgname=waf-docs
pkgver=1.7.8
pkgrel=1
pkgdesc="Documentation for Waf, a general-purpose build system modelled after Scons"
url="http://code.google.com/p/waf/"
arch=('any')
license=("BSD")
makedepends=("python2-sphinx" "texlive-latexextra" "asciidoc" 
             "dia" "imagemagick" "source-highlight"
             "dblatex" "waf" "graphviz")
source=("https://waf.googlecode.com/files/waf-$pkgver.tar.bz2"
        "collapsiblesidebar.patch")
md5sums=('24f80ee6618f3435ef7fd67c23ceb4a6'
         'd6f863bbae3df76e26b7aa9267259d04')

build() {

  # Part 1, build waf documentation
  ###########################################################################

  cd "$srcdir/waf-$pkgver/docs/sphinx"
  sed -i "s/sphinx-build/sphinx-build2/g" Makefile
  patch conf.py $srcdir/collapsiblesidebar.patch

  make html
  make latex
  make -C build/latex
  mv build/latex/waf.pdf build
  rm build/latex -rf

  # Part 2, build waf book
  ###########################################################################

  cd "$srcdir/waf-$pkgver/docs/book"
  waf configure
  waf
}

package() {
  cd "$srcdir/waf-$pkgver/docs/sphinx"

  rm build/doctrees -rf
  mkdir -p $pkgdir/usr/share/doc/
  mv build $pkgdir/usr/share/doc/waf
  mv $srcdir/waf-$pkgver/docs/book/build $pkgdir/usr/share/doc/waf/book

  mkdir -p $pkgdir/usr/share/doc/waf/bookpdf
  mv $pkgdir/usr/share/doc/waf/book/*.pdf $pkgdir/usr/share/doc/waf/bookpdf
}

# vim:set ts=2 sw=2 et:
