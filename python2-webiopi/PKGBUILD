# Maintainer: TDY <tdy@archlinux.info>

pkgname=python2-webiopi
pkgver=0.5.3
pkgrel=1
pkgdesc="A Raspberry Pi REST framework to control the GPIO and more"
arch=('armv6h')
url="http://code.google.com/p/webiopi/"
license=('APACHE')
depends=('python2')
makedepends=('python2-distribute')
conflicts=('python-webiopi')
install=webiopi.install
source=(http://webiopi.googlecode.com/files/WebIOPi-$pkgver.tar.gz
        webiopi.service)
sha256sums=('b21d1b36d273eaf95b6ea9555cf857f7007c0a054410ce75a7418de85b900d40'
            '14fefdffaec288d3fcfb1ed62ac47cb40d89e1cfe1dff5043b575ae684c2dd77')

build() {
  cd "$srcdir/WebIOPi-$pkgver/python"
  python2 setup.py build
  find ! -executable -exec sed -i '1s/python/&2/' '{}' \;
}

package() {
  cd "$srcdir/WebIOPi-$pkgver/python"
  python2 setup.py install --root="$pkgdir"

  # passwd
  install -Dm755 ${pkgname#*-}-passwd.py "$pkgdir/usr/bin/${pkgname#*-}-passwd"
  install -Dm644 passwd "$pkgdir/etc/${pkgname#*-}/passwd"

  # resources
  install -dm755 "$pkgdir/usr/share/${pkgname#*-}/htdocs/app/expert"
  install -m644 ../htdocs/app/expert/* "$pkgdir/usr/share/${pkgname#*-}/htdocs/app/expert"
  install -m644 ../htdocs/*.* "$pkgdir/usr/share/${pkgname#*-}/htdocs"

  # service
  install -Dm644 "$srcdir/${pkgname#*-}.service" \
      "$pkgdir/usr/lib/systemd/system/${pkgname#*-}.service"

  # fix module import
  _V="$(python2 -V |& awk '{print $2}' | sed 's/\.[0-9]*$//')"
  touch "$pkgdir/usr/lib/python$_V/site-packages/_webiopi/__init__.py"
}

# vim:set ts=2 sw=2 et:
