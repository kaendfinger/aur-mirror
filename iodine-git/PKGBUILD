# Maintainer: Pascal 'hardfalcon' Ernster <aur@hardfalcon.net>
# Contributor: nofxx <x@nofxx.com>
# Contributor: Isaac Aronson <i@pingas.org>
# Contributor: Daenyth <Daenyth+Arch [at] gmail [dot] com>
pkgname=iodine-git
_pkgname=iodine
pkgver=20121222
_pkgver=0.6.0-rc1
pkgrel=1
pkgdesc="Lets you tunnel IPv4 data through a DNS server."
arch=('i686' 'x86_64' 'armv5' 'armv6h' 'armv7h' 'mips64el')
url="http://code.kryo.se/iodine"
license=('custom: ISC')
depends=('net-tools')
checkdepends=('check')
provides=("${_pkgname}=${_pkgver}")
backup=("etc/conf.d/${_pkgname}"
	"etc/conf.d/${_pkgname}d")
install="${_pkgname}.install"
source=("${_pkgname}.conf"
	"${_pkgname}d.conf"
	"${_pkgname}.service"
	"${_pkgname}d.service")
sha256sums=('a19c3c0c807b7e42557cb9ddf5d5f32c05eed9f835f0e52b5bb0d96a368df16b'
	'c1b179b7c7683ad5a1069706bb5f0096132787cb1eb2087a45931f21869e6d42'
	'1dc081e029f3b0cec044bf6d07448dcf79890a434b50404400482f0bab4cb693'
	'1e80d9d3d7f87b10e35d9f14220241ce8d6b00fcb834e40345bebcfd5b850001')


_gitroot="git://github.com/yarrick/${_pkgname}.git"
_gitname="${_pkgname}"

build() {
	cd "${srcdir}"
	msg "Connecting to GIT server...."

	if [[ -d "${_gitname}" ]]; then
		cd "${_gitname}" && git pull origin
		msg "The local files are updated."
	else
		git clone "${_gitroot}" "${_gitname}"
	fi

	msg "GIT checkout done or server timeout"
	msg "Starting build..."

	rm -rf "${srcdir}/${_gitname}-build"
	git clone "${srcdir}/${_gitname}" "${srcdir}/${_gitname}-build"

	cd "${srcdir}/${_gitname}-build"
	make
}


check() {
	cd "${srcdir}/${_gitname}-build"
	make test
}

package() {
	cd "${srcdir}/${_gitname}-build"
	make prefix="/usr" DESTDIR="${pkgdir}" install

	install -Dm600 "${srcdir}/${_pkgname}.conf" "${pkgdir}/etc/conf.d/${_pkgname}"
	install -Dm600 "${srcdir}/${_pkgname}d.conf" "${pkgdir}/etc/conf.d/${_pkgname}d"
	install -Dm644 "${srcdir}/${_pkgname}.service" "${pkgdir}/usr/lib/systemd/system/${_pkgname}.service"
	install -Dm644 "${srcdir}/${_pkgname}d.service" "${pkgdir}/usr/lib/systemd/system/${_pkgname}d.service"

	install -d "${pkgdir}/usr/share/licenses/${_pkgname}"
	sed -n '/AUTHORS & LICENSE/,$p' README > "${pkgdir}/usr/share/licenses/${_pkgname}/LICENSE"
}

