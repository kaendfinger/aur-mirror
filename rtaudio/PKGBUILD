# Maintainer: Limao Luo <luolimao+AUR@gmail.com>
# Contributor: rtfreedman
# Contributor: SpepS <dreamspepser@yahoo.it>

pkgname=rtaudio
pkgver=4.0.11
pkgrel=1
pkgdesc="A set of C++ classes that provide a common API for realtime audio input/output."
arch=(i686 x86_64)
url=http://www.music.mcgill.ca/~gary/$pkgname
license=(custom)
depends=(jack)
makedepends=(python2-distribute)
optdepends=('python2: python bindings')
source=($url/release/$pkgname-$pkgver.tar.gz
    LICENSE)
sha256sums=('1167ebd33515c064cd7c2d7c7b44f34998ab016a182c1874d70790163eced311'
    'a4ccad379c6c17138a99fe445882fa0d59e0c061bc0cff82cc5708ede9224224')
sha512sums=('92611b9bba4984c6e29670b54d47ac474d884efab364bc2760876c8555b4e152845bd3763028497482a002282761be8e80f8f6aa775933ef7db9d8a13239770a'
    'aea65fe4cb5e66d83edc24c9f16b93b631d3f2a53e9dcbee135385d6ab7c4e336f6fb772d979d778c4b3be06300adb4ed7b034282e57ea58da21070c7c5d7176')

build() {
    cd "$srcdir"/$pkgname-$pkgver/

    sed -i 's/__LINUX_OSS__/__LINUX_NO_OSS__/g' contrib/python/pyrtaudio/setup.py
    ./configure --prefix=/usr --with-alsa --with-jack
    make

    pushd tests/
    make
    popd

    cd contrib/python/pyrtaudio/
    CFLAGS="$CFLAGS -I../../../include" python2 setup.py build
}

package() {
    cd "$srcdir"/$pkgname-$pkgver/

    install -Dm755 librtaudio.so "$pkgdir"/usr/lib/librtaudio.so
    install -Dm755 rtaudio-config "$pkgdir"/usr/bin/rtaudio-config

    install -d "$pkgdir"/usr/share/doc/$pkgname
    cp -a doc/{html,images,release.txt} "$pkgdir"/usr/share/doc/$pkgname/

    install -d "$pkgdir"/usr/include/$pkgname
    install *.h include/* "$pkgdir"/usr/include/$pkgname/

    install -Dm644 ../LICENSE "$pkgdir"/usr/share/licenses/$pkgname/LICENSE

    for i in $(find tests -maxdepth 1 -perm 755 -type f); do
        install -Dm755 $i "$pkgdir"/usr/bin/$pkgname-${i/*\//}
    done

    cd contrib/python/pyrtaudio/
    python2 setup.py install --root="$pkgdir"

    install -Dm644 PyRtAudioTest.py "$pkgdir"/usr/share/doc/$pkgname/
    sed -i "/inline/s/;$//" "$pkgdir"/usr/include/$pkgname/RtAudio.h
}
