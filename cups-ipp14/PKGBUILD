# $Id$
# Maintainer: Gicu GORODENCO <cyclopsihus@gmail.com>
# Contributor (cups): Andreas Radke <andyrtr@archlinux.org>

_pkgbase="cups"
pkgbase="${_pkgbase}-ipp14"
### Comming the "split packages sections - unsupported by AUR apparently ###
#pkgname=('libcups-ipp14' 'cups-ipp14')
pkgname=(${pkgbase})
pkgver=1.6.1
pkgrel=7
arch=('i686' 'x86_64')
provides=('cups')
replaces=('cups')
conflicts=('cups')
license=('GPL')
url="http://www.cups.org/"
makedepends=('libtiff>=4.0.0' 'libpng>=1.5.7' 'acl' 'pam' 'xdg-utils' 'krb5' 'gnutls>=2.8.3' 'cups-filters' 'bc'
             'colord' 'xinetd' 'gzip' 'autoconf' 'libusb' 'dbus-core' 'avahi'  'hicolor-icon-theme' 'systemd')
source=(ftp://ftp.easysw.com/pub/cups/${pkgver}/cups-${pkgver}-source.tar.bz2
        cups-no-export-ssllibs.patch
        cups-no-gcrypt.patch
        cups-no-gzip-man.patch
        cups-systemd-socket.patch
        cups cups.logrotate cups.pam
        cupsd-conf-remove-obsolete-browse-directives.patch
        avahi-missing-in-conditionals.patch
        recognize-remote-cups-queue-via-dnssd-uri.patch
        usb-backend-reset-after-job-only-for-specific-devices.patch
        add-ipp-backend-of-cups-1.4.patch)
#options=('!emptydirs')
md5sums=('87ade07e3d1efd03c9c3add949cf9c00'
         '9b8467a1e51d360096b70e2c3c081e6c'
         '3733c23e77eb503bd94cc368e02830dc'
         '033c20fb7b7ccfd0a005d08a27bf6d52'
         'b1d8f2021d6924d0d0beffc900261408'
         '9657daa21760bb0b5fa3d8b51d5e01a1'
         'f861b18f4446c43918c8643dcbbd7f6d'
         '96f82c38f3f540b53f3e5144900acf17'
         '56067676abee53cc9768c1eeee6a8d68'
         'a92c7c0b8b156d7f16bcf9fa45646adc'
         '6079620cf0cf20725b92487a232309cd'
         'b7af93f3998236f39d72f4a042a4a1fe'
         '49e1c75beb87e9b7c9511f59509839e7')

build() {
  cd ${srcdir}/${_pkgbase}-${pkgver}

  # Applying patch to add ipp14 backend:
  patch -Np1 -i ${srcdir}/add-ipp-backend-of-cups-1.4.patch
  # add systemd socket support - Fedora patch, also used in Gentoo
  # modified now to the changes done by Gentoo in their svn ebuild
  # http://sources.gentoo.org/cgi-bin/viewvc.cgi/gentoo-x86/net-print/cups/files/cups-1.5.0-systemd-socket.patch?revision=1.1
  patch -Np1 -i ${srcdir}/cups-systemd-socket.patch

  # Do not export SSL libs in cups-config
  patch -Np1 -i "${srcdir}/cups-no-export-ssllibs.patch"

  patch -Np1 -i "${srcdir}/cups-no-gcrypt.patch"
  # don't zip man pages in make install, let makepkg do that / Fedora
  patch -Np1 -i ${srcdir}/cups-no-gzip-man.patch

  # upstream bugtracker patches
  # http://www.cups.org/str.php?L4157 - fix broken default server config
  patch -Np1 -i ${srcdir}/cupsd-conf-remove-obsolete-browse-directives.patch
  # http://www.cups.org/str.php?L4156 - In many DNS-SD/Bonjour conditionals Avahi is not considered
  patch -Np1 -i ${srcdir}/avahi-missing-in-conditionals.patch
  # http://www.cups.org/str.php?L4158 - DNS-SD-based print queues pointing to CUPS server are not considered remote printers with driver on server
  patch -Np1 -i ${srcdir}/recognize-remote-cups-queue-via-dnssd-uri.patch
  # http://cups.org/str.php?L4155 / https://bugs.archlinux.org/task/30965
  patch -Np0 -i ${srcdir}/usb-backend-reset-after-job-only-for-specific-devices.patch
  
  # Rebuild configure script for not zipping man-pages.
  aclocal -I config-scripts
  autoconf -I config-scripts

  ./configure --prefix=/usr --sysconfdir=/etc --localstatedir=/var \
     --libdir=/usr/lib \
     --with-systemdsystemunitdir=/usr/lib/systemd/system \
     --with-logdir=/var/log/cups \
     --with-docdir=/usr/share/cups/doc \
     --with-cups-user=daemon \
     --with-cups-group=lp \
     --enable-pam=yes \
     --enable-raw-printing \
     --enable-dbus --with-dbusdir=/etc/dbus-1 \
     --enable-ssl=yes --enable-gnutls \
     --enable-threads \
     --enable-avahi\
     --with-php=/usr/bin/php-cgi \
     --with-optim="$CFLAGS"
  make
}

check() {
  cd "$srcdir/${_pkgbase}-$pkgver"
  #httpAddrGetList(workstation64): FAIL
  #1 TESTS FAILED!
  #make[1]: *** [testhttp] Error 1
  make -k check || /bin/true
}

### Commenting the package section for libcups - unsupported by AUR ###
#package_libcups-ipp14() {
#pkgdesc="The CUPS Printing System - client libraries and headers"
#depends=('gnutls>=2.8.3' 'libtiff>=4.0.0' 'libpng>=1.5.7' 'krb5' 'avahi')
#backup=(etc/cups/client.conf)
#
#  cd ${srcdir}/${_pkgbase}-${pkgver}
#  make BUILDROOT=${pkgdir} install-headers install-libs
#  # put this into the libs pkg to make other software find the libs(no pkg-config file included)
#  mkdir -p ${pkgdir}/usr/bin
#  install -m755 ${srcdir}/${_pkgbase}-${pkgver}/cups-config ${pkgdir}/usr/bin/cups-config
#
#  # install client.conf man page and config file
#  install -dm755 ${pkgdir}/usr/share/man/man5
#  #install -Dm644  ${srcdir}/${_pkgbase}-${pkgver}/man/client.conf.5.gz ${pkgdir}/usr/share/man/man5/
#  install -Dm644  ${srcdir}/${_pkgbase}-${pkgver}/man/client.conf.5 ${pkgdir}/usr/share/man/man5/
#  install -dm755 ${pkgdir}/etc/cups
#  touch ${pkgdir}/etc/cups/client.conf
#  echo "# see 'man client.conf'" >> ${pkgdir}/etc/cups/client.conf
#  echo "ServerName /var/run/cups/cups.sock #  alternative: ServerName hostname-or-ip-address[:port] of a remote server" >> ${pkgdir}/etc/cups/client.conf
#  chgrp lp ${pkgdir}/etc/cups/client.conf
#}

package_cups-ipp14() {
pkgdesc="The CUPS Printing System - daemon package"
install=cups.install
backup=(etc/cups/cupsd.conf
        etc/cups/snmp.conf
        etc/cups/printers.conf
        etc/cups/classes.conf
        etc/cups/subscriptions.conf
        etc/dbus-1/system.d/cups.conf
        etc/logrotate.d/cups
        etc/pam.d/cups
        etc/xinetd.d/cups-lpd)
depends=('acl' 'pam' "libcups>=${pkgver}" 'cups-filters' 'bc' 'colord' 'libusb' 'dbus-core' 'libsystemd' 'hicolor-icon-theme')
optdepends=('xdg-utils: xdg .desktop file support')

  cd ${srcdir}/${_pkgbase}-${pkgver}
  make BUILDROOT=${pkgdir} install-data install-exec

  # this one we ship in the libcups pkg
  rm -f ${pkgdir}/usr/bin/cups-config

  # kill the sysv stuff
  rm -rf ${pkgdir}/etc/rc*.d
  rm -rf ${pkgdir}/etc/init.d
  install -D -m755 ../cups ${pkgdir}/etc/rc.d/cupsd
  install -D -m644 ../cups.logrotate ${pkgdir}/etc/logrotate.d/cups
  install -D -m644 ../cups.pam ${pkgdir}/etc/pam.d/cups
  
  # fix perms on /var/spool and /etc
  chmod 755 ${pkgdir}/var/spool
  chmod 755 ${pkgdir}/etc

  # install ssl directory where to store the certs, solves some samba issues
  install -dm700 -g lp ${pkgdir}/etc/cups/ssl
  # remove directory from package, we create it in cups rc.d file
  rm -rf ${pkgdir}/var/run

  # install some more configuration files that will get filled by cupsd
  touch ${pkgdir}/etc/cups/printers.conf
  touch ${pkgdir}/etc/cups/classes.conf
  touch ${pkgdir}/etc/cups/subscriptions.conf 
  chgrp lp ${pkgdir}/etc/cups/{printers.conf,classes.conf,subscriptions.conf}
  
  # fix .desktop file
  sed -i 's|^Exec=htmlview http://localhost:631/|Exec=xdg-open http://localhost:631/|g' ${pkgdir}/usr/share/applications/cups.desktop
  
  # compress some driver files, adopted from Fedora
  find ${pkgdir}/usr/share/cups/model -name "*.ppd" | xargs gzip -n9f

  # remove client.conf man page
  rm -f ${pkgdir}/usr/share/man/man5/client.conf.5

  # remove files now part of cups-filters
  rm -v ${pkgdir}/usr/share/cups/banners/*
  rm -v ${pkgdir}/usr/share/cups/data/testprint
  # comment out all conversion rules which use any of the removed filters
  perl -p -i -e 's:^(.*\s+bannertops\s*)$:#\1:' /usr/share/cups/mime/mime.convs

  # Provide native service for arch-daemons generator
  ln -s cups.service "$pkgdir/usr/lib/systemd/system/cupsd.service"
}
