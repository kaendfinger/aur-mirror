# $Id$
# Maintainer: Jean-Baptiste Giraudeau <jb@giraudeau.info>
# Contributor: Jan de Groot <jgc@archlinux.org>
# Contributor: Andrea Scarpino <andrea@archlinux.org>
# Contributor: Pierre Schmitz <pierre@archlinux.de>

pkgname=apache24
pkgver=2.4.4
pkgrel=1
pkgdesc='A high performance Unix-based HTTP server'
arch=('i686' 'x86_64')
options=('!libtool')
url='http://www.apache.org/dist/httpd'
license=('APACHE')
backup=(etc/httpd/conf/httpd.conf
        etc/httpd/conf/extra/httpd-{autoindex,dav,default,info,languages}.conf
        etc/httpd/conf/extra/httpd-{manual,mpm,multilang-errordoc}.conf
        etc/httpd/conf/extra/httpd-{ssl,userdir,vhosts}.conf
        etc/logrotate.d/httpd)
depends=('openssl' 'zlib' 'apr-util' 'pcre')
optdepends=('lynx: apachectl status')
provides=('apache')
conflicts=('apache')
install=apache.install
source=(http://www.apache.org/dist/httpd/httpd-${pkgver}.tar.bz2
        http://www.apache.org/dist/httpd/httpd-${pkgver}.tar.bz2.asc
        http://mpm-itk.sesse.net/mpm-itk-2.4.4-03.tar.gz
        mpm-itk-2.4.4-03.patch
        apache.tmpfiles.conf
        httpd.logrotate
        httpd
        httpd.service
        arch.layout)
md5sums=('0e712ee2119cd798c8ae39d5f11a9206'
         '0e652e521f4f4f0f206e72a7b3c92dad'
         '1cece5cf68b2b2012dd9032aa7876fe6'
         'b1fc05d63f787b7e5a25880f248e397d'
         '82068753dab92fe86312b1010a2904d7'
         '6382331e9700ed9e8cc78ea51887b537'
         'c7e300a287ef7e2e066ac7639536f87e'
         'ed219c8228d4d5ae9b7a55107cf79706'
         '3d659d41276ba3bfcb20c231eb254e0c')

build() {

	cd "${srcdir}"
    patch -Np0 -i mpm-itk-2.4.4-03.patch

	cd "${srcdir}/httpd-${pkgver}"

	patch -Np1 -i "${srcdir}/mpm-itk-2.4.4-03/patches/r1368121-post-perdir-config-hook.diff"
	patch -Np1 -i "${srcdir}/mpm-itk-2.4.4-03/patches/r1388447-dirwalk-stat-hook.diff"
	patch -Np1 -i "${srcdir}/mpm-itk-2.4.4-03/patches/r1389339-pre-htaccess-hook.diff"

	# set default user
	sed -e 's#User daemon#User http#' \
	    -e 's#Group daemon#Group http#' \
	    -i docs/conf/httpd.conf.in

	cat "${srcdir}/arch.layout" >> config.layout
	
	./configure --enable-layout=Arch \
		--enable-mpms-shared=all \
		--enable-modules=all \
		--enable-mods-shared=all \
		--enable-so \
		--enable-suexec \
		--with-suexec-caller=http \
		--with-suexec-docroot=/srv/http \
		--with-suexec-logfile=/var/log/httpd/suexec.log \
		--with-suexec-bin=/usr/sbin/suexec \
		--with-suexec-uidmin=99 --with-suexec-gidmin=99 \
		--enable-ldap --enable-authnz-ldap \
		--enable-cache --enable-disk-cache --enable-mem-cache --enable-file-cache \
		--enable-ssl --with-ssl \
		--enable-deflate --enable-cgid \
		--enable-proxy --enable-proxy-connect \
		--enable-proxy-http --enable-proxy-ftp \
		--enable-dbd \
		--with-apr=/usr/bin/apr-1-config \
		--with-apr-util=/usr/bin/apu-1-config \
		--with-pcre=/usr \
		 

    make DESTDIR="${pkgdir}" install
    
	install -m755 httpd "${pkgdir}/usr/sbin/httpd"

	install -D -m755 "${srcdir}/httpd" "${pkgdir}/etc/rc.d/httpd"
	install -D -m644 "${srcdir}/httpd.logrotate" "${pkgdir}/etc/logrotate.d/httpd"
	install -D -m644 "${srcdir}/apache.tmpfiles.conf" "${pkgdir}/usr/lib/tmpfiles.d/apache.conf"

	# symlinks for /etc/httpd
	ln -fs /var/log/httpd "${pkgdir}/etc/httpd/logs"
	ln -fs /run/httpd "${pkgdir}/etc/httpd/run"
	ln -fs /usr/lib/httpd/modules "${pkgdir}/etc/httpd/modules"
	ln -fs /usr/lib/httpd/build "${pkgdir}/etc/httpd/build"

	# set sane defaults
	sed -e 's#/usr/lib/httpd/modules/#modules/#' \
	    -e 's|#\(LoadModule negotiation_module \)|\1|' \
	    -e 's|#\(LoadModule include_module \)|\1|' \
	    -e 's|#\(LoadModule userdir_module \)|\1|' \
	    -e 's|#\(LoadModule slotmem_shm_module \)|\1|' \
	    -e 's|#\(Include conf/extra/httpd-multilang-errordoc.conf\)|\1|' \
	    -e 's|#\(Include conf/extra/httpd-autoindex.conf\)|\1|' \
	    -e 's|#\(Include conf/extra/httpd-languages.conf\)|\1|' \
	    -e 's|#\(Include conf/extra/httpd-userdir.conf\)|\1|' \
	    -e 's|#\(Include conf/extra/httpd-default.conf\)|\1|' \
	    -e 's|#\(Include conf/extra/httpd-mpm.conf\)|\1|' \
	    -i "${pkgdir}/etc/httpd/conf/httpd.conf"

	# cleanup
	rm -rf "${pkgdir}/usr/share/httpd/manual"
	rm -rf "${pkgdir}/etc/httpd/conf/original"
	rm -rf "${pkgdir}/srv/"
	rm -rf "${pkgdir}/var/run"


        install -m755 -d "${pkgdir}/usr/lib/systemd/system"
        install -m644 "${srcdir}/httpd.service" "${pkgdir}/usr/lib/systemd/system/"
}
