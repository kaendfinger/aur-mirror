#!/bin/bash

. /etc/rc.conf
. /etc/rc.d/functions
. /etc/conf.d/mylar

SB_PIDFILE="/run/mylar/mylar.pid"

case "$1" in
  start)
    stat_busy "Starting Mylar"

    if [ -f /run/daemons/mylar ]; then
      echo "Mylar is already running as a daemon! If you are certain it is not, remove /run/daemons/mylar."
      stat_fail
    elif [ -f $ML_PIDFILE ]; then
      echo "Mylar may already be running. If you are certain it is not, remove $ML_PIDFILE."
      stat_fail
    else
      SB_ARGS+=" --pidfile $ML_PIDFILE"
      if [ ! "$SB_USER" ]; then
        /usr/bin/env python2 /opt/mylar/Mylar.py $ML_ARGS
        RC=$?
      else
        su - $ML_USER -s /bin/sh -c "/usr/bin/env python2 /opt/mylar/Mylar.py $ML_ARGS"
        RC=$?
      fi

      if [ $RC -gt 0 ]; then
        stat_fail
      else
        add_daemon mylar
        stat_done
      fi
    fi
    ;;
  stop)
    stat_busy "Stopping Mylar"

    if [ ! -f $ML_PIDFILE ]; then
      echo "The pid file is missing. Check that Mylar is actually running."
      RC=1
    else
      read -r SB_PID < $ML_PIDFILE
      kill $ML_PID
      RC=$?
    fi

    if [ $RC -gt 0 ]; then
      echo "The shutdown failed. Check that Mylar is actually running."
      stat_fail
    else
      while [ -f $ML_PIDFILE ]; do
        sleep 1
      done

      rm_daemon mylar
      stat_done
    fi
    ;;
  restart)
    "$0" stop
    sleep 1
    "$0" start
    ;;
  status)
    stat_busy "Mylar daemon status:";
    ck_status $daemon_name
    ;;

  *)
    echo "usage: $0 {start|stop|restart|status}"
esac
exit 0

