# Maintainer: Andre Bartke <dev@bartke.cc>
# Contributor: Vojtech Horky

_pkgname=binutils
_target="sparc64-unknown-elf"
_sysroot="/usr/lib/cross-${_target}"
 
pkgname=${_target}-${_pkgname}
pkgver=2.23.1
pkgrel=1
pkgdesc="Bare-metal binutils for sparc targets"
arch=('i686' 'x86_64')
url="http://www.gnu.org/software/binutils/"
license=('GPL')
depends=('zlib')
options=('!libtool' '!distcc' '!ccache' '!buildflags')
source=("ftp://ftp.gnu.org/gnu/binutils/${_pkgname}-${pkgver}.tar.bz2")
 
build() {
  cd ${srcdir}/${_pkgname}-${pkgver}
  rm -rf build
  mkdir build && cd build

  ../configure --prefix=${_sysroot} \
    --with-sysroot=${_sysroot} \
    --target=${_target} \
    --enable-64-bit-bfd \
    --disable-shared \
    --disable-nls \
    --with-gnu-as \
    --with-gnu-ld \
    --disable-multilib

  make configure-host || return 1
  make || return 1
}
 
package() {
  cd ${srcdir}/${_pkgname}-${pkgver}/build
  make DESTDIR=${pkgdir} install || return 1

  msg "Cleaning-up cross compiler tree..."
  rm -rf ${pkgdir}/${_sysroot}/share/{info,man}
  rm ${pkgdir}/${_sysroot}/${_target}/bin/*

  msg "Creating out-of-path executables..."
  mkdir -p ${pkgdir}/${_sysroot}/${_target}/bin/
  cd ${pkgdir}${_sysroot}/${_target}/bin/
  for bin in ${pkgdir}${_sysroot}/bin/${_target}-*; do
    bbin=`basename "$bin"`;
    ln -s ${_sysroot}/bin/$bbin `echo "$bbin" | sed "s#^${_target}-##"`;
  done

  msg "Creating /usr/bin symlinks..."
  mkdir -p $pkgdir/usr/bin
  for bin in ${pkgdir}${_sysroot}/bin/${_target}-*; do
    bbin=`basename "$bin"`;
    ln -s "${_sysroot}/bin/${bbin}" "${pkgdir}/usr/bin/${bbin}";
  done

  # avoid some sysroot issues
  install -dm755 ${pkgdir}/${_sysroot}/include
  install -dm755 ${pkgdir}/${_sysroot}/usr/include
  install -vm644 ${srcdir}/${_pkgname}-${pkgver}/include/libiberty.h ${pkgdir}${_sysroot}/include/
}

md5sums=('33adb18c3048d057ac58d07a3f1adb38')

# vim: set ts=2 sw=2 ft=sh noet:
