pkgname=yabause-qt-svn
pkgver=3095
pkgrel=1
pkgdesc="A Sega Saturn emulator with QT interface. (svn)"
arch=(i686 x86_64)
license=("GPL")
depends=(qt4 sdl freeglut)
makedepends=(subversion cmake)
url="http://yabause.org"
conflicts=(yabause yabause-qt yabause-svn)
provides=(yabause-qt)
_svntrunk="https://yabause.svn.sourceforge.net/svnroot/yabause/trunk"
_svnmod="yabause"

_optimal_make_jobs() {
  if [ -r /proc/cpuinfo ]; then
    local core_count=$(grep -Fc processor /proc/cpuinfo)
  else
    local core_count=0
  fi
  if [ $core_count -gt 1 ]; then
    echo -n $[$core_count-1]
  else
    echo -n 1
  fi
}

build() {
  cd "${srcdir}"

  if [ -d "${_svnmod}/.svn" ]; then
    (cd "$_svnmod" && svn up -r $pkgver)
  else
    svn co "$_svntrunk" --config-dir ./ -r $pkgver $_svnmod
  fi

  msg 'SVN checkout done or server timeout'

  rm -rf "${_svnmod}-build"
  cp -r "$_svnmod" "${_svnmod}-build"
  cd "${_svnmod}-build/yabause"

  sed -i "s/\(#define SETTINGS_H\)/\1\n#include \"QtYabause.h\"/" src/qt/Settings.h
  sed -i "s/\(#define QTYABAUSE_H\)/\1\n#define PACKAGE \"${pkgname%-qt}\"/" src/qt/QtYabause.h
  sed -i "s/\(#define QTYABAUSE_H\)/\1\n#define VERSION \"${pkgver}\"/" src/qt/QtYabause.h
  cmake \
    -DCMAKE_BUILD_TYPE=Release \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DYAB_PORTS=qt \
    -DYAB_MULTIBUILD=OFF \
    -DYAB_NETWORK=ON \
    -DYAB_OPTIMIZED_DMA=on \
    -DYAB_PERKEYNAME=ON
  make -j$(_optimal_make_jobs)
}

package() {
  cd "${srcdir}/${_svnmod}-build/yabause"
  make DESTDIR="${pkgdir}" install
  install -Dm644 COPYING "$pkgdir/usr/share/licenses/$pkgname/COPYING"
}
