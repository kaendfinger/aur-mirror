# Maintainer: Splex

pkgname=inkscape-bzr
pkgver=12072
pkgrel=1
pkgdesc="An Open Source vector graphics editor, with capabilities similar to Illustrator, CorelDraw, or Xara X, using the W3C standard Scalable Vector Graphics (SVG) file format."
url="https://launchpad.net/inkscape"
arch=('i686' 'x86_64')
license=('GPL' 'LGPL')
depends=('gc' 'gtkmm' 'poppler-glib' 'pyxml' 'libxslt' 'gsl' 'popt' 'python2'
         'gtkspell' 'imagemagick' 'desktop-file-utils' 'hicolor-icon-theme')
optdepends=('python2-numpy: some extensions'
            'python-lxml: some extensions and filters'
            'uniconvertor: reading/writing to some proprietary formats')
makedepends=('boost' 'pkg-config' 'intltool' 'bzr' 'gettext' 'autoconf')
provides=('inkscape')
conflicts=('inkscape')
options=('!libtool')
source=('https://launchpadlibrarian.net/127104104/autogen.sh-configure.ac-update-r11998-v1.diff')
md5sums=('22beb972ac4f5531c0e1b871bdd3986e')
install=inkscape-bzr.install

_bzrtrunk="lp:inkscape"
_bzrmod="inkscape"

build() {
  cd "$srcdir"
  msg "Connecting to Bazaar server...."

  if [ -d $_bzrmod ] ; then
    cd ${_bzrmod} && bzr pull ${_bzrtrunk} -r ${pkgver}
    msg "The local files are updated."
  else
    bzr branch ${_bzrtrunk} ${_bzrmod} -r ${pkgver}
  fi

  msg "Bazaar checkout done or server timeout"
  msg "Starting make..."

  rm -rf "$srcdir/$_bzrmod-build"
  cp -r "$srcdir/$_bzrmod" "$srcdir/$_bzrmod-build"
  cd "$srcdir/$_bzrmod-build"

  patch -p0 -i "$srcdir/autogen.sh-configure.ac-update-r11998-v1.diff"

  ./autogen.sh
  ./configure --prefix=/usr \
    --with-python \
    --with-perl \
    --without-gnome-vfs \
    --enable-lcms \
    --enable-poppler-cairo \
    --disable-dependency-tracking

  # fix for inkscape to use python2 with the python 3 package installed.
  sed -i 's|/usr/bin/python\>|/usr/bin/python2|g' cxxtest/*.py
  sed -i 's|/usr/bin/env python\>|/usr/bin/env python2|g' share/*/{test/,}*.py
  sed -i 's|"python" },|"python2" },|g' src/extension/implementation/script.cpp
  sed -i 's|python -c |python2 -c|g' configure share/extensions/uniconv*.py
  sed -i 's|"python"|"python2"|g' src/main.cpp

  make
}

package() {
  cd "$srcdir/$_bzrmod-build"
  make DESTDIR=$pkgdir install
}

# vim:set ts=2 sw=2 et:
