# Maintainer: Jason St. John <jstjohn .. purdue . edu>
# Contributor: Mikhail Kulemin <mihkulemin@gmail.com>
# Contributor: Michael Pusterhofer <pusterhofer (at) student (dot) tugraz (dot) at>

pkgname=opennebula
_unstable_pkg=opennebula-unstable
pkgver=3.8.3
pkgrel=2
pkgdesc="Virtual management infrastructure as a service (IaaS) toolkit for cloud computing (NOTE: Read the PKGBUILD!)"
arch=('i686' 'x86_64')
url="http://opennebula.org/software:rnotes:rn-rel3.8"
license=('Apache')
depends=('ruby>=1.8.7' 'xmlrpc-c-abyss>=1.06' 'openssl>=0.9.8' 'sqlite3>=3.6' 'openssh' 'libxml2>=2.7'
         'curl' 'libxslt' 'expat' 'cdrkit')
makedepends=('xmlrpc-c-abyss>=1.06' 'pkgconfig' 'scons>=0.98')
optdepends=('nfs-utils: for using the shared file system storage model'
            'mysql>=5.1: optional replacement for SQLite as the DB back-end'
            'libmysqlclient>=5.1: required if using MySQL instead of SQLite')
conflicts=('opennebula-unstable')
install=opennebula.install
changelog=ChangeLog
source=("http://dev.opennebula.org/packages/$pkgname-$pkgver/$pkgname-$pkgver.tar.gz"
        'opennebula.service'
        'chown_fix.patch')
sha512sums=('dec982800373dc0901990a9f1b9e9693cd7b0f6c9a4c484d3cf6046f635958898297cb789a5c8bce303cbf434449f7c02112bb1aef356d10aa064dd890ecfeb4'
            '00db19d355a789fb27320d16917804354fc824c99a851e755a58943824377f87f158e0e560773957acfc3f959a2937462391271d309aff4e6aabb50cb964009d'
            '2098ebe54a5227d803f2a7bb6b0d782750770469984839d6a01ec92d62d2165a4a1be7c2566856b4a59536062d8d64d1c0e4ab5f2307f50ed4e7d7f8710b739b')

build() {
	cd "$srcdir/$pkgname-$pkgver"

	##########################################################################
	##                                                                      ##
	## It is highly recommended that you read the documentation and tweak   ##
	##     the PKGBUILD accordingly:                                        ##
	## http://opennebula.org/documentation:rel3.8:compile                   ##
	##                                                                      ##
	## This package assumes a self-contained install. If you do NOT want a  ##
	##     self-contained install, then remove `-d /srv/cloud/one` from the ##
	##     package() function and MAKE SURE you properly change the         ##
	##     appropriate sections of opennebula.install                       ##
	##                                                                      ##
	##########################################################################

	# This builds the vanilla OpenNebula package. Tweak this line as desired.
	scons
}

package() {
	cd "$srcdir/$pkgname-$pkgver"

	install -D -m644 "$srcdir"/opennebula.service "$pkgdir"/usr/lib/systemd/system/opennebula.service

	# Patch upstream install script to not attempt to chown the install
	# directories because `makepkg` will otherwise fail on a fresh installation.
	# We do our own chown in post_install().
	patch < "$srcdir/chown_fix.patch"

	# This checks to see whether OpenNebula is currently installed. To avoid
	# a potentially scary message, errors are sent to /dev/null
	if [[ ("$(pacman -Qq ${pkgname} 2>/dev/null)" == "${pkgname}") || ("$(pacman -Qq ${_unstable_pkg} 2>/dev/null)" == "${_unstable_pkg}") ]]; then
		# Use -k when running ./install.sh to keep previous configuration files
		DESTDIR="$pkgdir/" ./install.sh -k -u oneadmin -g cloud -d /srv/cloud/one
	else
		# Do not use -k when running ./install.sh for new installations
		DESTDIR="$pkgdir/" ./install.sh -u oneadmin -g cloud -d /srv/cloud/one
	fi
}
