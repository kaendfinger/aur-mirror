# Maintainer: Josh VanderLinden <arch@cloudlery.com>
pkgname=vnstatui
pkgver=20130121
pkgrel=1
pkgdesc="Basic web UI that displays traffic graphs from vnstat"
arch=('any')
url="http://bitbucket.org/instarch/vnstat-ui"
license=('BSD')
depends=('vnstat' 'python' 'python-bottle' 'python-distribute')
makedepends=('git' 'python-docutils')
optdepends=(
  'nginx: fast server to proxy requests to vnstatui'
  'python-docutils: to generate a page for the license'
)
backup=(
  'etc/conf.d/vnstatui.conf'
  'etc/nginx/sites/vnstatui.conf'
)
source=(
  'vnstatui.service'
  'vnstatui.conf'
  'vnstatui.nginx'
)
md5sums=('43edd686d2ad7d8465dba09385132853'
         '669c2032a8df2de01f77dafb6363db49'
         '84abf6ddf4924cc57f56c7cc9f859c16')

_gitroot=http://bitbucket.org/instarch/vnstat-ui
_gitname=vnstatui-git

build() {
  cd "$srcdir"
  msg "Connecting to GIT server...."

  if [[ -d "$_gitname" ]]; then
    cd "$_gitname" && git pull origin
    msg "The local files are updated."
  else
    git clone "$_gitroot" "$_gitname"
  fi

  msg "GIT checkout done or server timeout"
  msg "Starting build..."

  rm -rf "$srcdir/$_gitname-build"
  git clone "$srcdir/$_gitname" "$srcdir/$_gitname-build"
  cd "$srcdir/$_gitname-build"
}

package() {
  cd "$srcdir/$_gitname-build"

  python3 setup.py install --optimize=1 --root="${pkgdir}/"
  install -D vnstatui/index.tpl "${pkgdir}/usr/lib/python3.3/site-packages/vnstatui/index.tpl"

  install -D vnstatui.conf "${pkgdir}/etc/conf.d/vnstatui.conf"
  install -D vnstatui.nginx "${pkgdir}/etc/nginx/sites/vnstatui.conf"
  install -D vnstatui.service "${pkgdir}/usr/lib/systemd/system/vnstatui.service"
  install -D LICENSE "${pkgdir}/usr/share/licenses/${pkgname}/LICENSE"

  rst2man README.rst | gzip > vnstatui.1.gz
  install -D vnstatui.1.gz "${pkgdir}/usr/share/man/man1/vnstatui.1.gz"

  rst2html LICENSE > "${pkgdir}/usr/lib/python3.3/site-packages/vnstatui/license.html"
}

# vim:set ts=2 sw=2 et:
