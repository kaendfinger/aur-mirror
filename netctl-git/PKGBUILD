# Maintainer: Daniel Wallace <danielwallace at gtmanfred dot com>
# Contributor: Jouke Witteveen <j.witteveen@gmail.com>

pkgname=netctl-git
_pkgname=netctl
pkgver=20130202
pkgrel=1
pkgdesc="Network configuration and profile scripts"
url="http://projects.archlinux.org/netctl.git"
license=("GPL")
depends=("coreutils" "iproute2" "openresolv")
makedepends=('asciidoc')  # The source tarball includes pre-built documentation.
optdepends=('dialog: for the menu based wifi assistant'
            'dhclient: for DHCP support (or dhcpcd)'
            'dhcpcd: for DHCP support (or dhclient)'
            'wpa_supplicant: for wireless networking support'
            'ifplugd: for automatic wired connections through netctl-ifplugd'
            'wpa_actiond: for automatic wireless connections through netctl-auto'
            'ifenslave: for bond connections'
            'bridge-utils: for bridge connections'
           )
conflicts=("netcfg")
arch=(any)

_gitroot="git://projects.archlinux.org/netctl.git"
_gitname="$_pkgname"

build() {
  cd "$srcdir"
  msg "Connecting to GIT server...."

  if [[ -d "$_gitname" ]]; then
    cd "$_gitname" && git pull origin
    msg "The local files are updated."
  else
    git clone "$_gitroot" "$_gitname"
  fi

  msg "GIT checkout done or server timeout"
  msg "Starting build..."

  rm -rf "$srcdir/$_gitname-build"
  git clone "$srcdir/$_gitname" "$srcdir/$_gitname-build"
  cd "$srcdir/$_gitname-build"
  make docs 
}


package() {
  cd "$srcdir/$_gitname-build"
  make DESTDIR="$pkgdir" install

  # Shell Completion
  install -D -m644 contrib/bash-completion "$pkgdir/usr/share/bash-completion/completions/netctl"
  install -D -m644 contrib/zsh-completion "$pkgdir/usr/share/zsh/site-functions/_netctl"
}

# vim:set ft=sh ts=2 sw=2 et:
