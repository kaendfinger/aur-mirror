# $Id$
# Maintainer: Jonas Heinrich <onny@project-insanity.org>

pkgname=webkitgtk2-svn
pkgver=2O130113
pkgrel=3
pkgdesc="GTK+ Web content engine library for GTK+ 2.0"
arch=('i686' 'x86_64')
url="http://webkitgtk.org/"
license=('custom')
depends=('libwebp' 'libxt' 'libxslt' 'sqlite' 'libsoup' 'enchant' 'libgl' 'geoclue' 'gtk2' 'gstreamer0.10-base')
makedepends=('subversion' 'libxt' 'libxslt' 'sqlite' 'libsoup' 'enchant' 'libgl' 'geoclue' 'gtk2' 'gtk3' 'gst-plugins-base-libs' 'gstreamer0.10-base' 'gperf' 'gobject-introspection' 'python2' 'mesa' 'ruby' 'gtk-doc' 'libsecret' 'python' 'pango')
conflicts=('libwebkit')
provides=("libwebkit=${pkgver}")
replaces=('libwebkit' 'webkitgtk2')
options=('!libtool' '!emptydirs')

build() {
  cd "$srcdir"
  svn co http://svn.webkit.org/repository/webkit/trunk
  cd "$srcdir/trunk"
  mkdir build-gtk2

  cd build-gtk2
  ../autogen.sh
  ../configure --prefix=/usr \
    --enable-introspection \
    --disable-silent-rules \
    --libexecdir=/usr/lib/webkitgtk2 \
    --with-gstreamer=0.10 \
    --with-gtk=2.0 \
    --disable-webkit2

  # patching Makefile to be compatible with archlinux python-setup
  sed -i '11975iPYTHON2 = /usr/bin/python2' GNUmakefile
  sed -i 's|$(PYTHON) $(WebCore)/inspector/generate-inspector-protocol-version|$(PYTHON2) $(WebCore)/inspector/generate-inspector-protocol-version|' GNUmakefile
  sed -i 's|$(PYTHON) $(srcdir)/Tools/gtk/generate-gtkdoc|$(PYTHON2) $(srcdir)/Tools/gtk/generate-gtkdoc|' GNUmakefile

  make -j4 all stamp-po
}

package() {
  cd "$srcdir/trunk/build-gtk2"
  make -j1 DESTDIR="$pkgdir" install
  install -Dm644 ../Source/WebKit/LICENSE "$pkgdir/usr/share/licenses/${pkgname}/LICENSE"
}
