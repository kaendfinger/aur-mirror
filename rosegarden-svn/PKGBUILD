# Maintainer: Limao Luo <luolimao+AUR@gmail.com>
# Contributor: Asa Marco <marcoasa90@gmail.com>

_pkgname=rosegarden
pkgname=$_pkgname-svn
pkgver=13222
pkgrel=1
pkgdesc="Audio and MIDI sequencer, score editor - development branch, doesn't conflict with the old version"
arch=(i686 x86_64)
url=http://www.rosegardenmusic.com/
license=(GPL2)
depends=(dssi fftw liblrdf lirc-utils qt4 shared-mime-info)
makedepends=(imake subversion)
optdepends=('lilypond: notation display'
    'cups: printing support'
    flac
    'okular: print preview, or any other PDF viewer'
    'timidity++: MIDI playback, or any other softsynth'
    wavpack)
[[ $CARCH == "i686" ]] && optdepends+=('dssi-vst: win32 VST support')
install=$_pkgname.install

_svntrunk=svn://svn.code.sf.net/p/$_pkgname/code/trunk/$_pkgname
_svnmod=$_pkgname

build() {
    cd "$pkgdir"
    install -d usr/bin
    install -d usr/share/{applications,pixmaps,$_name/{chords,examples,fonts,library,presets,styles}}

    cd "$srcdir"
    msg "Starting SVN checkout..."
    if [[ -d $_svnmod/.svn ]]; then
        pushd $_svnmod && svn up -r $pkgver
        msg2 "The local files have been updated."
        popd
    else
        svn co $_svntrunk --config-dir ./ -r $pkgver $_svnmod
    fi
    msg2 "SVN checkout done or server timeout"

    rm -rf $_svnmod-build/
    cp -r $_svnmod/ $_svnmod-build/
    cd $_svnmod-build/

    msg "Compiling..."
    autoreconf -fi
    ./configure --with-qtdir=/usr --prefix=/usr
    make
    sed -ri 's:(Exec|Name|Icon)=rosegarden:&-qt4:g' data/desktop/rosegarden.desktop
}

package() {
    cd "$srcdir"/$_svnmod-build/
    make DESTDIR="$pkgdir" install
}
