# Contributor: skydrome <skydrome@i2pmail.org>
# Maintainer: skydrome <skydrome@i2pmail.org>

pkgname=tor-git
pkgver=20130303
pkgrel=1
pkgdesc="An anonymizing overlay network (development version)"
arch=('i686' 'x86_64')
url="http://www.torproject.org"
license=('BSD')
depends=('openssl' 'libevent' 'bash')
makedepends=('ca-certificates' 'asciidoc')
optdepends=('torsocks-git: for torify support')
# this is an optdep because the torsocks in community requires tor which is wrong
# and the tor in extra installs tsocks which support for has been removed since last year
# use torsocks-git instead
conflicts=('tor' 'tor-unstable')
provides=('tor')
install='tor.install'
backup=('etc/tor/torrc')
source=('torrc'
        'tor.service')
sha256sums=('acde77cf0f4163481e6a1e26ef496b1451eb3d40554cd57b3a09cdce346d86b4'
            '50926c7c0410c04baa13515a6bb7af7ec148911b4be87efa35e5a96c8da59e8d')

_gitroot="git://git.torproject.org/tor.git"
_gitname="$pkgname"

build() {
    cd "$srcdir"

    msg "Connecting to GIT server..."
    if [ -d "$_gitname/.git" ]; then
        cd "$_gitname"
        #git clean -xdf
        git pull --depth 1 origin
        msg "The local repository was updated"
    else
        git clone --depth 1 ${_gitroot} ${_gitname}
        cd "$_gitname"
        msg "The remote repository was cloned"
    fi

    msg "Starting build..."
    ./autogen.sh
    ./configure --prefix=/usr --sysconfdir=/etc --localstatedir=/var
    make
}

check() {
    cd "$_gitname"
    make test || true
}

package() {
    cd "$srcdir/$_gitname"

    make DESTDIR="$pkgdir" install

    install -dm0700 "${pkgdir}/var/lib/tor"
    rm -f "${pkgdir}/etc/tor/tor-tsocks.conf"
    mv "${pkgdir}/etc/tor/torrc.sample"     "${pkgdir}/etc/tor/torrc-dist"
    install -Dm0644 "${srcdir}/torrc"       "${pkgdir}/etc/tor/torrc"
    install -Dm0644 "${srcdir}/tor.service" "${pkgdir}/usr/lib/systemd/system/tor.service"
    install -Dm0644 LICENSE "${pkgdir}/usr/share/licenses/tor/LICENSE"
}
