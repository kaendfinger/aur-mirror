# Maintainer: superlex

# Based on firefox-extension-globalmenu 
# Contributor: Tom Kuther <gimpel@sonnenkinder.org> 

pkgname=seamonkey-extension-globalmenu
pkgver=20130221
_seaver=2.16
_xulver=19.0
pkgrel=1
pkgdesc="Global dbusmenu/appmenu extension for Seamonkey (unofficial)"
url="https://launchpad.net/globalmenu-extension"
arch=('i686' 'x86_64')
license=('LGPL')
depends=('libdbusmenu' 'libdbusmenu-gtk2' "seamonkey>=${_seaver}")
makedepends=('bzr' 'autoconf2.13' 'python2' 'yasm' 'zip' 'unzip' "xulrunner>=${_xulver}" 'libidl2')
options=()
conflicts=()
replaces=()
install=
source=('seamonkey_compat.patch'
        'disable_webgl.patch')
md5sums=('69533d3d8a08e626cb69b72d6d3ec161'
         '630639b4d8e3f99fe48428249452b8f0')

# _bzrroot=lp:globalmenu-extension
_bzrroot=lp:globalmenu-extension/3.7
_bzrname=globalmenu-extension

build() {
	cd "$srcdir"
	msg "Connecting to BZR server...."

	if [ -d $_bzrname ] ; then
		cd $_bzrname && bzr update
		msg "The local files are updated."
	else
		bzr branch $_bzrroot $_bzrname
	fi
	msg "BZR checkout done"
	
	msg "Starting make..."
	rm -rf "$srcdir/${_bzrname}-build"
	cp -r "$srcdir/${_bzrname}" "$srcdir/${_bzrname}-build"
	cd "$srcdir/${_bzrname}-build"
        
        # Add seamonkey section to install.rdf 
	patch -Np1 -i "${srcdir}/seamonkey_compat.patch"
	
        # Disable mesa dep
	patch -p0 < "${srcdir}/disable_webgl.patch"
	
	autoconf-2.13
	sed -i 's/^ \t/\t/g' config/rules.mk
	sed -i '/^XPIDL_COMPILE[[:space:]]*=/s@\$(LIBXUL_DIST)/@&sdk/@' config/config.mk

	./configure --with-libxul-sdk=`pkg-config --variable=sdkdir libxul` \
		--with-system-libxul \
		--with-system-nspr \
		--with-system-nss \
		--enable-application=extensions \
		--enable-extensions=globalmenu \
		--disable-tests \
		--disable-necko-wifi \
		--disable-webrtc
	make || return 1
}

package() {
	cd "$srcdir/${_bzrname}-build"
	emid=$(sed -n '/.*<em:id>\(.*\)<\/em:id>.*/{s//\1/p;q}' dist/xpi-stage/globalmenu/install.rdf)
	install -d "${pkgdir}/usr/lib/seamonkey-$_seaver/extensions/${emid}"
	unzip -d "${pkgdir}/usr/lib/seamonkey-$_seaver/extensions/${emid}" dist/xpi-stage/globalmenu.xpi
}

# vim:syntax=sh
