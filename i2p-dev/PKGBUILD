# Maintainer: skydrome <skydrome@i2pmail.org>
# Contributor: skydrome <skydrome@i2pmail.org>

########[ OPTIONS ]########################################
# Comment out if you want to build all language translations
export LG2=en

## Clone i2p from github
#_gitroot="git://github.com/i2p/i2p.i2p.git"
_gitroot="git://github.com/skydrome/i2p.i2p.git"

## Clone i2p through i2p
# Add and Start a new Standard client tunnel with the following settings:
#   Name: pull.git.repo.i2p
#   Port: 9450  or some other unused port
#   Tunnel Destination: 3so7htzxzz6h46qvjm3fbd735zl3lrblerlj2xxybhobublcv67q.b32.i2p
#_gitroot="git://127.0.0.1:9450/i2p.i2p.git"
###########################################################

pkgname=i2p-dev
pkgver=0.9.4
pkgrel=12
_wrapper_ver=3.5.17
pkgdesc="A distributed anonymous network (daily mtn->git sync)"
url="http://www.i2p2.de"
license=('GPL2')
arch=('x86_64' 'i686' 'armv6h')
depends=('java-environment' 'gmp')
makedepends=('apache-ant' 'git')
optdepends=('gtk2: for rrd graphs')
conflicts=('i2p' 'i2p-bin' 'i2p-portable' 'i2p-portable-source')
provides=('i2p')
backup=('opt/i2p/wrapper.config')
install='i2p.install'
source=("https://wrapper.tanukisoftware.com/download/${_wrapper_ver}/wrapper_${_wrapper_ver}_src.tar.gz"
        'i2prouter.service' 'i2prouter.sh')

sha256sums=('e4432fdef6cfc1d19d8571429c8554748ec21ffa9fecabd464f5f12ec5160f9b'
            'bfa4de5936b0a0ff08c20307b0739ab6a65bee325e82fe22662ccf2062f35666'
            '25b227bc33a4b76e022d4ab40cd1eb1037f1439000d96258de90eda506ecb620')

build_jbigi() {
    msg "Building libjbigi..."
    cd ${srcdir}/i2p.i2p/core/c/jbigi
    CFLAGS+=" -fPIC -Wall"
    INCLUDES="-I./jbigi/include -I${JAVA_HOME}/include -I${JAVA_HOME}/include/linux"
    LDFLAGS="-shared -Wl,-O1,--sort-common,-z,relro,-soname,libjbigi.so"
    echo "gcc -c ${CFLAGS} ${INCLUDES} jbigi/src/jbigi.c"
    gcc -c ${CFLAGS} ${INCLUDES} jbigi/src/jbigi.c
    echo "gcc ${LDFLAGS} ${INCLUDES} -o libjbigi.so jbigi.o -lgmp"
    gcc ${LDFLAGS} ${INCLUDES} -o libjbigi.so jbigi.o -lgmp
    install -Dm644 libjbigi.so "${srcdir}/i2p.i2p/pkg-temp/lib/libjbigi.so"
}

build_jcpuid() {
if [[ "$CARCH" != @(arm)* ]]; then
    msg "Building libjcpuid..."
    cd ${srcdir}/i2p.i2p/core/c/jcpuid
    INCLUDES="-I./include -I${JAVA_HOME}/include -I${JAVA_HOME}/include/linux"
    LDFLAGS="-shared -Wl,-O1,--sort-common,-z,relro,-soname,libjcpuid-${CARCH}-linux.so"
    echo "gcc ${CFLAGS} ${LDFLAGS} ${INCLUDES} src/jcpuid.c -o libjcpuid-${CARCH}-linux.so"
    gcc ${CFLAGS} ${LDFLAGS} ${INCLUDES} src/jcpuid.c -o libjcpuid-${CARCH}-linux.so
    install -Dm644 libjcpuid-${CARCH}-linux.so "${srcdir}/i2p.i2p/pkg-temp/lib/libjcpuid.so"
fi
}

build_wrapper() {
    msg "Building java wrapper..."
    cd "${srcdir}/wrapper_${_wrapper_ver}_src"
    sed 's:value="1.4":value="1.6":' -i build.xml
    [[ "$CARCH" = "x86_64" ]] && _arch=64 || _arch=32
    ./build${_arch}.sh
}

build() {
    cd "$srcdir"
    if [[ -d "i2p.i2p/.git" ]]; then
        msg "Cleaning repo..."
        cd "i2p.i2p"
        git clean -xdf 2&>/dev/null
        git pull --depth 1
        msg "The local repository was updated."
    else
        msg "Connecting to ${_gitroot} ..."
        git clone --depth 1 ${_gitroot}
        cd "i2p.i2p"
        msg "The remote repository was cloned."
    fi

    source /etc/profile.d/apache-ant.sh
    source /etc/profile.d/jdk.sh 2>/dev/null || source /etc/profile.d/openjdk6.sh

    sed -i router/java/build.xml core/java/build.xml \
        -e 's:target="1.5":target="1.6":'

    ant preppkg-linux
    build_jbigi
    build_jcpuid
    build_wrapper
}

package() {
    cd "$srcdir/i2p.i2p"

    install -dm700   ${pkgdir}/run/i2p
    install -dm755   ${pkgdir}/usr/bin
    install -dm755   ${pkgdir}/opt/i2p

    cp -r pkg-temp/* ${pkgdir}/opt/i2p

    install -Dm755 ${srcdir}/i2prouter.sh         "${pkgdir}/opt/i2p/i2prouter"
    install -Dm644 ${srcdir}/i2prouter.service    "${pkgdir}/usr/lib/systemd/system/i2prouter.service"
    install -Dm644 ${pkgdir}/opt/i2p/man/eepget.1 "${pkgdir}/usr/share/man/man1/eepget.1"
    install -Dm644 ${pkgdir}/opt/i2p/LICENSE.txt  "${pkgdir}/usr/share/licenses/i2p/LICENSE"
    mv ${pkgdir}/opt/i2p/licenses/*                  "${pkgdir}/usr/share/licenses/i2p/"

    cd "$srcdir/wrapper_${_wrapper_ver}_src"
    install -m755 bin/wrapper       "${pkgdir}/opt/i2p/i2psvc"
    install -m644 lib/wrapper.jar   "${pkgdir}/opt/i2p/lib/wrapper.jar"
    install -m644 lib/libwrapper.so "${pkgdir}/opt/i2p/lib/libwrapper.so"

    chmod +x ${pkgdir}/opt/i2p/eepget
    ln -s /opt/i2p/{eepget,i2prouter} ${pkgdir}/usr/bin/

    rm -r ${pkgdir}/opt/i2p/{osid,postinstall.sh,runplain.sh,INSTALL-headless.txt,LICENSE.txt,licenses,man,lib/wrapper}

    sed -i $pkgdir/opt/i2p/{eepget,wrapper.config} \
        -e 's:%INSTALL_PATH:/opt/i2p:g' \
        -e 's:$INSTALL_PATH:/opt/i2p:' \
        -e 's:wrapper.signal.mode.usr1=.*:wrapper.signal.mode.usr1=RESTART:'
    sed -i $pkgdir/opt/i2p/clients.config \
        -e "s:clientApp.4.startOnLoad=.*:clientApp.4.startOnLoad=false:"
    echo 'router.updateDisabled=true' >${pkgdir}/opt/i2p/router.config
}
