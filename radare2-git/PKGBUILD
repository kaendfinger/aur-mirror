pkgname="radare2-git"
pkgver=20130127
pkgrel=1
pkgdesc="Toolchain for reverse engineering"
arch=('i686' 'x86_64')
url="http://radare.org"
license=('GPL3')
depends=('valabind-git' 'python2' 'libewf')
makedepends=('mercurial' 'python2' 'lua')
provides=('radare2' 'r2-bindings')
conflicts=('radare2' 'r2-bindings' 'radare2-hg')
options=('!strip')

_gitroot="git://github.com/radare/radare2.git"
_gitname="radare2"

build() {
	export CFLAGS="${CFLAGS//-fPIE -pie}"
	export CXXFLAGS="${CXXFLAGS//-fPIE -pie}"
	export PKG_CONFIG_PATH="${srcdir}/${_gitname}-build/pkgcfg:$PKG_CONFIG_PATH"

	cd ${srcdir}

	if [ -d ${_gitname}-build ] ; then
		msg "Removing leftovers..."
		rm -rf ${_gitname}-build
	fi

	if [ -d ${_gitname} ]; then
		cd ${_gitname}
		git pull --rebase
	else
		git clone ${_gitroot} ${_gitname}
	fi

	cd ${srcdir}
	git clone ${_gitname} ${_gitname}-build
	cd ${_gitname}-build

	msg "Building radare2..."
	./sys/build.sh

	if [ -e /usr/include/libr/r_core.h ]; then
		msg "Building language bindings..."
		cd r2-bindings
		make clean
		./configure --prefix=/usr --enable=python --enable=lua
		cd python
		make
		cd ..
		cd lua
		make
	fi
}

package() {
	cd ${srcdir}/${_gitname}-build

	make DESTDIR=${pkgdir} install

	if [ -e /usr/include/libr/r_core.h ]; then
		cd r2-bindings
		make DESTDIR=${pkgdir} install-vapi
		make DESTDIR=${pkgdir} PYTHON=python2.7 install-python
		make DESTDIR=${pkgdir} install-lua
	else
		msg "Rebuild the package after installation to get the language bindings"
	fi

	cd ${srcdir}
	rm -rf ${_gitname}-build
}
