# Maintainer: Lukas Jirkovsky <l.jirkovsky@gmail.com>
pkgname=makehuman
pkgver=1.0alpha7
_pkgver=1_0_0_alpha7
pkgrel=1
pkgdesc="Parametrical modeling program for creating human bodies"
arch=('i686' 'x86_64')
url="http://www.makehuman.org/"
depends=('python2' 'libgl' 'sdl' 'sdl_image' 'mesa' 'glew')
optdepends=('aqsis: rendering' 'povray: rendering' 'mitsuba: rendering' 'python2-numpy: warping')
license=('AGPL3')
source=(makehuman.desktop makehuman.sh makehuman.png Makefile.diff)
makedepends=('subversion')
md5sums=('f54fdfbc6c783effc4624808d2547563'
         '31369d91ab9974b6aeff6e0514b6d855'
         '2e7da6fe63519dba19877bd2ecfc9f0c'
         '6ad874111511b74d9e2c9a9708a3e533')

build() {
  cd "$srcdir"
  svn co http://makehuman.googlecode.com/svn/releases/makehuman_$_pkgver ${pkgname}_${_pkgver}_source

  cd "$srcdir"/${pkgname}_${_pkgver}_source

  # make sure there is no patch applied before building
  svn revert -R .
  patch -Np0 < "$srcdir"/Makefile.diff || true

  make -f Makefile.Linux
}

package() {
  cd "$srcdir"/${pkgname}_${_pkgver}_source

  install -d -m755 "$pkgdir"/opt/makehuman/
  cp -a {makehuman,main.py,apps,core,data,docs,plugins,pythonmodules,shared,tools,utils} \
      "$pkgdir"/opt/makehuman/
  # the tools subdirectory contains scripts for blender

  # remove svn files
  find "$pkgdir"/opt/makehuman -type d -name ".svn" -exec rm -rf '{}' '+'
  # fix python location
  find "$pkgdir"/opt/makehuman -type f -name "*.py" -exec sed -i 's|^#!.*|#!/usr/bin/python2|' '{}' '+'

  install -D -m755 "$srcdir"/$pkgname.sh "$pkgdir"/usr/bin/$pkgname
  install -D -m644 "$srcdir"/$pkgname.png "$pkgdir"/usr/share/pixmaps/$pkgname.png
  install -D -m644 "$srcdir"/$pkgname.desktop "$pkgdir"/usr/share/applications/$pkgname.desktop
}
