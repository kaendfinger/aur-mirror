# Maintainer: Marco Schulze <marco.c.schulze-gmail.com>
# Contributor: TuxSpirit<tuxpsiritATarchlinuxDOTfr>
# Contributor: Jan Stępień <jstepien@users.sourceforge.net>

pkgname=zpaq
pkgdesc="Programmable file compressor, library and utilities. Based on the PAQ compression algorithm"
url="http://mattmahoney.net/dc/zpaq.html"
pkgver=6.22
pkgrel=2

_zpaq_ver=622

arch=('i686' 'x86_64')
license=('GPL3' 'MIT')

source=(http://mattmahoney.net/dc/zpaq${_zpaq_ver}.zip)
sha512sums=('d9a5dbbf407f22fe4d9e2f92233db320393e412535248ca576efc3c4d2314d54e196d35eb448add7fe055992662851adf2db7ade4a75fc7710ce649b11aa3721')

build()
{
	cd "$srcdir"

	if [ -z "$CC" ]
	then
		CC=gcc
	fi
	if [ -z "$CXX" ]
	then
		CXX=g++
	fi

	msg "Building libzpaq"
	$CXX -fPIC -shared $CXXFLAGS -DNDEBUG libzpaq.cpp -o libzpaq.so

	msg "Building zpaq"
	$CC $CFLAGS -DNDEBUG -fopenmp -c divsufsort.c
	$CXX $CXXFLAGS $LDFLAGS -DNDEBUG -fopenmp zpaq.cpp divsufsort.o -L. -lzpaq -o zpaq

	msg "Building zpaqd"
	$CXX $CXXFLAGS $LDFLAGS -DNDEBUG zpaqd.cpp -L. -lzpaq -o zpaqd
}

package()
{
	install -Dm 644 libzpaq.h  "$pkgdir/usr/include/libzpaq.h"
	install -Dm 644 libzpaq.so "$pkgdir/usr/lib/libzpaq.so"
	install -Dm 755 zpaq       "$pkgdir/usr/bin/zpaq"
	install -m  755 zpaqd      "$pkgdir/usr/bin/zpaqd"
}
