# Maintainer: Limao Luo <luolimao+AUR@gmail.com>
# Contributor: Lex Black <autumn-wind@web.de>
# Contributor: Silvio Knizek <killermoehre@gmx.net>
# Special Thanks to lh <jarryson@gmail.com> for the PKGBUILD
# Contributor: Xavier Devlamynck <magicrhesus@ouranos.be>

_pkgname=xfce4-panel
pkgname=$_pkgname-git
pkgver=20130126
pkgrel=1
pkgdesc="Panel for the Xfce desktop environment"
arch=(i686 x86_64)
license=(GPL2)
url=http://git.xfce.org/xfce/$_pkgname/tree/README
groups=(xfce4-git)
depends=(desktop-file-utils exo-git garcon-git libwnck libxfce4ui-git)
makedepends=(git xfce4-dev-tools)
provides=($_pkgname=4.10.0)
conflicts=($_pkgname)
options=(!libtool)
install=$_pkgname.install

_gitroot=git://git.xfce.org/xfce/$_pkgname
_gitname=$_pkgname

build() {
    cd "$srcdir"
    msg "Connecting to git.gnome.org GIT server...."
    if [[ -d $_gitname/.git ]]; then
        pushd $_gitname && git pull
        msg2 "The local files are updated."
        popd
    else
        git clone --depth 1 $_gitroot
    fi
    msg2 "GIT checkout done or server timeout"

    rm -rf $_gitname-build/
    cp -r $_gitname/ $_gitname-build/
    cd $_gitname-build/

    msg "Starting build..."
    ./autogen.sh --prefix=/usr \
        --sysconfdir=/etc \
        --libexecdir=/usr/lib \
        --localstatedir=/var \
        --enable-gio-unix \
        --disable-static

    make
}

package() {
    cd "$srcdir"/$_gitname-build/
    make DESTDIR="$pkgdir" install
}
