# Maintainer: Christian Hesse <mail@eworm.de>
# Contributor: Alexandre Boily <alexandreboily@gmail.com>
# Contributor: Illarion Kovalchuk <illarion.kovalchuk@gmail.com>
# Contributor: totoloco <totoloco at gmail _dot_com>
# Contributor: Ionut Biru <ibiru@archlinux.org>

pkgname=mysql-workbench
pkgver=5.2.47
pkgrel=2
pkgdesc="A cross-platform, visual database design tool developed by MySQL"
arch=('i686' 'x86_64')
url="https://www.mysql.com/products/workbench/"
license=('GPL2')
depends=('python' 'libzip' 'libmysqlclient' 'lua51' 'gtkmm' 'ctemplate'
	 'libgnome-keyring' 'libgl' 'python2-paramiko' 'python2-pexpect'
	 'pcre' 'tinyxml' 'libxml2' 'mysql-python' 'python2-pysqlite' 'cairo'
         'python-cairo' 'hicolor-icon-theme' 'desktop-file-utils' 'libiodbc')
makedepends=('boost' 'curl' 'mesa')
options=('!libtool !distcc !ccache')
source=("http://ftp.gwdg.de/pub/misc/mysql/Downloads/MySQLGUITools/${pkgname}-gpl-${pkgver}-src.tar.gz"
	'http://www.eworm.de/download/linux/mysql-workbench-hide-sleeping.patch')

build() {
	cd "${srcdir}/${pkgname}-gpl-${pkgver}-src"

	# fix compilation
	sed -i -e 's/python -c/python2 -c/gi' configure.in
	sed -i -e 's#glib/gthread.h#glib.h#gi' backend/wbpublic/sqlide/autocomplete_object_name_cache.cpp

	# add an option to hide sleeping connections in server status
	patch -Np1 < ${srcdir}/mysql-workbench-hide-sleeping.patch

	./autogen.sh
	./configure --prefix=/usr

	make
}

package() {
	cd "${srcdir}/${pkgname}-gpl-${pkgver}-src"

	make DESTDIR="${pkgdir}" install

	cd ${pkgdir}/usr/share/mysql-workbench/
	for i in *.py *.lua *.glade; do
		ln -f -s "/usr/share/mysql-workbench/${i}" "${pkgdir}/usr/lib/mysql-workbench/modules/"
	done
}

sha256sums=('e6928beb157f1e0abbb8885d16544eefe4d25b05f54dc0fb75b8375d8775838c'
            '56562cc85ef4bea8a19b5461fcdd0b5379b92145d1b0d67feeb60593996dc0a5')
