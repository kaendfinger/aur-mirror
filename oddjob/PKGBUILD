# Maintainer: Xiao-Long Chen <chenxiaolong@cxl.epac.to>

# Enable SELinux support?
build_selinux=false

pkgname=oddjob
pkgver=0.31.3
pkgrel=1
pkgdesc="A D-Bus service which runs odd jobs on behalf of client applications"
arch=('i686' 'x86_64')
url="http://www.fedorahosted.org/oddjob"
license=('BSD')
depends=('cyrus-sasl' 'dbus' 'krb5' 'libxml2' 'openldap' 'pam' 'python2' 'systemd')

if [ "x${build_selinux}" == "xtrue" ]; then
  depends+=('selinux-usr-libselinux')
fi

backup=('etc/oddjobd.conf.d/oddjobd-introspection.conf'
        'etc/oddjobd.conf.d/oddjobd-mkhomedir.conf'
        'etc/oddjobd.conf')
options=('!libtool')
install=${pkgname}.install
source=("http://fedorahosted.org/released/oddjob/oddjob-${pkgver}.tar.gz")
sha512sums=('3d3b51b15e04518caf1ad50d75c91202ab5cc40464be9ba820153ef48fc9667de3286c19ed95eb29a93173d96fc8c77e64e639864ed6e0a15d8579bd45190d8b')

if [ "x${build_selinux}" == "xfalse" ]; then
  source+=('0001_Remove_SELinux_support.patch')
  sha512sums+=('025e28ac5fb3991909c629d2dbf67a38bd4497cef422034c4d17938c5e3420bdeeae9c4a207c65c17cd47b803934021cf685bb426da17352cc44563283550d18')
fi

build() {
  cd "${srcdir}/${pkgname}-${pkgver}"

  SELINUX_OPTIONS=""
  if [ "x${build_selinux}" == "xtrue" ]; then
    SELINUX_OPTIONS="--with-selinux-acls --with-selinux-labels"
  else
    patch -p1 -i "${srcdir}/0001_Remove_SELinux_support.patch"
    aclocal --force --install
    autoreconf -vfi
  fi

  ./configure \
    --prefix=/usr \
    --sysconfdir=/etc \
    --libexecdir=/usr/lib/${pkgname} \
    --disable-static \
    --enable-pie \
    --enable-now \
    ${SELINUX_OPTIONS} \
    --without-python \
    `# --enable-xml-docs` \
    `# --enable-compat-dtd` \
    --enable-systemd \
    --disable-sysvinit

  make ${MAKEFLAGS}
}

package() {
  cd "${srcdir}/${pkgname}-${pkgver}"
  make DESTDIR="${pkgdir}/" install
}

# vim:set ts=2 sw=2 et:
