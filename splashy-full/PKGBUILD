#! /bin/bash
# Maintainer: Jorge Barroso <jorge.barroso.11 at gmail dot com>
# Contributors: Det, Lexiw, Angel 'angvp' Velasquez, dongiovanni, Darwin Bautista, Jeremy Sands
#               Tons of people here: http://bbs.archlinux.org/viewtopic.php?id=48978

pkgname=splashy-full
pkgver=20130302
pkgrel=3
pkgdesc="A boot splashing system"
arch=('any')
url="http://anonscm.debian.org/gitweb/?p=$provides/$provides;a=summary"
license=('GPL')
depends=('mkinitcpio' 'sysfsutils' 'glib2' 'directfb')
makedepends=('git')
options=('!libtool' '!distcc' '!makeflags')
provides=(splashy)
install=$pkgname.install
backup=(etc/splash.conf
        etc/splashy/config.xml)
source=(splash.conf
        splash-initscripts
        splashy-functions
        initcpio_hook
        initcpio_install
        automake1.10.patch
        configure.ac.patch)
sha512sums=('d48cffcd5f810c53100011b4f4b5437b69c2e0da3d261d22907dcbe20515abbeef06755c367ff3f0a77bf830eb67e165bb62eea717af76b4bcb0b631832af830'
            'd60dbb2b71f30b1d94a43f28fd8219df6271c08b26e92b5bd43d0beed0e9360afa1de250af01af104811cfcb7b30458669689d10b74a197033d6def562f60f3e'
            'c2cb16c9f61eb1feea8ebc22daa03d0aa3633caa30d2e45112e11da02940ee0028cb5e65e7ca07ae090f71150488ac0938e98dd4066b0e1e732710b96b1f99a3'
            '270511c7336aaf0dea0db13c868ebd93916519bb042c245f031703cc49ad395c444f33817803fbd11fa5e6dca42624135223028176bec8deae9622f15caf4494'
            '5d12f6edcf572d26daf51b95aba98db1097ff2f2aeab4cd6cc400ba84b1194589d5a0a0360f9f929f2fcf63c79e107892401d38336ef908794447c14bda2fdb8'
            'ae960a2d468e07c3a798544d710024b7f3bdb2d94f0be818986c0e0d13c3c87a451d4c332517f07ccc99a1f95c35edcb5fdfbcaafc9155fa97d0448d04abd233'
            '2ede62cc64e5348b564a93d07449bd3d60296203ca8112ded073239e77f288be52e774b3cd5fe2026d41cb8bc117b45c00208653809da0d2056a85ad8fab152f')
_gitroot=git://anonscm.debian.org/git/$provides/$provides.git
_gitname=$provides

build() {
  if [ -d "${_gitname}" ] ; then
    cd "${srcdir}/${_gitname}"
    msg2 "Updating local tree..."
    git pull --depth=1 origin master
  else
    cd "${srcdir}"
    msg2 "Cloning initial copy of ${_gitname}..."
    git clone --depth=1 "${_gitroot}" "${_gitname}"
    cd "${srcdir}/${_gitname}"
  fi

  # Fix the build
  sed -e 's|-Werror||g' -i configure.ac
  
  patch -p1 ./autogen.sh $srcdir/automake1.10.patch || return 1
  patch -p1 ./configure.ac $srcdir/configure.ac.patch || return 1
  
  ./autogen.sh --prefix=/usr \
               --libdir=/usr/lib \
               --sysconfdir=/etc \
               --sbindir=/sbin \
               --datarootdir=/usr/share \
               --mandir=/usr/share/man \
               --includedir=/usr/include	|| return 1

  make || return 1
}

package() {
  cd "${srcdir}/${_gitname}"

  make DESTDIR="${pkgdir}" install || return 1

  rm -rf "${pkgdir}/"{etc/{console-tools,default,init.d,lsb-base-logging.sh},usr/share/initramfs-tools}

  install -Dm644 ../initcpio_install "${pkgdir}/usr/lib/initcpio/install/$provides"
  install -Dm644 ../initcpio_hook "${pkgdir}/usr/lib/initcpio/hooks/$provides"
  install -Dm644 ../$provides-functions "${pkgdir}/etc/rc.d/$provides-functions"
  install -Dm644 ../splash-initscripts "${pkgdir}/etc/rc.d/functions.d/splash"
  install -Dm644 ../splash.conf "${pkgdir}/etc/splash.conf"

  sed -e 's|>/etc/$provides/themes<|>/usr/share/$provides/themes<|' -i "${pkgdir}/etc/$provides/config.xml"
}