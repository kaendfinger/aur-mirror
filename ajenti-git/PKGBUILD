# Please report PKGBUILD bugs at
# https://github.com/ystein/archlinux-aur-packages

# Maintainer: Yannik Stein <yannik.stein [at] gmail.com>
# Submitter: vbPadre <vbpadre [at] gmail.com>

pkgname=ajenti-git
pkgver=20130126
pkgrel=1
pkgdesc="An easy server administration frontend like webmin. Please report \
PKGBUILD bugs at https://github.com/ystein/archlinux-aur-packages."

arch=('any')
url="http://github.com/Eugeny/ajenti/"
license=('GPL')
depends=( 'python2' 'python2-lxml' 'python2-pyopenssl'
          'python2-feedparser' 'python2-gevent' 'python2-passlib' )
makedepends=('git' 'python2' 'python2-distribute')
optdepends=('python2-psutil: Task Manager plugin'
            'python2-beautifulsoup3: Munin plugin')
provides=('ajenti')
conflicts=('ajenti')
install='ajenti.install'

_gitroot=git://github.com/Eugeny/ajenti.git
_gitname=dev

backup=('etc/ajenti/ajenti.conf' 'etc/ajenti/users/admin.conf')

build() {
  if [[ ! -d "$pkgname" ]] ; then
    msg2 'Cloning GIT repository...'
    git clone "$_gitroot" "$pkgname"
  fi
  cd "$pkgname"
  git reset --hard
  git checkout "$_gitname"
  git pull

  msg2 'Replacing python shebang by python2 shebang...'
  find . -type f -exec sed -i \
    -e'1s|^#!/usr/bin/env python$|#!/usr/bin/env python2|' \
    -e '1s|^#!/usr/bin/python$|#!/usr/bin/env python2|' \
    "{}" \;
}

package() {
  cd "$pkgname"
  python2 setup.py install --root "$pkgdir"
  rm -r "$pkgdir/etc/init.d"
  install -D 'packaging/files/ajenti.arch' "$pkgdir/etc/rc.d/ajenti"
  install -D 'packaging/files/ajenti.service' "$pkgdir/usr/lib/systemd/system/ajenti.service"
  msg2 'Fixing config file permissions...'
  chmod o-rwx "$pkgdir/etc/ajenti/ajenti.conf"
}
