# Maintainer: Limao Luo <luolimao+AUR@gmail.com>
# Contributor: Brian Knox <taotetek@gmail.com>

_pkgname=rsyslog
pkgname=$_pkgname-dev
pkgver=7.3.5
pkgrel=1
pkgdesc="development release of rsyslog with liblognorm support"
arch=(i686 x86_64)
url=http://www.$_pkgname.com/
license=(GPL3)
depends=(json-c liblognorm)
makedepends=(gnutls libmysqlclient net-snmp postgresql-libs)
optdepends=(gnutls
    'libmysqlclient: MySQL Database Support'
    net-snmp
    'postgresql-libs: PostgreSQL Database Support')
provides=($_pkgname=$pkgver)
conflicts=($_pkgname)
backup=(etc/$_pkgname.conf
    etc/logrotate.d/$_pkgname
    etc/conf.d/$_pkgname)
options=(!libtool strip zipman)
source=($url/files/download/$_pkgname/$_pkgname-$pkgver.tar.gz
    $_pkgname.logrotate
    $_pkgname.conf.d)
sha256sums=('8135fa1dbcda4b5026879eb0e39556fa90cf4b3af28a80d3400e2d707571f43a'
    'fb6904956d2f7a439ac232976ba145436250a5de399823c2423220f7c23e23a3'
    '31ac5cc26b368307ac798c81b5c092f02be6ccbeb02c7254faedabb97acfcf0e')
sha512sums=('653333672f207637960586f569c7ae38282e8671bc4bb34960e09b4abd189413f6a811d305efff37a95e69ff2d7a064cb4e38cbbc187701cd1c288cc5e00f976'
    '9e6907d4fbe252f2051503e60163cc052b6f20c8f7a69db56b53fdd2df31649adcc373b504cfe7f734acdcf0590e98d0cf835103ee38cd2eed28ad36f4de349e'
    '68da6324941bdbb9fda0cf52585fd80b0281366b0652b68daf372ecbd1449866a0fa3a078b53896d96cc24d9db2ce5fa82f4646fce2f9fdcaab61438114574f9')

build() {
    cd "$srcdir"/$_pkgname-$pkgver/
    ./configure --prefix=/usr \
        --enable-mysql \
        --enable-pgsql \
        --enable-mail \
        --enable-imfile \
        --enable-imtemplate \
        --enable-snmp \
        --enable-imptcp \
        --enable-gnutls \
        --enable-inet \
        --enable-mmnormalize \
        --with-systemdsystemunitdir=/usr/lib/systemd/system
    make
}

package() {
    cd "$srcdir"/$_pkgname-$pkgver/
    make install DESTDIR="$pkgdir"
    install -Dm644 ../$_pkgname.logrotate "$pkgdir"/etc/logrotate.d/$_pkgname
    install -Dm644 ../$_pkgname.conf.d "$pkgdir"/etc/conf.d/$_pkgname
}
