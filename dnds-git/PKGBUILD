# Maintainer: Sebastien Duthil <duthils@free.fr>
pkgname=dnds-git
pkgver=20130109
pkgrel=1
pkgdesc="A Dynamic VPN solution that allow you to securely access and manage computers/devices via Internet"
arch=('x86_64')
url="https://github.com/nicboul/DNDS"
license=('GPL')
depends=('postgresql-libs' 'gcc-libs-multilib' 'uuid')
makedepends=('git' 'cmake' 'udt')
backup=('etc/dnds/dnc.conf' 'etc/dnds/dnd.conf' 'etc/dnds/dsd.conf')
source=(http://downloads.sourceforge.net/udt/udt.sdk.4.10.tar.gz
        dnds_arch.patch)
md5sums=('6bb2d8454d67c920eb446fddb7d030c4'
         '7d414e826969e638996a4d135d9c6cb6')

_gitroot="https://github.com/nicboul/DNDS.git"
_gitname="DNDS"


build_udt() {
  cd "$srcdir/"
  patch -p0 < "$srcdir/$_gitname-build/patch/udt4.9_ext_ptr.patch"

  cd 'udt4'

  if [ "$CARCH" == 'i686' ] ; then
    make_arch='arch=IA32'
  else
    make_arch='arch=AMD64'
  fi
  make "$make_arch" src.all
}

build() {
  git_patch_path="$srcdir/dnds_arch.patch"
  cd "$srcdir"
  msg "Connecting to GIT server...."

  if [[ -d "$_gitname" ]]; then
    cd "$_gitname" && git pull origin
    msg "The local files are updated."
  else
    git clone "$_gitroot" "$_gitname"
  fi

  msg "GIT checkout done or server timeout"
  msg "Starting build..."

  rm -rf "$srcdir/$_gitname-build"
  git clone "$srcdir/$_gitname" "$srcdir/$_gitname-build"

  build_udt
  ln -s "$srcdir/udt4" "$srcdir/$_gitname-build"

  cd "$srcdir/$_gitname-build"
  git apply "$git_patch_path"

  cd 'libdnds'
  cmake -DCMAKE_INSTALL_PREFIX:PATH=/usr .
  make
  cd ..

  cd 'dsd'
  cmake -DCMAKE_INSTALL_PREFIX:PATH=/usr .
  make
  cd ..

  cd 'dnd'
  cmake -DCMAKE_INSTALL_PREFIX:PATH=/usr .
  make
  cd ..

  cd 'dnc'
  cmake -DCMAKE_INSTALL_PREFIX:PATH=/usr .
  make
  cd ..
}

package() {
  cd "$srcdir/$_gitname-build"

  cd 'libdnds'
  make DESTDIR="$pkgdir" install
  cd ..

  cd 'dsd'
  make DESTDIR="$pkgdir" install
  cd ..

  cd 'dnd'
  make DESTDIR="$pkgdir" install
  cd ..

  cd 'dnc'
  make DESTDIR="$pkgdir" install
  cd ..
}

# vim:set ts=2 sw=2 et:
