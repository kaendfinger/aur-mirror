# Maintainer: Doug Newgard <scimmia22 at outlook dot com>
# Contributor: Ronald van Haren <ronald.archlinux.org>

pkgname=enlightenment17-git
pkgver=20130222
pkgrel=1
pkgdesc="Enlightenment window manager (formerly e17) - Development version"
arch=('i686' 'x86_64')
url="http://www.enlightenment.org"
license=('BSD')
depends=('elementary-git' 'xcb-util-keysyms' 'hicolor-icon-theme'
         'desktop-file-utils' 'udisks' 'ttf-font')
makedepends=('git')
optdepends=('connman: network module')
conflicts=('e' 'e-svn' 'enlightenment17' 'enlightenment17-svn')
provides=('e' 'e-svn' 'enlightenment17' 'enlightenment17-svn'
          'notification-daemon')
backup=('etc/enlightenment/sysactions.conf'
        'etc/xdg/menus/enlightenment.menu')
options=('!libtool' '!strip')
install=enlightenment17.install

_gitroot="git://git.enlightenment.org/core/enlightenment.git"
_gitname="enlightenment"

build() {
  cd "$srcdir"
  msg "Connecting to GIT server...."

  if [[ -d "$_gitname" ]]; then
    cd "$_gitname" && git pull origin
    msg "The local files are updated."
  else
    git clone "$_gitroot" "$_gitname"
  fi

  msg "GIT checkout done or server timeout"
  msg "Starting build..."

  rm -rf "$srcdir/$_gitname-build"
  git clone "$srcdir/$_gitname" "$srcdir/$_gitname-build"
  cd "$srcdir/$_gitname-build"

# set suspend and hibernate to use systemd
  sed -e 's:@SUSPEND@:/usr/bin/systemctl suspend:g' \
      -e 's:@HIBERNATE@:/usr/bin/systemctl hibernate:g' \
      -i "$srcdir/$_gitname-build/data/etc/sysactions.conf.in"

  ./autogen.sh --prefix=/usr \
	--sysconfdir=/etc

  make
}

package() {
  cd "$srcdir/$_gitname-build"

  make DESTDIR="$pkgdir" install

# install license files
  install -Dm644 COPYING "$pkgdir/usr/share/licenses/$pkgname/COPYING"
  install -Dm644 AUTHORS "$pkgdir/usr/share/licenses/$pkgname/AUTHORS"

# remove build directory
  rm -r "$srcdir/$_gitname-build"
}
