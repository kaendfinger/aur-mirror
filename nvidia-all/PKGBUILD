# Maintainer : Cian Mc Govern <cian@cianmcgovern.com>
# Contributor : Ng Oon-Ee <ngoonee.talk@gmail.com>
# Based on nvidia-beta by Dan Vratil <vratil@progdansoft.com>

pkgname=nvidia-all
pkgver=313.26
pkgrel=1
pkgdesc="NVIDIA drivers for linux." 
arch=('i686' 'x86_64') 
[ "$CARCH" = x86_64 ] && ARCH=x86_64 &&_srcname=NVIDIA-Linux-x86_64-${pkgver}-no-compat32
[ "$CARCH" = "i686" ] && ARCH=x86 && _srcname=NVIDIA-Linux-x86-${pkgver}
provides=("nvidia=${pkgver}")
url="http://www.nvidia.com/" 
depends=("nvidia-utils=${pkgver}") 
makedepends=('linux-headers' 'kmod>=9-2')
conflicts=('nvidia-96xx' 'nvidia-71xx' 'nvidia-legacy' 'nvidia' 'nvidia-beta' 'nvidia-beta-all') 
license=('custom') 
install=nvidia.install
source=("http://us.download.nvidia.com/XFree86/Linux-$ARCH/${pkgver}/${_srcname}.run") 

### Enable using of version numbers from pacman. This should only be used if you have kernels with
### long version numbers (for example if compiled with git-suffixes). This will not work with kernels
### compiled outside pacman's management, and takes much longer than the canonical method, due to 
### needing to search the local repository. Set to '1' to enable.
USE_PACMAN_VERSION=0

[ "$CARCH" = "i686" ] && md5sums=('3c2f5138d0fec58b27e26c5b37d845b8')
[ "$CARCH" = "x86_64" ] && md5sums=('2d35124fa5a4b009f170fe893b5d07e3')

build()
{
    # Extract the nvidia drivers
    cd "${srcdir}"
    if [ -d ${_srcname} ]; then
        rm -rf ${_srcname}
    fi
    sh ${_srcname}.run --extract-only
    cd ${_srcname}/kernel

  if [ "$USE_PACMAN_VERSION" = "0" ]; then
    _KERNELS=`file /boot/* | grep 'Linux kernel.*boot executable' | sed 's/.*version \([^ ]\+\).*/\1/'`
  else
    _PACKAGES=`pacman -Qsq linux`
    _KERNELS=`pacman -Ql $_PACKAGES | grep /modules.alias.bin | sed 's/.*\/lib\/modules\/\(.*\)\/modules.alias.bin/\1/g'`
  fi

  x=0;
  
  # Loop through all detected kernels
  for _kernver in $_KERNELS;
  do  
    cd ${srcdir}/${_srcname}
    cp -R kernel kernel-${_kernver}
    cd kernel-${_kernver}
    echo Building module for $_kernver
    
    # Don't build for linux-mainline
    if [ `echo $_kernver | grep -i 3.8` ]; then
        _KERNELS=`echo $_KERNELS | sed "s/$_kernver//"`;
        echo Not building $_pkgname for linux-mainline. Use nvidia-mainline instead!;
        sleep 5;
        continue;
    fi

    make SYSSRC=/usr/lib/modules/"${_kernver}"/build module  
  done

  # Modify .INSTALL (nvidia.install) file if needed
  if [ "$USE_PACMAN_VERSION" = "0" ]; then
    sed 's/USE_PACMAN_VERSION=1/USE_PACMAN_VERSION=0/g' $startdir/nvidia.install > $startdir/nvidia.installtemp
    mv $startdir/nvidia.installtemp $startdir/nvidia.install
  else
    sed 's/USE_PACMAN_VERSION=0/USE_PACMAN_VERSION=1/g' $startdir/nvidia.install > $startdir/nvidia.installtemp
    mv $startdir/nvidia.installtemp $startdir/nvidia.install
  fi
}

package() {
  if [ "$USE_PACMAN_VERSION" = "0" ]; then
    _KERNELS=`file /boot/* | grep 'Linux kernel.*boot executable' | sed 's/.*version \([^ ]\+\).*/\1/'`
  else
    _PACKAGES=`pacman -Qsq linux`
    _KERNELS=`pacman -Ql $_PACKAGES | grep /modules.alias.bin | sed 's/.*\/lib\/modules\/\(.*\)\/modules.alias.bin/\1/g'`
  fi

  # Find all extramodules directories
  _EXTRAMODULES=`find /usr/lib/modules -name version | sed 's|\/usr\/lib\/modules\/||; s|\/version||'`

  # Loop through all detected kernels
  for _kernver in $_KERNELS;
  do 
    # linux-mainline check
    if [ `echo $_kernver | grep -i 3.8` ]; then
        _KERNELS=`echo $_KERNELS | sed "s/$_kernver//"`;
        continue;
    fi
    cd "${srcdir}/${_srcname}/kernel-${_kernver}" 
    # Loop through all detected extramodules directories
    for _moduledirs in $_EXTRAMODULES
    do
      # Check which extramodules directory corresponds with the built module
      if [ `cat "/usr/lib/modules/${_moduledirs}/version"` = $_kernver ]; then
        mkdir -p "${pkgdir}/usr/lib/modules/${_moduledirs}/"
        install -m644 nvidia.ko "${pkgdir}/usr/lib/modules/${_moduledirs}/"
          gzip "${pkgdir}/usr/lib/modules/${_moduledirs}/nvidia.ko"                      
      fi
    done
  done
    # Blacklist nouveau since 2.6.34
    mkdir -p "${pkgdir}/etc/modprobe.d/"
    echo "blacklist nouveau" >> "${pkgdir}/etc/modprobe.d/nouveau_blacklist.conf"
                      
}
