# Contributor: A. Fluteaux <sigma_g@melix.net>
# Contributor: Elmo Todurov <todurov+arch@gmail.com>
pkgname=mudlet-git
pkgver=20130305
pkgrel=1
pkgdesc="GIT version of mudlet"
arch=('i686' 'x86_64')
url="http://sourceforge.net/projects/mudlet/"
license=('GPL')
depends=('lua51' 'qt4' 'phonon' 'mesa' 'hunspell' 'zziplib' 'luazip5.1' 'lrexlib-pcre5.1' 'lua51-filesystem' 'yajl1' 'lua51-sql-sqlite' 'glu')
makedepends=('cmake' 'boost' 'git')
provides=('mudlet')
_gitroot="git://mudlet.git.sourceforge.net/gitroot/mudlet/mudlet"
_gitname="mudlet"

build() {
    cd "$srcdir"
    msg "Connecting to GIT server...."

    # Clone or update the Git source
    if [ -d $_gitname ] ; then
        cd $_gitname && git pull origin
        msg "The local files are updated."
    else
        git clone $_gitroot $_gitname
    fi

    # Delete and recreate (this is very fast) the compiling directory
    rm -rf "$srcdir/$_gitname-build"
    git clone "$srcdir/$_gitname" "$srcdir/$_gitname-build"
    cd "$srcdir/$_gitname-build/src"

    # The Arch Linux yajl1 package provides includes in /usr/include/yajl1, libs in /usr/lib/yajl1 (you need rpath)
    sed -i '4s,$, -I/usr/include/yajl1,' src.pro
    sed -i '5s,$, -I/usr/include/yajl1,' src.pro

    # We need lua 5.1
    sed -i '12s,-llua$,-llua5.1,' src.pro

    # Phonon includes should be like this
    for i in *.{cpp,h} ; do sed -i 's/#include <phonon>/#include <phonon\/MediaObject>\n#include <phonon\/AudioOutput>/' $i ; done

    # Hunspell something or other
    sed -i 's/-lhunspell/-lhunspell-1.3/' src.pro
    
    # Fixes this issue: https://bugreports.qt-project.org/browse/QTBUG-22829
    sed -i '/MOC_DIR.*/i QMAKE_MOC += -DBOOST_TT_HAS_OPERATOR_HPP_INCLUDED' src.pro
    
    # Adding a library they may have forgotten to add
    sed -i '/.*unix:LIBS.*/a -lz \\' src.pro

    # Set a system-wide Lua scripts directory
    sed -i 's,QString path = "mudlet-lua/lua/LuaGlobal.lua";,QString path = "/usr/share/mudlet/lua/LuaGlobal.lua";,' TLuaInterpreter.cpp
    sed -i 's;local result, msg = pcall(dofile, "./mudlet-lua/lua/" .. package);local result, msg = pcall(dofile, "/usr/share/mudlet/lua/" .. package);' mudlet-lua/lua/LuaGlobal.lua

    qmake

    # How do you fix this in src.pro?
    # Add proper link-time and runtime path for dynamic libs
    sed -i '18s.$. -L/usr/lib/yajl1 -Wl,-rpath=/usr/lib/yajl1.' Makefile

    make

    cd ../..
}

package() {
    cd $srcdir/$_gitname-build/src
    mkdir -p ${pkgdir}/usr/bin
    mkdir -p ${pkgdir}/usr/share/mudlet/lua/geyser
    mkdir -p ${pkgdir}/usr/share/applications
    mkdir -p ${pkgdir}/usr/share/pixmaps

    install -m 755 mudlet ${pkgdir}/usr/bin/mudlet || return 1
    install -m 644 mudlet-lua/lua/*.lua ${pkgdir}/usr/share/mudlet/lua || return 1
    install -m 644 mudlet-lua/lua/geyser/* ${pkgdir}/usr/share/mudlet/lua/geyser || return 1
    install -m 644 ../mudlet.desktop ${pkgdir}/usr/share/applications || return 1
    install -m 644 ../mudlet.png ${pkgdir}/usr/share/pixmaps || return 1
}
