# Maintainer: Alper KANAT <alperkanat@raptiye.org>

pkgname="aws-cli"
pkgver=20130207
pkgrel=1
pkgdesc="Universal Command Line Interface for Amazon Web Services"
url="https://github.com/aws/aws-cli"
arch=('any')
license=('Apache')
depends=('python2' 'python2-botocore-git')
makedepends=('python2-distribute' 'git')

_gitroot="git://github.com/aws/aws-cli.git"
_gitname="aws-cli"

build() { 
	cd $srcdir

	# checking out or updating code
	msg "Downloading source code from git server..."

	if [ -d ${srcdir}/${_gitname} ]; then
		cd ${_gitname} && git pull origin
		msg "The local files are updated."
	else
		git clone ${_gitroot}
	fi

	msg "GIT checkout done or server timeout."

	# copying files..
	msg "Copying files..."
	cd $srcdir/$_gitname

	# first copying aws-cli
	python2 setup.py install --root=$pkgdir --optimize=1 || return 1

	# copying license information
	install -D -m644 LICENSE.txt $pkgdir/usr/share/licenses/$pkgname/LICENSE

	# copying docs
	install -D -m644 README.md $pkgdir/usr/share/doc/$pkgname/README
	install -D -m644 requirements.txt $pkgdir/usr/share/doc/$pkgname/requirements.txt

	# copying bash completion script
	cd $srcdir/$_gitname/bin
	install -D -m644 zsh_complete.sh $pkgdir/etc/zsh/aws_complete.zsh
}
