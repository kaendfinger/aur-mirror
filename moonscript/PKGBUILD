# Maintainer: Alexander Rødseth <rodseth@gmail.com>

pkgname=moonscript
pkgver=0.2.2
_luaver=5.1
pkgrel=1
pkgdesc='Dynamic scripting language that compiles into Lua'
arch=('any')
url='http://moonscript.org/'
license=('MIT')
depends=('luarocks' 'lpeg' "lua>=$_luaver" 'luafilesystem')
makedepends=('git' 'curl')
source=("http://moonscript.org/rocks/moonscript-dev-1.rockspec")
md5sums=('f053fe0065e8e1c29ebed8d6c3563811')

build() {
  cd "$srcdir"

  msg2 "Fetching documentation..."
  # These changes a lot, so md5-sum-ing them is like fighting windmills
  curl --progress-bar -OL \
    "https://raw.github.com/leafo/moonscript/master/README.md"
  curl --progress-bar -OL \
    "https://raw.github.com/leafo/moonscript/master/docs/reference_manual.html"
  curl --progress-bar -OL \
    "http://moonscript.org/reference/index.html"
}

package() {
  cd "$srcdir"

  msg2 "Packaging moonscript..."
  mkdir "$pkgdir/usr"
  luarocks install moonscript-dev-1.rockspec --to="$pkgdir/usr"
  install -Dm644 reference_manual.html "$pkgdir/usr/share/doc/$pkgname/reference_manual.html"
  install -Dm644 index.html "$pkgdir/usr/share/doc/$pkgname/index.html"
  install -Dm644 README.md "$pkgdir/usr/share/licenses/$pkgname/README.md"

  msg2 "Patching..."
  sed -i "s:$pkgdir::" "$pkgdir/usr/bin/moon"
  sed -i "s:$pkgdir::" "$pkgdir/usr/bin/moonc"

  msg2 "Cleaning up..."
  rm "$pkgdir/usr/lib/lua/$_luaver/lfs.so"
  rm "$pkgdir/usr/lib/lua/$_luaver/lpeg.so"
  rm "$pkgdir/usr/share/lua/$_luaver/re.lua"
  chmod 755 "$pkgdir/usr" "$pkgdir/usr/bin/" "$pkgdir/usr/share/lua/$_luaver/" \
    "$pkgdir/usr/lib/lua/$_luaver/" "$pkgdir/usr/lib" "$pkgdir/usr/share" \
    "$pkgdir/usr/share/lua" "$pkgdir/usr/lib/lua"
}

# vim:set ts=2 sw=2 et:
