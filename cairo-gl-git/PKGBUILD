# Maintainer: Limao Luo <luolimao+AUR@gmail.com>
# Contributor: Aljosha Papsch <papsch.al@gmail.com>
# Contributor: Jan de Groot <jgc@archlinux.org>
# Contributor: Disfido <disfido@gmail.com>

_pkgname=cairo-gl
pkgname=$_pkgname-git
pkgver=20130210
_realver=1.12.8
pkgrel=1
pkgdesc="Cairo vector graphics library with GL and EGL enabled"
arch=(i686 x86_64)
url=http://cairographics.org/
license=(LGPL2.1 MPL)
depends=(fontconfig libegl libpng libxext libxrender pixman)
makedepends=(gtk-doc git librsvg mesa poppler-glib)
provides=(cairo=$_realver cairo-xcb=$_realver)
conflicts=(cairo)
options=(!libtool)

_gitroot=git://anongit.freedesktop.org/git/cairo
_gitname=cairo

build() {
    cd "$srcdir"
    msg "Connecting to GIT server..."
    if [[ -d $_gitname/.git ]]; then
        pushd $_gitname && git pull
        msg2 "The local files are updated."
        popd
    else
        git clone --depth 1 $_gitroot
    fi
    msg2 "GIT checkout done or server timeout"

    rm -rf $_gitname-build/
    cp -r $_gitname/ $_gitname-build/
    cd $_gitname-build/

    msg "Building..."
    # glesv2 and gl cannot be enabled at the same time.
    # Please take a look at: http://lists.freedesktop.org/archives/cairo/2011-July/022138.html
    ./autogen.sh \
        --prefix=/usr \
        --sysconfdir=/etc \
        --localstatedir=/var \
        --enable-gl \
        --enable-egl \
        --enable-xcb \
        --disable-static
    make
}

package() {
    cd "$srcdir"/$_gitname-build/
    make DESTDIR="$pkgdir" install
}
