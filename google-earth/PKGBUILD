# Maintainer: Det
# Contributors: 458italia, Madek, Berseker, Syr

pkgname=google-earth
pkgver=7.0.3.8542
pkgrel=1
pkgdesc="A 3D interface to view satellite images of Earth and other objects"
arch=('i686' 'x86_64')
url="http://www.google.com/earth"
license=('custom')
depends=('desktop-file-utils' 'hicolor-icon-theme' 'ld-lsb' 'lib32-fontconfig' 'lib32-libgl' 'lib32-gcc-libs' 'lib32-libsm' 'lib32-libxrender' 'lib32-mesa' 'shared-mime-info')
optdepends=('lib32-ati-dri: For the open source AMD/ATI Radeon driver'
            'lib32-catalyst-utils: For AMD Catalyst'
            'lib32-gtk2: SCIM support'
            'lib32-intel-dri: For the open source Intel driver'
            'lib32-nouveau-dri: For the open source Nouveau driver'
            'lib32-nss-mdns: In case the application fails to contact the servers'
            'lib32-nvidia-utils: For the NVIDIA driver'
            'lib32-nvidia-utils-bumblebee: For the NVIDIA driver + Bumblebee setups'
            'qt4: For changing the font size with qtconfig'
            'ttf-ms-fonts: Fonts')
options=('!emptydirs')
install=$pkgname.install
_arch=i386  # Workaround for the AUR Web interface Source parser
[ "$CARCH" == "x86_64" ] && _arch=amd64
source=("$pkgname-stable_$pkgver_$_arch.deb"::"http://dl.google.com/earth/client/current/$pkgname-stable_current_$_arch.deb"
        'googleearth'
        "$pkgname-fonts.conf"
        "$pkgname-mimetypes.xml")
md5sums=('d34b798b2ff2435b431935a134ad35e8'  # google-earth-stable_$pkgver_amd64.deb
         'e8434412823bba56c49bba366aca0df4'  # googleearth
         'cdfd204594e927c6e2a3b8cd96ac8e32'  # google-earth-fonts.conf
         'e3c67b8d05c3de50535bd7e45eee728e') # google-earth-mimetypes.xml

if [[ "$CARCH" == "i686" ]]; then
  depends=('desktop-file-utils' 'fontconfig' 'hicolor-icon-theme' 'ld-lsb' 'libgl' 'libsm' 'libxrender' 'mesa' 'shared-mime-info')
  optdepends=('ati-dri: For the open source AMD/ATI Radeon driver'
              'catalyst-utils: For AMD Catalyst'
              'gtk2: SCIM support'
              'intel-dri: For the open source Intel driver'
              'nouveau-dri: For the open source Nouveau driver'
              'nss-mdns: In case the application fails to contact the servers'
              'nvidia-utils: For the NVIDIA driver'
              'nvidia-utils-bumblebee: For the NVIDIA driver + Bumblebee setups'
              'qt4: For changing the font size with qtconfig'
              'ttf-ms-fonts: Fonts')
  md5sums[0]='5c0925682acfd90ff93ca91c5fd37254'  # google-earth-stable_$pkgver_i386.deb
fi

_instdir=/opt/google/earth/free/
PKGEXT=".pkg.tar"

package() {
  msg2 "Extracting the data.tar.lzma"
  bsdtar -xf data.tar.lzma -C "$pkgdir/"

  msg2 "Moving stuff in place"
  # The .desktop
  mv "$pkgdir"/$_instdir/$pkgname.desktop "$pkgdir"/usr/share/applications/

  # The MIME package
  install -Dm644 $pkgname-mimetypes.xml "$pkgdir"/usr/share/mime/packages/$pkgname-mimetypes.xml

  # Icons
  for i in 16 22 24 32 48 64 128 256; do
    install -Dm644 "$pkgdir"/$_instdir/product_logo_${i}.png "$pkgdir"/usr/share/icons/hicolor/${i}x${i}/apps/$pkgname.png
  done

  # The license
  install -d "$pkgdir"/usr/share/licenses/$pkgname/
  curl -s $url/license.html -o "$pkgdir"/usr/share/licenses/$pkgname/license.html

  msg2 "Removing the Debian-intended cron job and duplicated images"
  rm "$pkgdir"/etc/cron.daily/$pkgname "$pkgdir"/$_instdir/product_logo_*.png

  msg2 "Fixing the coordinates/fontconfig bug"
  install -m755 googleearth "$pkgdir"/$_instdir/
  install -m644 $pkgname-fonts.conf "$pkgdir"/$_instdir/
}