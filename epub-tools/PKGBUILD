# Maintainer: Dino Morelli <dino@ui3.info>

# custom variables
_hkgname=epub-tools

# PKGBUILD options/directives
pkgname=epub-tools
pkgver=2.1.1
pkgrel=1
pkgdesc="A suite of command-line utilities for creating and manipulating epub book files"
url="http://ui3.info/d/proj/epub-tools.html"
license=("BSD3")
arch=('i686' 'x86_64')
makedepends=("ghc")
depends=()
options=('strip')
source=("http://hackage.haskell.org/packages/archive/${_hkgname}/${pkgver}/${_hkgname}-${pkgver}.tar.gz")

sha256sums=("1cad80346e2c8f2601e009abb85b174ce5094092de0dd04a936ef62e5212a948")

# PKGBUILD functions
build() {
   checkCabalDev

   cd ${srcdir}/${_hkgname}-${pkgver}
   cabal-dev update
   cabal-dev install-deps
   cabal-dev configure -O --prefix=/usr --docdir=/usr/share/doc/${pkgname}
   cabal-dev build
}

package() {
    cd ${srcdir}/${_hkgname}-${pkgver}
    runhaskell Setup copy --destdir=${pkgdir}
}


checkCabalDev() {
   [ $(which cabal-dev 2>/dev/null) ] || {
      cat <<ERRORMSG
No cabal-dev binary found. ABORTING BUILD NOW!


The sort-of easy fix is:

   Install cabal-dev-git from the AUR first

But.. what I myself do instead:

   Get cabal-dev from github and build it with cabal, with --user,
   and then make sure the resulting cabal-dev binary is on the PATH.

   https://github.com/creswick/cabal-dev


What does this really mean? Read on..

The goal of installing this package is not to have its build deps
installed on your system, it's to have its binaries. To that end,
I chose to build it with cabal-dev which will build in a sandboxed
environment.

I don't want to make a decision as to the right way for you to get
cabal-dev installed on your system, so that's why we're here. You
need cabal-dev installed and I couldn't find one.

Also, cabal-dev-git is patched to build against the current ghc
that Arch has, so we're preferring that over cabal-dev in the AUR.
ERRORMSG
      false
   }
}
