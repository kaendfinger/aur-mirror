# Maintainer: Christian Hesse <mail@eworm.de>
# Contributor: Benjamin Bukowski <benjamin.bukowski@gmail.com>

pkgname=atftp-git
pkgver=0.7.1.0.gbe3291a
pkgrel=1
pkgdesc="a client/server implementation of the TFTP protocol - git checkout"
arch=('i686' 'x86_64')
url="http://sourceforge.net/projects/atftp/"
license=('GPL')
depends=('pcre' 'readline')
conflicts=('atftp')
provides=('atftp')
backup=('etc/conf.d/atftpd')
source=('atftp::git://atftp.git.sourceforge.net/gitroot/atftp/atftp'
	'atftpd.conf'
	'atftpd.rc'
	'atftpd.service')

pkgver() {
	cd atftp/
	git describe --tags --long | sed 's|^[^0-9]*||;s|-|.|g'
}


build() {
	cd atftp/
 
	./autogen.sh
	./configure --prefix=/usr --mandir=/usr/share/man \
		--enable-libreadline --disable-libwrap
	make
}

package() {
	cd atftp/

	make DESTDIR=${pkgdir} install

	install -Dm644 ${srcdir}/atftpd.conf ${pkgdir}/etc/conf.d/atftpd
	install -Dm755 ${srcdir}/atftpd.rc ${pkgdir}/etc/rc.d/atftpd
	install -Dm644 ${srcdir}/atftpd.service ${pkgdir}/usr/lib/systemd/system/atftpd.service
	install -dm775 --group=nobody ${pkgdir}/var/tftpboot 

	# remove in.tftpd link as it conflicts with extra/tftp-hpa
	rm "${pkgdir}/usr/share/man/man8/in.tftpd.8"
	rm "${pkgdir}/usr/sbin/in.tftpd"
}

sha256sums=('SKIP'
            '100225ede793b9c01d0c5bcb9c2cc1933552609d3a9439b9bd03dfa0b269143e'
            'b3e33fc4de872472334dfd7bc577c2cbe5e15a531da53d0d2c3ef2aa4d3fe7b4'
            '1957eca701db52e2dc9964bc847f9eace943f766bc8fff43d2f2490a6ab10ad5')
