# Maintainer: Nicky726 (Nicky726 <at> gmail <dot> com)                 
# Contributor: Sergej Pupykin (pupykin <dot> s+arch <at> gmail <dot> com)
# Contributor: angelux/xangelux (xangelux <at> gmail <dot> com)

pkgname=selinux-usr-policycoreutils
_origname=policycoreutils
_release=20120924
pkgver=2.1.13
pkgrel=2
pkgdesc="SELinux userspace (policycoreutils)"
arch=('i686' 'x86_64')
url="http://userspace.selinuxproject.org"
license=('GPL')
groups=('selinux' 'selinux-userspace')
depends=('selinux-usr-libsemanage>=2.1.0' 'selinux-usr-libselinux>=2.1.0' 'python2'
         'libcap-ng' 'libcgroup' 'dbus-glib')
options=(!emptydirs)
source=(http://userspace.selinuxproject.org/releases/${_release}/${_origname}-${pkgver}.tar.gz
        policycoreutils-seunshare.diff
        restorecond.service)
sha256sums=('34040f06f3111d9ee957576e4095841d35b9ca9141ee8d80aab036cbefb28584'
            '10e1dfcd1db7fb1c2efa26151f37ede7d3da6cada393e186ffdeac234a8635fc'
            '4d01581a32888699d31f4e12058998fc39e22b844965155a8cbe9161fde1900b')

build() {
  cd "${srcdir}/${_origname}-${pkgver}"
  sed -i -e "s/shell python -c/shell python2 -c/" "semanage/Makefile"
  sed -i -e "s/shell python -c/shell python2 -c/" "sandbox/Makefile"

	patch -Np1 -i ../${_origname}-seunshare.diff
  make
}

package(){
  cd "${srcdir}/${_origname}-${pkgver}"

  make DESTDIR="${pkgdir}" install

  # Move daemons to correct locations
  mv "${pkgdir}/etc/rc.d/init.d/restorecond" \
        "${pkgdir}/etc/rc.d/"
  sed -i -e "s/init.d\/functions/functions.d/g" \
        "${pkgdir}/etc/rc.d/restorecond"
  rmdir "${pkgdir}/etc/rc.d/init.d/"
  # Install unit file
  install -Dm644 "${srcdir}/restorecond.service" \
  "${pkgdir}/usr/lib/systemd/system/restorecond.service"

  # Following are python2 scripts
  sed -i -e "s/python -E/python2 -E/" \
        "${pkgdir}/usr/bin/audit2allow"
  sed -i -e "s/python -E/python2 -E/" \
        "${pkgdir}/usr/bin/audit2why"
  sed -i -e "s/python -E/python2 -E/" \
        "${pkgdir}/usr/bin/chcat"
  sed -i -e "s/python -E/python2 -E/" \
        "${pkgdir}/usr/bin/sepolgen-ifgen"
  sed -i -e "s/python -E/python2 -E/" \
        "${pkgdir}/usr/sbin/semanage"
  sed -i -e "s/python -E/python2 -E/" \
        "${pkgdir}/usr/lib/python2.7/site-packages/seobject.py"
}
