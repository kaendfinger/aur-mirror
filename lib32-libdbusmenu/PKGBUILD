# Maintainer: Jameson Pugh <imntreal@gmail.com>

pkgname=lib32-libdbusmenu
_pkgbase=libdbusmenu
pkgver=12.10.2
pkgrel=4
pkgdesc="Small little library that passes a menu structure across DBus"
arch=('i686' 'x86_64')
url="https://launchpad.net/dbusmenu"
license=('LGPL')
depends=('lib32-glib2' "$_pkgbase" 'lib32-json-glib')
makedepends=('gcc-multilib' 'lib32-gtk2' 'intltool' 'gnome-doc-utils' 'gobject-introspection' 'vala' 'lib32-json-glib' $_pkgbase)
optdepends=('lib32-python2: for dbusmenu-dumper tool'
            'lib32-gtk2: for dbusmenu-bench tool'
            'lib32-json-glib: for dbusmenu-testapp tool')
options=('!libtool' '!emptydirs')
install=$pkgname.install
source=(http://launchpad.net/dbusmenu/${pkgver%.*}/$pkgver/+download/$_pkgbase-$pkgver.tar.gz)
md5sums=('e30fc986b447f62513d61225fa573a70')

build() {
  export CC='gcc -m32'
  export CXX='g++ -m32'
  export PKG_CONFIG_PATH='/usr/lib32/pkgconfig'

  cd "$srcdir/$_pkgbase-$pkgver"
  sed -i 's@^#!.*python$@#!/usr/bin/python2@' tools/dbusmenu-bench

  ./configure --prefix=/usr --sysconfdir=/etc --localstatedir=/var --libexecdir=/usr/lib/$pkgname \
              --libdir=/usr/lib32 --disable-static --with-gtk=2
  make
}

package() {
  cd "$srcdir/$_pkgbase-$pkgver"

  make -C libdbusmenu-glib DESTDIR="$pkgdir/" install
  make -C tools DESTDIR="$pkgdir/" install
  make -C docs DESTDIR="$pkgdir/" install
  rm -rf "${pkgdir}"/usr/{include,share,bin}
}
