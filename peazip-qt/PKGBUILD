# Maintainer: Limao Luo <luolimao+AUR@gmail.com>
# Contributor: TuxSpirit <tuxspirit@archlinux.fr>

_pkgname=peazip
pkgname=$_pkgname-qt
pkgver=4.9
pkgrel=1
pkgdesc="QT archiver utility"
arch=(i686 x86_64)
url=http://www.peazip.org/peazip-linux.html
license=(Artistic2.0 LGPL2.1)
depends=(balz desktop-file-utils lib32-{curl,gmp4,libx11,ncurses,qt4pas} p7zip quad unace upx zpaq)
[[ $CARCH = "i686" ]] && depends=(${depends[@]/lib32-/})
provides=($_pkgname)
conflicts=($_pkgname-gtk2)
install=$_pkgname.install
source=($pkgname-$pkgver.tgz::http://downloads.sourceforge.net/project/$_pkgname/$pkgver/$_pkgname-$pkgver.LINUX.Qt.tgz $_pkgname.desktop)
noextract=($pkgname-$pkgver.tgz)
sha256sums=('e1c13c3d3210dd876b28d37140e41e89fc6042f905c82b23f17f819d01906766'
    'c3ca0fa3e89626447acc376e2392fdd4e452c2b8f0685324eb978ab63c217fce')
sha512sums=('250e7ce38a94f146ffe0acb30a0b7ba1d0a185ae838ac7da8b6d58197868d76ec469d4d2621b2de763b241542a581aa446c202ed229fa6567b816422bf2b8fc2'
    '0fb2e432f3b403df55134ebd57aacd2fb1eea75ef96520a5e4d7827ec319ca16a45c6e147504af0472704872fc601273b38d8f45ebfb8f68328266d240a1f71f')

_resdir="$pkgdir"/usr/lib/$_pkgname/res

package() {
    install -d "$srcdir"/$pkgname/
    cd "$srcdir"/$pkgname/
    tar xf ../$pkgname-$pkgver.tgz -C ./
    cd usr/local/share/PeaZip/res/

    install -Dm755 ../$_pkgname "$pkgdir"/usr/lib/$_pkgname/$_pkgname

    for i in pea pealauncher rnd; do
        install -Dm755 $i "$_resdir"/$i
    done
    for i in arc/{arc,*.sfx}; do
        install -Dm755 $i "$_resdir"/$i
    done
    for i in altconf.txt lang/* themes/{{nographic,seven}-embedded/*,*.7z} arc/arc.{ini,groups}; do
        install -Dm644 $i "$_resdir"/$i
    done

    install -d "$_resdir"/7z/Codecs/
    for i in 7z{,a,r,.so,Con.sfx,.sfx} Codecs/Rar29.so; do
        ln -sf /usr/lib/p7zip/$i "$_resdir"/7z/$i
    done
    for i in quad/{balz,quad} unace/unace upx/upx lpaq/lpaq8 paq/paq8o zpaq/zpaq; do
        install -d "$_resdir"/$(dirname $i)/
        ln -sf /usr/bin/$(basename $i) "$_resdir"/$i
    done
    install -d "$pkgdir"/usr/bin/
    for i in /usr/lib/$_pkgname/{$_pkgname,res/pea,res/pealauncher}; do
        ln -sf $i "$pkgdir"/usr/bin/$(basename $i)
    done

    install -Dm644 "$srcdir"/$pkgname/usr/local/share/icons/$_pkgname.png "$pkgdir"/usr/share/pixmaps/$_pkgname.png
    desktop-file-install "$srcdir"/$_pkgname.desktop --dir "$pkgdir"/usr/share/applications/
}
