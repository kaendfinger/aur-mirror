# Maintainer: Filip Brcic <brcha@gna.org>
pkgname=mingw-w64-pthreads
pkgver=2.8.0
pkgrel=2
mingw_headers_ver=2.0.7
_prerelease_date=20110511
pkgdesc='MinGW-w64 pthreads library'
arch=('any')
url='http://sourceware.org/pthreads-win32/'
license=('LGPLv2+')
groups=('mingw-w64-toolchain' 'mingw-w64')
depends=()
makedepends=('mingw-w64-gcc-base' 'mingw-w64-binutils' 'mingw-w64-crt' 'mingw-w64-headers-bootstrap')
optdepends=()
provides=('mingw-w64-headers-bootstrap' 'mingw-w64-winpthreads')
conflicts=('mingw-w64-winpthreads')
replaces=('mingw-w64-winpthreads')
backup=()
options=('!strip' '!buildflags' '!libtool' '!emptydirs')
source=("pthreads-w32-${_prerelease_date}.tar.bz2"
        'mingw32-pthreads-2.8.0-20110511-use-wine-for-tests.patch'
        'mingw32-pthreads-2.8.0-20110511-no-failing-tests.patch'
        'mingw32-pthreads-flags.patch'
        "http://sourceforge.net/projects/mingw-w64/files/mingw-w64/mingw-w64-release/mingw-w64-v${mingw_headers_ver}.tar.gz")
md5sums=('30200c77d2042bdb479a1c8cf6a44471'
         '8fd397178a270e38d816a45ba917f36b'
         'f95940870ad03a9447c9d049ad78d110'
         'a13189af1cc44f0eb5602a3c6bd247cc'
         '07627b66731f40f01eb8547cfdd299eb')

_architectures="i686-w64-mingw32 x86_64-w64-mingw32"

build() {
  cd ${srcdir}/pthreads-w32-${_prerelease_date}

  patch -Np1 -i ${srcdir}/mingw32-pthreads-2.8.0-20110511-use-wine-for-tests.patch
  patch -Np1 -i ${srcdir}/mingw32-pthreads-2.8.0-20110511-no-failing-tests.patch
  patch -Np1 -i ${srcdir}/mingw32-pthreads-flags.patch

  for _arch in ${_architectures}
  do
    cp -av ${srcdir}/pthreads-w32-${_prerelease_date} ${srcdir}/build-${_arch}
    
    pushd ${srcdir}/build-${_arch}
    
    export CFLAGS="-O2 -g -pipe -Wall -Wp,-D_FORTIFY_SOURCE=2 -fexceptions --param=ssp-buffer-size=4"

    sed -i s/"target  = "/"target  = ${_arch}"/ GNUmakefile

    make clean
    make CROSS=${_arch}- GC-inlined
    make clean
    make CROSS=${_arch}- GCE-inlined
    mv libpthreadGC2.a libpthreadGC2.dll.a
    mv libpthreadGCE2.a libpthreadGCE2.dll.a
    make clean
    make CROSS=${_arch}- GC-static

    popd
  done
}

package() {
  cd ${srcdir}

  for _arch in ${_architectures}
  do
    pushd build-${_arch}

    mkdir -p ${pkgdir}/usr/${_arch}/{bin,lib,include}

    install -m 0755 *.dll ${pkgdir}/usr/${_arch}/bin
    install -m 0644 *.a ${pkgdir}/usr/${_arch}/lib
    install -m 0644 {pthread,sched,semaphore}.h ${pkgdir}/usr/${_arch}/include

    install -m 0644 ${srcdir}/mingw-w64-v${mingw_headers_ver}/mingw-w64-headers/crt/pthread_{unistd,time}.h  ${pkgdir}/usr/${_arch}/include

    # Symlink libpthreadGC2.a to libpthread.a
    ln -s libpthreadGC2.a ${pkgdir}/usr/${_arch}/lib/libpthread.a
    ln -s libpthreadGC2.dll.a ${pkgdir}/usr/${_arch}/lib/libpthread.dll.a

    popd
  done
}
