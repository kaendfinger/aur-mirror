# Contributor: Prurigro
# Maintainer: Prurigro

pkgname=cjdns-git
pkgver=130221
pkgrel=1
pkgdesc="A routing engine designed for security, scalability, speed and ease of use."
url="https://github.com/cjdelisle/cjdns"
license="GPLv3"
makedepends=('git' 'make' 'gcc' 'patch')
optdepends=('python2: cjdnslog support')
arch=('i686' 'x86_64' 'armv7h')

source=('cjdns.conf.d' 'cjdns.rc.d' 'cjdns.service' 'cjdnslog.patch')
sha1sums=('3bd5a146323a074beb131f026def8f75b71cb6aa'
          '24223de2902210953c61bdeb4cda214f7fe74c88'
          'b8c9f012be435b1f3a03eada596be4b7b94b735d'
          '1427d6e67632864bda1792b0661b7a72b082bdd9')

install=${pkgname}.install
backup=(etc/conf.d/cjdns)

# Disable Arch's default compilation optimizations
CFLAGS=""
OPTIONS=""

build() {
    # Delete 'cjdns' build files before updating if it already exists, or clone a fresh repo
    if [ -d "$srcdir"/cjdns ]; then
        pushd "$srcdir"/cjdns
            rm -rf build/ cjdns cjdroute
            git pull
        popd
    else
        git clone https://github.com/cjdelisle/cjdns.git
    fi

    pushd "$srcdir"/cjdns
        # BUILD CJDNS
        ./do

        # PATCH 'cjdnslog' IF NOT FIXED UPSTREAM
        if [ $(grep -c python2 contrib/python/cjdnslog) = 0 ]; then
            patch -p0 < ../cjdnslog.patch
        fi
    popd
}

package() {
    # If the package directory isn't empty, delete its contents
    if [ ! $(ls -1 "$pkgdir" | wc -l) = 0 ]; then
        rm -rf "$pkgdir"/*
    fi

    pushd "$srcdir"
        install -D -m644 cjdns.conf.d "$pkgdir"/etc/conf.d/cjdns
        install -D -m755 cjdns.rc.d "$pkgdir"/etc/rc.d/cjdns
        install -D -m644 cjdns.service "$pkgdir"/usr/lib/systemd/system/cjdns.service
        install -D -m755 cjdns/scripts/cjdns.sh "$pkgdir"/usr/bin/cjdns.sh
        install -D -m644 cjdns/contrib/python/cjdns.py "$pkgdir"/usr/lib/$(python2-config --libs | grep -o -E python2.*)/cjdns.py
        install -D -m644 cjdns/contrib/python/bencode.py "$pkgdir"/usr/lib/$(python2-config --libs | grep -o -E python2.*)/bencode.py
        install -D -m755 cjdns/contrib/python/cjdnslog "$pkgdir"/usr/bin/cjdnslog
        install -D -m755 cjdns/cjdroute "$pkgdir"/usr/bin/cjdroute
        install -D -m755 cjdns/cjdns "$pkgdir"/usr/bin/cjdns
    popd
}
