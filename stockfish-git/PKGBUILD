# Maintainer: Boohbah <boohbah at gmail.com>
# Contributor: P. Badredin <p dot badredin at gmail dot com>
# Contributor: Justin Blanchard <UncombedCoconut at gmail dot com>

_pkgname=stockfish
pkgname=$_pkgname-git
pkgver=20130227
pkgrel=1
pkgdesc="A free UCI chess engine derived from Glaurung 2.1"
arch=('i686' 'x86_64')
url="http://stockfishchess.org/"
license=('GPL3')
optdepends=('polyglot: for xboard support')
makedepends=('git')
conflicts=('stockfish')
install=stockfish.install
source=("http://cl.ly/3x333m0G173F/download/stockfish-231-book.zip")
md5sums=('9e51c2e57d8b55bbc588150033e4b133')

_gitroot=git://github.com/mcostalba/Stockfish.git
_gitname=Stockfish

build() {
  cd "$srcdir"
  msg "Connecting to GIT server...."

  if [[ -d "$_gitname" ]]; then
    cd "$_gitname" && git pull origin
    msg "The local files are updated."
  else
    git clone "$_gitroot" "$_gitname"
  fi

  msg "GIT checkout done or server timeout"
  msg "Starting build..."

  rm -rf "$srcdir/$_gitname-build"
  git clone "$srcdir/$_gitname" "$srcdir/$_gitname-build"
  cd "$srcdir/$_gitname-build/src"

  # Change the default book path to a system-wide location
  sed -i '/Book File/s:book.bin:/usr/share/stockfish/book.bin:' ucioption.cpp

  if [[ "$CARCH" == "i686" ]]; then
      _arch=x86-32
  elif grep -q popcnt /proc/cpuinfo; then
      _arch=x86-64-modern
  else
      _arch=x86-64
  fi

  msg "Making profile $_arch"
  make profile-build ARCH=$_arch
}

package() {
  cd "$srcdir"
  install -D -m644 Book.bin "$pkgdir/usr/share/$_pkgname/book.bin"

  cd "$srcdir/$_gitname-build/src"
  make PREFIX="$pkgdir/usr" install
}

# vim:set ts=2 sw=2 et:
