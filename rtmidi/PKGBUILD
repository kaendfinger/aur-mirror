# Maintainer: speps <speps at aur dot archlinux dot org>

pkgname=rtmidi
pkgver=2.0.1
pkgrel=2
pkgdesc="A set of C++ classes that provides a common API for realtime MIDI input/output."
arch=(i686 x86_64)
url="http://www.music.mcgill.ca/~gary/rtmidi/"
license=('custom:MIT')
depends=('jack')
source=("${url}release/$pkgname-$pkgver.tar.gz" $pkgname.pc)
md5sums=('6c4d51ce4c838fef74b9cb0bb5153eed'
         '20f6a7102226200d44063a5c1598f54a')

build() {
  cd "$srcdir/$pkgname-$pkgver"
  ./configure --prefix=/usr \
              --with-alsa \
              --with-jack
  make

  # utils
  cd tests && make
}

package() {
  cd "$srcdir/$pkgname-$pkgver"

  # pkg-config
  install -Dm755 rtmidi-config "$pkgdir/usr/bin/rtmidi-config"

  # bin
  for _bin in `find tests -type f -perm 755`; do
    install -Dm755 $_bin "$pkgdir/usr/bin/rtmidi-"`basename $_bin`
  done

  # lib
  install -d "$pkgdir/usr/lib"
  install -Dm755 librtmidi*.so.* "$pkgdir/usr/lib/"

  # headers
  install -d "$pkgdir/usr/include"
  install -Dm644 Rt{Midi,Error}.h "$pkgdir/usr/include"

  # pkg-config
  install -Dm644 $srcdir/$pkgname.pc \
    "$pkgdir/usr/lib/pkgconfig/$pkgname.pc"

  # docs
  install -d "$pkgdir/usr/share/doc/$pkgname"
  cp -a readme doc/{html,images} "$pkgdir/usr/share/doc/$pkgname"

  # license
  install -d "$pkgdir/usr/share/licenses/$pkgname"
  tail -n26 readme > "$pkgdir/usr/share/licenses/$pkgname/LICENSE"
}

# vim:set ts=2 sw=2 et:
