# Maintainer: josephgbr <rafael.f.f1@gmail.com>
# Contributor: Franco Tortoriello <franco.tortoriello@gmail.com>
# Contributor: Tobias Powalowski <tpowa@archlinux.org>

_pkgbase=libtirpc
pkgname=lib32-${_pkgbase}
pkgver=0.2.2
pkgrel=3
pkgdesc="Transport Independent RPC library (SunRPC replacement) (lib32)"
arch=('x86_64')
url="http://libtirpc.sourceforge.net/"
license=('BSD')
depends=("${_pkgbase}" 'lib32-libgssglue')
makedepends=('gcc-multilib')
options=('!libtool')
source=(http://downloads.sourceforge.net/sourceforge/${_pkgbase}/${_pkgbase}-${pkgver}.tar.bz2
       libtirpc-0.2.1-fortify.patch
       libtirpc-0.2.3rc1.patch
       libtirpc-fix-segfault-0.2.2.patch)
md5sums=('74c41c15c2909f7d11d9c7bfa7db6273'
         '2e5c067f1651469dfbbdc91d3c9c60e8'
         'ac2a1657b44f4a99c37d8265b05b9133'
         '5a3ab55934cad4e8b38fc90c54756472')

build() {
  cd ${_pkgbase}-${pkgver}
  
  # fix http://bugs.gentoo.org/293593
  # https://bugs.archlinux.org/task/20082
  patch -Np1 -i ../libtirpc-0.2.1-fortify.patch
  # add patches from fedora git to make nfs-utils compile again
  patch -Np1 -i ../libtirpc-0.2.3rc1.patch
  patch -Np1 -i ../libtirpc-fix-segfault-0.2.2.patch
  
  # Fix the AM_CONFIG_HEADER obsolete error
  sed -i 's/AM_CONFIG_HEADER(config.h)/AC_CONFIG_HEADERS(\[config.h\])/' configure.ac
  
  sh autogen.sh
  autoreconf -fisv
  ./configure --prefix=/usr --libdir=/usr/lib32 --enable-gss CC="gcc -m32"
  make
}

package() {
  make -C ${_pkgbase}-${pkgver} DESTDIR="${pkgdir}" install
  rm -rf "${pkgdir}"/{etc,usr/{include,share}}
  install -dm755 "${pkgdir}"/usr/share/licenses/
  ln -s $_pkgbase "${pkgdir}"/usr/share/licenses/${pkgname}
}
