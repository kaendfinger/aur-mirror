# Maintainer: Gustavo Alvarez <sl1pkn07@gmail.com>

_patchlevel=1
_basever=0.148

pkgname=sdlmamefamily-tools
pkgver="${_basever}.u${_patchlevel}"
pkgrel=1
pkgdesc="Tools for manage MAME/MESS/UME roms"
url="http://mamedev.org/"
license=('custom:MAME License')
arch=('i686' 'x86_64')
depends=('sdl>=1.2.11' 'fontconfig' 'sdl_ttf' 'alsa-lib')
makedepends=('nasm' 'mesa' 'glu' 'wget' 'python2')
DLAGENTS=('http::/usr/bin/wget -U "Mozilla/5.0 (X11; U; Linux x86_64; en-US; rv:1.9.1.2) Gecko/20090804 Shiretoko/3.5.2" -c -t 3 --waitretry=3 -O %o %u')

for i in $(seq 1 "${_patchlevel}"); do
  _patches="${_patches} sdlmame-${_basever/./}u${i}_diff.zip::http://mamedev.org/updates/${_basever/./}u${i}_diff.zip"
done

source=("mame${_basever/./}s.zip::http://mamedev.org/downloader.php?file=releases/mame${_basever/./}s.zip"
        ${_patches}
        'sdlmame-wout-gconf-v3.patch')
md5sums=('38f7727c2961cd31e2ab6aa1814a23ba'
         '01edd53824784f52448f4128f6d52aac'
         'd3d87fcab1b0629ba4bc56d305f5843a')

build() {
  cd "${srcdir}"
  bsdtar -xf mame.zip
  find . -type f -not -name "*.png" -exec perl -pi -e 's|\r\n?|\n|g' "{}" \;
  for i in $(seq 1 "${_patchlevel}"); do
    msg "Patch#${i}"
    patch --silent -p0 -E -i "${_basever/./}u${i}.diff"
  done

  msg "Patch disable Gconf and Gtk dependencies"
  patch --silent -p0 -E -i "${srcdir}/sdlmame-wout-gconf-v3.patch"
  msg2 "Done"

  make  tools PYTHON=python2 NO_X11=1 OPTIMIZE=2 NOWERROR=1 ARCHOPTS=-march=native TARGET=mame
  make  tools PYTHON=python2 NO_X11=1 OPTIMIZE=2 NOWERROR=1 ARCHOPTS=-march=native TARGET=mess
  make  tools PYTHON=python2 NO_X11=1 OPTIMIZE=2 NOWERROR=1 ARCHOPTS=-march=native TARGET=ume
}

package() {
  cd "${srcdir}"

  # Install the applications and the UI font in /usr/share
  install -Dm755 castool    "${pkgdir}/usr/bin/castool"
  install -Dm755 chdman     "${pkgdir}/usr/bin/chdman"
  install -Dm755 floptool   "${pkgdir}/usr/bin/floptool"
  install -Dm755 imgtool    "${pkgdir}/usr/bin/imgtool"
  install -Dm755 jedutil    "${pkgdir}/usr/bin/jedutil"
  install -Dm755 ldresample "${pkgdir}/usr/bin/ldresample"
  install -Dm755 ldverify   "${pkgdir}/usr/bin/ldverify"
  install -Dm755 regrep     "${pkgdir}/usr/bin/regrep"
  install -Dm755 romcmp     "${pkgdir}/usr/bin/romcmp"
  install -Dm755 split      "${pkgdir}/usr/bin/splitmamerom"
  install -Dm755 src2html   "${pkgdir}/usr/bin/src2html"
  install -Dm755 srcclean   "${pkgdir}/usr/bin/srcclean"
  install -Dm755 testkeys   "${pkgdir}/usr/bin/testkeys"
  install -Dm755 unidasm    "${pkgdir}/usr/bin/unidasm"

  # Install the extra bits
  install -d "${pkgdir}/usr/share/man/"man{1,6}
  install -m644 src/osd/sdl/man/*.1* "${pkgdir}/usr/share/man/man1/"

  find "${pkgdir}" -type f -exec strip "{}" \;

}
