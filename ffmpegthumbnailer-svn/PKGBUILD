# Maintainer: Limao Luo <luolimao+AUR@gmail.com>
# Contributor: Gergely Imreh <imrehg(at)gmail(dot)com>
# Contributor: Ronald van Haren <ronald.archlinux.org>
# Contributor: boromil@gmail.com
# Contributor: Ner0

_pkgname=ffmpegthumbnailer
pkgname=$_pkgname-svn
pkgver=247
pkgrel=1
pkgdesc="Lightweight video thumbnailer that can be used by file managers."
arch=(i686 x86_64)
url=http://code.google.com/p/$_pkgname/
license=(GPL2)
depends=(ffmpeg libjpeg libpng)
makedepends=(subversion)
optdepends=('gvfs: support for gio uris')
provides=($_pkgname=2.0.8)
conflicts=($_pkgname)
options=(!libtool)

_svntrunk=https://$_pkgname.googlecode.com/svn/trunk/
_svnmod=trunk

build() {
    install -d "$srcdir"/$_pkgname/
    cd "$srcdir"/$_pkgname/

    msg "Starting SVN checkout..."
    if [[ -d $_svnmod/.svn ]]; then
        pushd $_svnmod && svn up -r $pkgver
        msg2 "The local files have been updated."
        popd
    else
        svn co $_svntrunk --config-dir ./ -r $pkgver $_svnmod
    fi
    msg2 "SVN checkout done or server timeout"

    rm -rf $_svnmod-build
    cp -r $_svnmod $_svnmod-build
    cd $_svnmod-build

    msg "Compiling..."
    sed -i 's:AM_CONFIG_HEADER:AC_CONFIG_HEADERS:' configure.ac
    autoreconf -fi
    ./configure --prefix=/usr --enable-gio --enable-thumbnailer
    make
}

package() {
    cd "$srcdir"/$_pkgname/$_svnmod-build/
    make DESTDIR="$pkgdir" install
}
