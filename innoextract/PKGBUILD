# Maintainer: Sam S. <smls75@gmail.com>
# Contributor: Daniel Scharrer <daniel@constexpr.org>

pkgname=innoextract
pkgver=1.3
pkgrel=2
pkgdesc='A tool to extract installers created by Inno Setup'
url='http://constexpr.org/innoextract/'
arch=('i686' 'x86_64')
license=('ZLIB')
depends=('boost-libs' 'xz')
makedepends=('boost' 'cmake')
source=("http://downloads.sourceforge.net/${pkgname}/${pkgname}-${pkgver}.tar.gz")
sha1sums=('8b0b19b92f1bfbeb48bdb0cd01e1dab8c15cac2d')

build() {
  cd "${srcdir}/${pkgname}-${pkgver}"
  
  cmake . -DCMAKE_INSTALL_PREFIX=/usr \
          -DCMAKE_BUILD_TYPE=Release
  
  # To get a debug build change -DCMAKE_BUILD_TYPE=Release to
  # -DCMAKE_BUILD_TYPE=Debug - this will run signifincantly slower but
  # enable more runtime checks and generate better crash reports.
  
  make
}

package() {
  cd "${srcdir}/${pkgname}-${pkgver}"
  
  make DESTDIR="${pkgdir}" install
  
  install -Dm644 README.md \
          "${pkgdir}/usr/share/doc/${pkgname}/README.md"
  install -Dm644 CHANGELOG \
          "${pkgdir}/usr/share/doc/${pkgname}/CHANGELOG"
  install -Dm644 LICENSE \
          "${pkgdir}/usr/share/licenses/${pkgname}/LICENSE"
}
