# Maintainer: Det <nimetonmaili at gmail a-dot com>
# Based on google-earth: https://aur.archlinux.org/packages/google-earth/

pkgname=google-earth6
pkgver=6.2.2.6613
pkgrel=1
pkgdesc="A legacy 3D interface to view satellite images of Earth and other objects"
arch=('i686' 'x86_64')
url="http://www.google.com/earth"
license=('custom')
depends=('desktop-file-utils' 'hicolor-icon-theme' 'ld-lsb' 'lib32-fontconfig' 'lib32-libgl' 'lib32-gcc-libs' 'lib32-libsm' 'lib32-libxrender' 'lib32-mesa' 'shared-mime-info')
optdepends=('lib32-ati-dri: For the open source AMD/ATI Radeon driver'
            'lib32-catalyst-utils: For AMD Catalyst'
            'lib32-gtk2: SCIM support'
            'lib32-intel-dri: For the open source Intel driver'
            'lib32-nouveau-dri: For the open source Nouveau driver'
            'lib32-nss-mdns: In case the application fails to contact the servers'
            'lib32-nvidia-utils: For the NVIDIA driver'
            'lib32-nvidia-utils-bumblebee: For the NVIDIA driver + Bumblebee setups'
            'qt4: For changing the font size with qtconfig'
            'ttf-ms-fonts: Fonts')
options=('!emptydirs')
install=$pkgname.install
_arch=i386  # Workaround for the AUR Web interface Source parser
[ "$CARCH" == "x86_64" ] && _arch=amd64
source=("http://packages.linuxmint.com/pool/import/g/googleearth/googleearth_$pkgver-r0_$_arch.deb"
        "$pkgname-mimetypes.xml")
md5sums=('bd1960eb3e2d061b319c3f38835840d7'
         'e3c67b8d05c3de50535bd7e45eee728e')

if [[ "$CARCH" == "i686" ]]; then
  depends=('desktop-file-utils' 'fontconfig' 'hicolor-icon-theme' 'ld-lsb' 'libgl' 'libsm' 'libxrender' 'mesa' 'shared-mime-info')
  optdepends=('ati-dri: For the open source AMD/ATI Radeon driver'
              'catalyst-utils: For AMD Catalyst'
              'gtk2: SCIM support'
              'intel-dri: For the open source Intel driver'
              'nouveau-dri: For the open source Nouveau driver'
              'nss-mdns: In case the application fails to contact the servers'
              'nvidia-utils: For the NVIDIA driver'
              'nvidia-utils-bumblebee: For the NVIDIA driver + Bumblebee setups'
              'qt4: For changing the font size with qtconfig'
              'ttf-ms-fonts: Fonts')
  md5sums[0]='e38c0d806cf1d581213fb6f4f204ec9c'
fi

_instdir=/opt/google/earth/legacy
PKGEXT=".pkg.tar"

package() {
  msg2 "Extracting the data.tar.gz"
  bsdtar -xf data.tar.gz -C "$pkgdir/"

  msg2 "Making us coexist with 'google-earth'"
  mv "$pkgdir"/opt/google/earth/free/ "$pkgdir"/$_instdir/
  mv "$pkgdir"/usr/share/menu/${pkgname/6}{,6}.menu

  # The binary symlinks
  rm "$pkgdir"/usr/bin/${pkgname/6}
  ln -sf $_instdir/googleearth "$pkgdir"/usr/bin/$pkgname
  ln -sf $_instdir/googleearth "$pkgdir"/$_instdir/${pkgname/6}

  # The desktop
  sed -i "s/ Earth/ Earth Legacy/;s/3D/Legacy 3D/;s/free/legacy/;s/=${pkgname/6}/=${pkgname}/" "$pkgdir"/$_instdir/${pkgname/6}.desktop

  msg2 "Installing the .desktop file, the shared MIME info package, icons and the license"
  mv "$pkgdir"/$_instdir/${pkgname/6}.desktop "$pkgdir"/usr/share/applications/$pkgname.desktop
  install -Dm644 $pkgname-mimetypes.xml "$pkgdir"/usr/share/mime/packages/$pkgname-mimetypes.xml

  # Icons
  for i in 16 22 24 32 48 64 128 256; do
    install -Dm644 "$pkgdir"/$_instdir/product_logo_${i}.png "$pkgdir"/usr/share/icons/hicolor/${i}x${i}/apps/$pkgname.png
  done

  # The license
  install -d "$pkgdir"/usr/share/licenses/$pkgname/
  curl -s $url/license.html -o "$pkgdir"/usr/share/licenses/$pkgname/license.html

  msg2 "Removing the duplicated images"
  rm "$pkgdir"/$_instdir/product_logo_*.png
}