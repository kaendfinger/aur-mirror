# Maintainer: X0rg
# Contributor: Xemertix <arch88(at)katamail(dot)com>
# Contributor: Evangelos Foutras <foutrelis@gmail.com>

pkgname=php-gtk-git
pkgver=20130119
pkgrel=4
pkgdesc="PHP bindings for the Gtk+ 2 library"
arch=('i686' 'x86_64')
url="http://gtk.php.net/"
license=('LGPL')
depends=('git' 'php' 'gtk2>=2.12.0' 'libglade>=2.6.2' 'php-pecl-cairo-svn')
makedepends=('pkgconfig' 'gtk2+extra' 'gtkhtml' 'libsexy' 'xulrunner'
             'gtksourceview3' 'gtkspell')
optdepends=('gtk2+extra: extra extension'
            'gtkhtml: html extension'
            'libsexy: libsexy extension'
            'xulrunner: mozembed extension'
            'gtksourceview: sourceview extension'
            'gtkspell: spell extension')
provides=('php-gtk')
conflicts=('php-gtk' 'php-gtk-svn')
backup=(etc/php/php-gtk.ini)
install=php-gtk.install
source=(php.ini-template	
	disable-open_basedir.patch)

_gitroot="git://git.php.net/php/gtk-src.git"
_gitname="master"

md5sums=('219a7f7f336a4ed3fe27bb352b8199c3'
         '1654fc571317e32791ebf2db7791dbe5')

build() {
  
  msg "Connecting to GIT server...."

  if [[ -d "$_gitname" ]]; then
    cd "$_gitname" && git pull origin
    msg "The local files are updated."
  else
    git clone "$_gitroot" "$_gitname"
  fi

  msg "GIT checkout done or server timeout"
  msg "Starting build..."

  rm -rf "$srcdir/$_gitname-build"
  git clone "$srcdir/$_gitname" "$srcdir/$_gitname-build"
  cd "$srcdir/$_gitname-build"

  ./buildconf
  ./configure
  # We need to allow php to open files outside of open_basedir (defined
  # in /etc/php/php.ini) during the build
  patch -Np1 -i "$srcdir/disable-open_basedir.patch" || return 1

  # Don't work with 'make -jN' where N>1
  make -j1

  make INSTALL_ROOT="$pkgdir" install

  install -D -m644 "$srcdir/php.ini-template" "$pkgdir/etc/php/php-gtk.ini"
  EXTENSION_DIR=$(php-config --extension-dir)
  sed -i "/^extension_dir/cextension_dir = \"$EXTENSION_DIR\"" \
         "$pkgdir/etc/php/php-gtk.ini"

}
